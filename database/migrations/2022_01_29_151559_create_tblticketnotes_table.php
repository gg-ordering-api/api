<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblticketnotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblticketnotes', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('TID')->default(0)->index('TID');
            $table->mediumText('NoteBody')->nullable();
            $table->boolean('isSupplierTicketNumberEdited')->default(false);
            $table->timestamp('CreatedOn')->useCurrent()->index('tblticketnotes_CreatedOn_index');
            $table->integer('CreatedBy')->nullable()->index('CreatedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblticketnotes');
    }
}
