<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWasteOnlineSundriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waste_online_sundries', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('waste_online_order_id')->nullable();
            $table->integer('client_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->string('sundry_name', 100)->nullable();
            $table->dateTime('sundry_date')->nullable()->useCurrent();
            $table->decimal('customer_charge', 10)->nullable();
            $table->boolean('credit_debit')->nullable();
            $table->integer('raised_by')->nullable();
            $table->boolean('is_invoiced')->nullable();
            $table->dateTime('invoiced_on')->nullable();
            $table->boolean('is_deleted')->nullable()->default(false);
            $table->integer('deleted_by')->nullable();
            $table->decimal('supplier_charge', 10)->nullable();
            $table->boolean('supplier_paid')->nullable()->default(false);
            $table->string('invoice_number', 50)->nullable();
            $table->boolean('is_paid')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->string('supplier_contact', 200)->nullable();
            $table->boolean('is_approved')->nullable();
            $table->integer('approved_by')->nullable();
            $table->dateTime('approved_on')->nullable();
            $table->mediumText('approval_reason')->nullable();
            $table->decimal('sundry_margin', 10)->nullable();
            $table->mediumText('sundry_details')->nullable();
            $table->string('supplier_invoice_number', 100)->nullable();
            $table->string('customer_name', 100)->nullable();
            $table->boolean('do_not_invoice')->nullable();
            $table->dateTime('creation_date')->nullable()->useCurrent();
            $table->string('po_number', 50)->nullable();
            $table->unsignedInteger('supplier_invoice_checked_by')->nullable();
            $table->string('supplier_ticket_number', 240)->nullable();
            $table->integer('waste_online_address_id')->nullable();
            $table->string('business_name', 100)->nullable();
            $table->dateTime('supplier_invoice_checked_date')->nullable();
            $table->integer('dispute_type_id')->nullable();
            $table->tinyInteger('is_disputed')->nullable();
            $table->integer('disputed_by')->nullable();
            $table->integer('dispute_cleared_by')->nullable();
            $table->text('dispute_reason')->nullable();
            $table->string('dispute_contact', 100)->nullable();
            $table->string('dispute_name', 100)->nullable();
            $table->dateTime('disputed_date')->nullable();
            $table->string('dispute_extra', 240)->nullable();
            $table->boolean('is_permit')->nullable()->default(false);
            $table->string('supplier_transfer_ticket_number', 50)->nullable();
            $table->text('dispute_clear_reason')->nullable();
            $table->dateTime('dispute_clear_date')->nullable();
            $table->integer('invoice_id')->nullable();
            $table->text('payment_reference')->nullable();
            $table->integer('order_document_id')->nullable();
            $table->string('supplier_weighbridge_ticket_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste_online_sundries');
    }
}
