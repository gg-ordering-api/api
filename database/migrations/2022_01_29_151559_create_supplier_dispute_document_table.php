<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierDisputeDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_dispute_document', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('invoice_id');
            $table->string('supplier_id', 50)->nullable();
            $table->string('document_name', 181)->nullable();
            $table->string('file_name', 181)->nullable();
            $table->string('extension', 5)->nullable();
            $table->string('file_type', 500)->nullable();
            $table->timestamp('date_added')->useCurrent();
            $table->unsignedInteger('added_by')->default('0');
            $table->integer('movement_type')->nullable()->default(0);
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'failed', 'yes'])->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_dispute_document');
    }
}
