<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblcontainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblcontainers', function (Blueprint $table) {
            $table->increments('ID')->unique('tblcontainers_ID_uindex');
            $table->string('SkipCode', 50)->nullable();
            $table->string('SkipDescription', 500)->nullable();
            $table->boolean('IsRoRo')->default(false);
            $table->decimal('CubicYards', 10, 4)->default(0);
            $table->boolean('IsCraneTested')->default(false);
            $table->string('MillerSWColumn', 20)->default('0');
            $table->boolean('LOLACert')->default(false);
            $table->integer('SkipCode_integer')->default(0);
            $table->integer('weight_required')->nullable()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblcontainers');
    }
}
