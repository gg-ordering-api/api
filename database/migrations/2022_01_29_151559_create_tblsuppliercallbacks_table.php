<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsuppliercallbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsuppliercallbacks', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('SupplierID')->default(0)->index('SupplierID');
            $table->integer('UserID')->default(0)->index('UserID');
            $table->mediumText('AlertBody')->nullable();
            $table->dateTime('AlertTimeStamp')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('Status')->default(0);
            $table->integer('CreatedBy')->default(0)->index('CreatedBy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsuppliercallbacks');
    }
}
