<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblmessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblmessages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recipient_user_id')->index('recipient_user_id');
            $table->integer('sender_user_id');
            $table->smallInteger('state')->default(0);
            $table->mediumText('body');
            $table->string('subject');
            $table->timestamp('date')->useCurrent();
            $table->integer('archive')->default(0);
            $table->boolean('IsWarned')->default(false);
            $table->dateTime('CloseTimestamp')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('is_quote')->default(0);
            $table->boolean('object_type')->nullable();
            $table->integer('object_id')->nullable();

            $table->index(['date', 'state', 'archive', 'recipient_user_id'], 'StaffActivityReport');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblmessages');
    }
}
