<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblportaluserpermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblportaluserpermissions', function (Blueprint $table) {
            $table->unsignedSmallInteger('ID')->default('0');
            $table->string('PermissionName', 50)->nullable();
            $table->boolean('CostReports')->default(false);
            $table->boolean('SkipMovements')->default(false);
            $table->boolean('ScheduleMovements')->default(false);
            $table->boolean('RecyclingReport')->default(false);
            $table->boolean('LeagueTables')->default(false);
            $table->boolean('CRMP')->default(false);
            $table->boolean('CanInvoice')->default(false);

            $table->primary(['CostReports', 'SkipMovements', 'ScheduleMovements', 'RecyclingReport', 'LeagueTables', 'CRMP', 'CanInvoice']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblportaluserpermissions');
    }
}
