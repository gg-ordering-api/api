<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsupplierdisputehandlingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsupplierdisputehandling', function (Blueprint $table) {
            $table->increments('ID');
            $table->mediumText('InvoiceNumber')->index('tblsupplierdisputehandling_InvoiceNumber_index');
            $table->integer('HandlingUser')->default(0);
            $table->mediumText('HandlingNotes');
            $table->timestamp('HandlingSetDate')->useCurrent();
            $table->tinyInteger('Warning')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsupplierdisputehandling');
    }
}
