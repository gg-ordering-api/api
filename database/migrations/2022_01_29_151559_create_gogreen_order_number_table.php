<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGogreenOrderNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gogreen_order_number', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('ticket_id')->nullable()->index('gogreen_order_number_ticket_id_IDX');
            $table->integer('ocr_order_type_id')->nullable()->index('gogreen_order_number_ocr_order_type_id_fki');
            $table->string('order_number', 50)->nullable()->index();
            $table->unsignedInteger('supplier_id')->nullable()->index('gogreen_order_number_supplier_id_indexi');
            $table->unsignedInteger('supplier_invoice_id')->nullable()->index('gogreen_order_number_supplier_invoice_id_indexi');
            $table->tinyInteger('compliance_checked')->nullable()->default(0);
            $table->boolean('show_wtn')->nullable()->default(false);
            $table->integer('gogreen_order_status_id')->nullable()->index('gogreen_order_number_gogreen_order_status_id_fki');
            $table->unsignedInteger('assigned_accounts_id')->nullable()->index('gogreen_order_number_tblusers_ID_fki');
            $table->unsignedInteger('assigned_compliance_id')->nullable();
            $table->decimal('estimated_supplier_cost', 10)->nullable();
            $table->decimal('estimated_client_cost', 10)->nullable();
            $table->integer('linked_order')->nullable()->index('gogreen_order_number_gogreen_order_number_id_fki');
            $table->boolean('supplier_invoice_fully_assigned')->nullable()->default(false);
            $table->dateTime('updated_at')->nullable();
            $table->boolean('supplier_ticket_not_matched')->nullable()->default(true);
            $table->boolean('supplier_ticket_wtn_missing')->nullable()->default(true);
            $table->boolean('override_supplier_ticket')->nullable()->default(false);
            $table->tinyInteger('allowed_dispute')->nullable()->default(0);
            $table->integer('assigned_fcc_id')->nullable();

            $table->index(['supplier_id', 'supplier_invoice_id', 'ocr_order_type_id'], 'gg_o_n_supplier_id_supplier_invoice_id_ocr_order_type_id_index');
            $table->index(['ticket_id', 'ocr_order_type_id'], 'gogreen_order_number_ticket_id_ocr_order_type_IDX');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gogreen_order_number');
    }
}
