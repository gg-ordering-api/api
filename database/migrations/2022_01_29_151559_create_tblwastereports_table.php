<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblwastereportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblwastereports', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('ClientID')->index('ClientID');
            $table->bigInteger('SiteID')->index('SiteID');
            $table->dateTime('EndDate')->nullable()->default('0000-00-00 00:00:00');
            $table->timestamp('DateCreated')->useCurrent();
            $table->integer('CreatedBy')->default(0);
            $table->smallInteger('WRStatus')->default(0);
            $table->dateTime('StartDate')->nullable()->default('0000-00-00 00:00:00');
            $table->string('ProvidedBy', 100)->nullable();
            $table->dateTime('ProvidedOn')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('ReportNote')->nullable();
            $table->decimal('aggper', 20, 4)->default(0);
            $table->decimal('aggyd', 20, 4)->default(0);
            $table->decimal('aggconv', 20, 4)->default(1);
            $table->decimal('aggtotal', 20, 4)->default(0);
            $table->decimal('aggperrec', 20, 4)->default(0);
            $table->decimal('aggrectonnes', 20, 4)->default(0);
            $table->mediumText('aggnote')->nullable();
            $table->decimal('hardcoreper', 20, 4)->default(0);
            $table->decimal('hardcoreyd', 20, 4)->default(0);
            $table->decimal('hardcoreconv', 20, 4)->default(1);
            $table->decimal('hardcoretotal', 20, 4)->default(0);
            $table->decimal('hardcoreperrec', 20, 4)->default(0);
            $table->decimal('hardcorerectonnes', 20, 4)->default(0);
            $table->mediumText('hardcorenote')->nullable();
            $table->decimal('ceramicsper', 20, 4)->default(0);
            $table->decimal('ceramicyd', 20, 4)->default(0);
            $table->decimal('ceramicconv', 20, 4)->default(0.35);
            $table->decimal('ceramictotal', 20, 4)->default(0);
            $table->decimal('ceramicperrec', 20, 4)->default(0);
            $table->decimal('ceramicrectonnes', 20, 4)->default(0);
            $table->mediumText('ceramicnote')->nullable();
            $table->decimal('tilesper', 20, 4)->default(0);
            $table->decimal('tilesyd', 20, 4)->default(0);
            $table->decimal('tilesconv', 20, 4)->default(0.125);
            $table->decimal('tilestotal', 20, 4)->default(0);
            $table->decimal('tilesperrec', 20, 4)->default(0);
            $table->decimal('tilesrectonnes', 20, 4)->default(0);
            $table->mediumText('tilesnote')->nullable();
            $table->decimal('soilper', 20, 4)->default(0);
            $table->decimal('soilyd', 20, 4)->default(0);
            $table->decimal('soilconv', 20, 4)->default(1.12);
            $table->decimal('soiltotal', 20, 4)->default(0);
            $table->decimal('soilperrec', 20, 4)->default(0);
            $table->decimal('soilrectonnes', 20, 4)->default(0);
            $table->mediumText('soilnote')->nullable();
            $table->decimal('cardboardper', 20, 4)->default(0);
            $table->decimal('cardboardyd', 20, 4)->default(0);
            $table->decimal('cardboardconv', 20, 4)->default(0.06);
            $table->decimal('cardboardtotal', 20, 4)->default(0);
            $table->decimal('cardboardperrec', 20, 4)->default(0);
            $table->decimal('cardboardrectonnes', 20, 4)->default(0);
            $table->mediumText('cardboardnote')->nullable();
            $table->decimal('paperper', 20, 4)->default(0);
            $table->decimal('paperyd', 20, 4)->default(0);
            $table->decimal('paperconv', 20, 4)->default(0.06);
            $table->decimal('papertotal', 20, 4)->default(0);
            $table->decimal('paperperrec', 20, 4)->default(0);
            $table->decimal('paperrectonnes', 20, 4)->default(0);
            $table->mediumText('papernote')->nullable();
            $table->decimal('softper', 20, 4)->default(0);
            $table->decimal('softyd', 20, 4)->default(0);
            $table->decimal('softconv', 20, 4)->default(0.06);
            $table->decimal('softtotal', 20, 4)->default(0);
            $table->decimal('softperrec', 20, 4)->default(0);
            $table->decimal('softrectonnes', 20, 4)->default(0);
            $table->mediumText('softnote')->nullable();
            $table->decimal('hardper', 20, 4)->default(0);
            $table->decimal('hardyd', 20, 4)->default(0);
            $table->decimal('hardconv', 20, 4)->default(0.115);
            $table->decimal('hardtotal', 20, 4)->default(0);
            $table->decimal('hardperrec', 10, 4)->nullable()->default(0);
            $table->decimal('hardrectonnes', 20, 4)->default(0);
            $table->mediumText('hardnote')->nullable();
            $table->decimal('plateper', 20, 4)->default(0);
            $table->decimal('plateyd', 20, 4)->default(0);
            $table->decimal('plateconv', 20, 4)->default(0.75);
            $table->decimal('platetotal', 20, 4)->default(0);
            $table->decimal('plateperrec', 20, 4)->default(0);
            $table->decimal('platerectonnes', 20, 4)->default(0);
            $table->mediumText('platenote')->nullable();
            $table->decimal('glassbottlesper', 20, 4)->default(0);
            $table->decimal('glassbottlesyd', 20, 4)->default(0);
            $table->decimal('glassbottlesconv', 20, 4)->default(0.6);
            $table->decimal('glassbottlestotal', 20, 4)->default(0);
            $table->decimal('glassbottlesperrec', 20, 4)->default(0);
            $table->decimal('glassbottlesrectonnes', 20, 4)->default(0);
            $table->mediumText('glassbottlesnote')->nullable();
            $table->decimal('timberper', 20, 4)->default(0);
            $table->decimal('timberyd', 20, 4)->default(0);
            $table->decimal('timberconv', 20, 4)->default(0.22);
            $table->decimal('timbertotal', 20, 4)->default(0);
            $table->decimal('timberperrec', 20, 4)->default(0);
            $table->decimal('timberrectonnes', 20, 4)->default(0);
            $table->mediumText('timbernote')->nullable();
            $table->decimal('mdfper', 20, 4)->default(0);
            $table->decimal('mdfyd', 20, 4)->default(0);
            $table->decimal('mdfconv', 20, 4)->default(0.22);
            $table->decimal('mdftotal', 20, 4)->default(0);
            $table->decimal('mdfperrec', 20, 4)->default(0);
            $table->decimal('mdfrectonnes', 20, 4)->default(0);
            $table->mediumText('mdfnote')->nullable();
            $table->decimal('plyper', 20, 4)->default(0);
            $table->decimal('plyyd', 20, 4)->default(0);
            $table->decimal('plyconv', 20, 4)->default(0.2);
            $table->decimal('plytotal', 20, 4)->default(0);
            $table->decimal('plyperrec', 20, 4)->default(0);
            $table->decimal('plyrectonnes', 20, 4)->default(0);
            $table->mediumText('plynote')->nullable();
            $table->decimal('metalper', 20, 4)->default(0);
            $table->decimal('metalyd', 20, 4)->default(0);
            $table->decimal('metalconv', 20, 4)->default(0.25);
            $table->decimal('metaltotal', 20, 4)->default(0);
            $table->decimal('metalperrec', 20, 4)->default(0);
            $table->decimal('metalrectonnes', 20, 4)->default(0);
            $table->mediumText('metalnote')->nullable();
            $table->decimal('plasterper', 20, 4)->default(0);
            $table->decimal('plasteryd', 20, 4)->default(0);
            $table->decimal('plasterconv', 20, 4)->default(0.2);
            $table->decimal('plastertotal', 20, 4)->default(0);
            $table->decimal('plasterperrec', 20, 4)->default(0);
            $table->decimal('plasterrectonnes', 20, 4)->default(0);
            $table->mediumText('plasternote')->nullable();
            $table->decimal('insulationper', 20, 4)->default(0);
            $table->decimal('insulationyd', 20, 4)->default(0);
            $table->decimal('insulationconv', 20, 4)->default(0.075);
            $table->decimal('insulationtotal', 20, 4)->default(0);
            $table->decimal('insulationperrec', 20, 4)->default(0);
            $table->decimal('insulationrectonnes', 20, 4)->default(0);
            $table->mediumText('insulationnote')->nullable();
            $table->decimal('fabricsper', 20, 4)->default(0);
            $table->decimal('fabricsyd', 20, 4)->default(0);
            $table->decimal('fabricsconv', 20, 4)->default(0.06);
            $table->decimal('fabricstotal', 20, 4)->default(0);
            $table->decimal('fabricsperrec', 20, 4)->default(0);
            $table->decimal('fabricsrectonnes', 20, 4)->default(0);
            $table->mediumText('fabricsnote')->nullable();
            $table->decimal('canteenper', 20, 4)->default(0);
            $table->decimal('canteenyd', 20, 4)->default(0);
            $table->decimal('canteenconv', 20, 4)->default(0.08);
            $table->decimal('canteentotal', 20, 4)->default(0);
            $table->decimal('canteenperrec', 20, 4)->default(0);
            $table->decimal('canteenrectonnes', 20, 4)->default(0);
            $table->mediumText('canteennote')->nullable();
            $table->decimal('vegper', 20, 4)->default(0);
            $table->decimal('vegyd', 20, 4)->default(0);
            $table->decimal('vegconv', 20, 4)->default(0.22);
            $table->decimal('vegtotal', 20, 4)->default(0);
            $table->decimal('vegperrec', 20, 4)->default(0);
            $table->decimal('vegrectonnes', 20, 4)->default(0);
            $table->mediumText('vegnote')->nullable();
            $table->integer('BreakdownBy')->default(0)->index('BreakdownBy');
            $table->integer('DensityBy')->default(0)->index('DensityBy');
            $table->integer('PortalBy')->default(0)->index('PortalBy');
            $table->string('CDE', 10)->default('C');
            $table->decimal('aggperreco', 10, 4)->default(0);
            $table->decimal('aggrecotonnes', 10, 4)->default(0);
            $table->mediumText('aggnotereco')->nullable();
            $table->decimal('hardcoreperreco', 10, 4)->default(0);
            $table->decimal('hardcorerecotonnes', 10, 4)->default(0);
            $table->mediumText('hardcorenotereco')->nullable();
            $table->decimal('ceramicperreco', 10, 4)->default(0);
            $table->decimal('ceramicrecotonnes', 10, 4)->default(0);
            $table->mediumText('ceramicnotereco')->nullable();
            $table->decimal('tilesperreco', 10, 4)->default(0);
            $table->decimal('tilesrecotonnes', 10, 4)->default(0);
            $table->mediumText('tilesnotereco')->nullable();
            $table->decimal('soilperreco', 10, 4)->default(0);
            $table->decimal('soilrecotonnes', 10, 4)->default(0);
            $table->mediumText('soilnotereco')->nullable();
            $table->decimal('cardboardperreco', 10, 4)->default(0);
            $table->decimal('cardboardrecotonnes', 10, 4)->default(0);
            $table->mediumText('cardboardnotereco')->nullable();
            $table->decimal('paperperreco', 10, 4)->default(0);
            $table->decimal('paperrecotonnes', 10, 4)->default(0);
            $table->mediumText('papernotereco')->nullable();
            $table->decimal('softperreco', 10, 4)->default(0);
            $table->decimal('softrecotonnes', 10, 4)->default(0);
            $table->mediumText('softnotereco')->nullable();
            $table->decimal('hardperreco', 10, 4)->default(0);
            $table->decimal('hardrecotonnes', 10, 4)->default(0);
            $table->mediumText('hardnotereco')->nullable();
            $table->decimal('plateperreco', 10, 4)->default(0);
            $table->decimal('platerecotonnes', 10, 4)->default(0);
            $table->mediumText('platenotereco')->nullable();
            $table->decimal('glassbottlesperreco', 10, 4)->default(0);
            $table->decimal('glassbottlesrecotonnes', 10, 4)->default(0);
            $table->mediumText('glassbottlesnotereco')->nullable();
            $table->decimal('timberperreco', 10, 4)->default(0);
            $table->decimal('timberrecotonnes', 10, 4)->default(0);
            $table->mediumText('timbernotereco')->nullable();
            $table->decimal('mdfperreco', 10, 4)->default(0);
            $table->decimal('mdfrecotonnes', 10)->default(0);
            $table->mediumText('mdfnotereco')->nullable();
            $table->decimal('plyperreco', 10, 4)->default(0);
            $table->decimal('plyrecotonnes', 10, 4)->default(0);
            $table->mediumText('plynotereco')->nullable();
            $table->decimal('metalperreco', 10, 4)->default(0);
            $table->decimal('metalrecotonnes', 10, 4)->default(0);
            $table->mediumText('metalnotereco')->nullable();
            $table->decimal('plasterperreco', 10, 4)->default(0);
            $table->decimal('plasterrecotonnes', 10, 4)->default(0);
            $table->mediumText('plasternotereco')->nullable();
            $table->decimal('insulationperreco', 10, 4)->default(0);
            $table->decimal('insulationrecotonnes', 10, 4)->default(0);
            $table->mediumText('insulationnotereco')->nullable();
            $table->decimal('fabricsperreco', 10, 4)->default(0);
            $table->decimal('fabricsrecotonnes', 10, 4)->default(0);
            $table->mediumText('fabricsnotereco')->nullable();
            $table->decimal('canteenperreco', 10, 4)->default(0);
            $table->decimal('canteenrecotonnes', 10, 4)->default(0);
            $table->mediumText('canteennotereco')->nullable();
            $table->decimal('vegperreco', 10, 4)->default(0);
            $table->decimal('vegrecotonnes', 10, 4)->default(0);
            $table->mediumText('vegnotereco')->nullable();
            $table->integer('AssignedToUser')->default(0);
            $table->integer('ApprovedBy')->nullable();
            $table->dateTime('breakdownRequested')->nullable();

            $table->index(['WRStatus', 'StartDate'], 'tblwastereports_WRStatus_StartDate_index');
            $table->index(['StartDate', 'EndDate'], 'tblwastereports_StartDate_EndDate_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblwastereports');
    }
}
