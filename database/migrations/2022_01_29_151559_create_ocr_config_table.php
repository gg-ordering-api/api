<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcrConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocr_config', function (Blueprint $table) {
            $table->integer('id')->nullable();
            $table->tinyInteger('use_cut_off')->nullable()->default(1);
            $table->integer('import_limit')->default(5);
            $table->integer('import_timeout')->nullable()->default(10);
            $table->integer('cron_active')->nullable()->default(0);
            $table->integer('batch_size')->nullable()->default(5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocr_config');
    }
}
