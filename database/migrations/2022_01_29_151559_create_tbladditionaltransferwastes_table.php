<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbladditionaltransferwastesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbladditionaltransferwastes', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('FacilityID')->index('FacilityID');
            $table->unsignedBigInteger('WasteType')->default('0');
            $table->string('RecyclePercent', 10)->nullable();
            $table->string('RecycleNotes', 200)->nullable();
            $table->string('RecoverPercent', 10)->nullable();
            $table->string('RecoverNotes', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbladditionaltransferwastes');
    }
}
