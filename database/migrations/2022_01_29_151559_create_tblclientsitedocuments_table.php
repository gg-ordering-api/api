<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblclientsitedocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblclientsitedocuments', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('DocName', 500)->nullable();
            $table->string('FileName', 500)->nullable();
            $table->string('Extension', 5)->nullable();
            $table->string('FileType', 10000)->nullable();
            $table->timestamp('DateAdded')->useCurrent();
            $table->unsignedBigInteger('AddedBy')->default('0');
            $table->unsignedBigInteger('SiteID')->default('0')->index('SiteID');
            $table->boolean('DocStatus')->default(false);
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'failed', 'yes'])->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblclientsitedocuments');
    }
}
