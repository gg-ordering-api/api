<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblbinsundriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblbinsundries', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('ClientID')->default(0);
            $table->unsignedBigInteger('SiteID')->default('0')->index('SiteID');
            $table->mediumText('SundryName')->nullable();
            $table->dateTime('SundryDate')->nullable()->default('0000-00-00 00:00:00');
            $table->decimal('ClientCharge', 10)->default(0);
            $table->boolean('CreditDebit')->default(false);
            $table->integer('RaisedBy')->default(0);
            $table->boolean('IsInvoiced')->default(false);
            $table->dateTime('InvoicedOn')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('IsDeleted')->default(false);
            $table->integer('DeletedBy')->default(0);
            $table->decimal('SupplierCharge', 10)->default(0);
            $table->string('InvoiceNumber', 50)->nullable()->index('InvoiceNumber');
            $table->boolean('IsPaid')->default(false);
            $table->integer('UsedSupplier')->default(0)->index('UsedSupplier');
            $table->string('SupplierContact', 200)->nullable();
            $table->boolean('IsApproved')->default(false);
            $table->integer('ApprovedBy')->default(0);
            $table->dateTime('ApprovalDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('ApprovalReason')->nullable();
            $table->decimal('SundryMargin', 10)->default(0);
            $table->mediumText('SundryDetails')->nullable();
            $table->string('SupplierInvNo', 100)->nullable()->index('SupplierInvNo');
            $table->smallInteger('SuppDisputeType')->default(0)->index('SuppDisputeType');
            $table->mediumText('SupplierDisputeReason')->nullable();
            $table->mediumText('SupplierDisputeClearReason')->nullable();
            $table->string('SuppTicketNo', 100)->nullable();
            $table->boolean('SuppIsDisputed')->default(false)->index('tblbinsundries_SuppIsDisputed_index');
            $table->dateTime('SuppDateDisputed')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SuppDisputedBy')->default(0)->index('SuppDisputedBy');
            $table->dateTime('SuppDateDisputeCleared')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SuppDisputeClearedBy')->default(0);
            $table->boolean('SuppIsPaid')->default(false);
            $table->dateTime('SuppPaidOn')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SuppPaidBy')->default(0);
            $table->integer('SuppInvCheckedBy')->default(0);
            $table->dateTime('SuppCheckDate')->nullable()->default('0000-00-00 00:00:00');
            $table->smallInteger('ClientDisputeType')->default(0);
            $table->mediumText('ClientDisputeReason')->nullable();
            $table->integer('ClientDisputedBy')->default(0);
            $table->boolean('ClientIsDisputed')->default(false)->index('bs_clientdisputed');
            $table->dateTime('ClientDateDisputed')->nullable()->default('0000-00-00 00:00:00')->index('tblbinsundries_ClientDateDisputed_index');
            $table->dateTime('ClientDateDisputeCleared')->nullable()->default('0000-00-00 00:00:00')->index('tblbinsundries_ClientDateDisputeCleared_index');
            $table->integer('ClientDisputeClearedBy')->default(0);
            $table->mediumText('ClientDisputeClearReason')->nullable();
            $table->boolean('BagPurchase')->default(false);
            $table->integer('LeadOwnerBDM')->default(0);
            $table->string('CommercialPO', 50)->nullable();
            $table->integer('TicketRelated')->nullable()->default(0);
            $table->mediumText('SundryReason')->nullable();
            $table->mediumText('DisputerName')->nullable();
            $table->mediumText('DisputerContact')->nullable();
            $table->mediumText('ClientDisputerName')->nullable();
            $table->mediumText('ClientDisputerContact')->nullable();
            $table->timestamp('CreationDate')->useCurrent();
            $table->decimal('SuppDisputeAdjust', 7)->default(0);
            $table->boolean('DoNotInvoice')->default(false);
            $table->integer('InvBlockedBy')->default(0);
            $table->string('dispute_extra', 240)->nullable();
            $table->string('supplier_transfer_ticket_number')->nullable();

            $table->index(['SuppDateDisputed', 'IsDeleted'], 'tblbinsundries_SuppDateDisputed_IsDeleted_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblbinsundries');
    }
}
