<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodonotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todonotes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('todo_id')->index('todonotes_todo_id_foreign');
            $table->unsignedInteger('user_id')->index('todonotes_user_id_foreign');
            $table->char('note')->nullable();
            $table->tinyInteger('deleted')->default(0);
            $table->dateTime('timestamp')->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todonotes');
    }
}
