<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblloginattemptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblloginattempts', function (Blueprint $table) {
            $table->string('user_id')->primary();
            $table->integer('attempts')->nullable();
            $table->dateTime('time')->default('0000-00-00 00:00:00');
            $table->string('last_ip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblloginattempts');
    }
}
