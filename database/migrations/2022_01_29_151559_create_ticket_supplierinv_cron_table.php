<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketSupplierinvCronTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_supplierinv_cron', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('go_green_order_id')->nullable()->index('ticket_supplierinv_cron_gogreen_order_number_id_fk');
            $table->integer('completed')->nullable()->default(0)->index();
            $table->timestamp('created')->useCurrent();
            $table->timestamp('completed_at')->default('0000-00-00 00:00:00');

            $table->index(['completed', 'go_green_order_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_supplierinv_cron');
    }
}
