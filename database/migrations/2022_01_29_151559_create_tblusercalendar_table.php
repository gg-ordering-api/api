<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblusercalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblusercalendar', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedInteger('UserID')->index('UserID');
            $table->unsignedInteger('TypeID')->index('TypeID');
            $table->dateTime('Start')->default('0000-00-00 00:00:00');
            $table->dateTime('End')->default('0000-00-00 00:00:00');
            $table->float('Duration', 10, 0)->unsigned();
            $table->string('Description', 100)->nullable();
            $table->dateTime('RequestedDate')->default('0000-00-00 00:00:00');
            $table->unsignedInteger('ManagerID')->nullable();
            $table->unsignedInteger('ManagerAction')->nullable();
            $table->dateTime('ManagerActionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->string('ManagerComment', 100)->nullable();
            $table->string('RejectReason', 1000)->nullable();
            $table->unsignedInteger('HRID')->nullable();
            $table->unsignedInteger('HRAction')->nullable();
            $table->dateTime('HRActionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->string('HRComment', 100)->nullable();
            $table->string('HRRejectReason', 100)->nullable();
            $table->dateTime('CancellationDate')->nullable()->default('0000-00-00 00:00:00');
            $table->unsignedInteger('StatusID')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblusercalendar');
    }
}
