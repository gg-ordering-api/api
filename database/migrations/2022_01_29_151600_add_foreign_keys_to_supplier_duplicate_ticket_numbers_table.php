<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSupplierDuplicateTicketNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier_duplicate_ticket_numbers', function (Blueprint $table) {
            $table->foreign(['supplier_id'], 'supplier_duplicate_ticket_numbers_tblsuppliers_ID_fk')->references(['ID'])->on('tblsuppliers');
            $table->foreign(['updated_by'], 'supplier_duplicate_ticket_numbers_tblusers_ID_fk_2')->references(['ID'])->on('tblusers');
            $table->foreign(['gogreen_order_number_id'], 'supplier_duplicate_ticket_numbers_gogreen_order_number_id_fk')->references(['id'])->on('gogreen_order_number');
            $table->foreign(['created_by'], 'supplier_duplicate_ticket_numbers_tblusers_ID_fk')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supplier_duplicate_ticket_numbers', function (Blueprint $table) {
            $table->dropForeign('supplier_duplicate_ticket_numbers_tblsuppliers_ID_fk');
            $table->dropForeign('supplier_duplicate_ticket_numbers_tblusers_ID_fk_2');
            $table->dropForeign('supplier_duplicate_ticket_numbers_gogreen_order_number_id_fk');
            $table->dropForeign('supplier_duplicate_ticket_numbers_tblusers_ID_fk');
        });
    }
}
