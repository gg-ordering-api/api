<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsuppliers', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('SupplierName', 300)->nullable()->index('SupplierName');
            $table->string('SupplierAddress', 2000)->nullable();
            $table->string('SupplierPostCode', 20)->nullable();
            $table->integer('country_id')->nullable()->default(1)->index('tblsuppliers_countries_id_fk');
            $table->string('Telephone')->nullable();
            $table->string('Fax', 60)->nullable();
            $table->string('Email', 200)->nullable();
            $table->string('CompanyRegNo', 12)->nullable();
            $table->string('VATNumber', 18)->nullable();
            $table->tinyInteger('EmailOrders')->default(0);
            $table->string('WCL', 200)->nullable();
            $table->dateTime('WCLExpire')->default('0000-00-00 00:00:00');
            $table->unsignedBigInteger('WCLLicense')->default('0');
            $table->unsignedBigInteger('EAWCL')->default('0');
            $table->integer('SupplierStatus')->default(0);
            $table->tinyInteger('AccountsAudit')->default(0);
            $table->tinyInteger('ComplianceAudit')->default(0);
            $table->tinyInteger('FinalApproval')->default(0);
            $table->boolean('ChargesToWeigh')->default(false);
            $table->boolean('CanWeigh')->default(false);
            $table->mediumText('RecycleNotes')->nullable();
            $table->unsignedBigInteger('PLDocument')->default('0');
            $table->boolean('RequestWTN')->default(false);
            $table->string('ComplianceAuditBy', 200)->nullable();
            $table->string('SupplierContact', 200)->nullable();
            $table->dateTime('AuditDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('ComplianceNote')->nullable();
            $table->unsignedBigInteger('HSDocument')->default('0');
            $table->string('SuppLat', 40)->nullable();
            $table->string('SuppLong', 40)->nullable();
            $table->string('AccountNumber', 50)->nullable();
            $table->string('Sortcode', 10)->nullable();
            $table->string('InvoicePeriod', 20)->nullable();
            $table->boolean('DelRem')->default(false);
            $table->boolean('TermsAgreed')->default(false);
            $table->string('WithWho', 100)->nullable();
            $table->mediumText('AccountsNotes')->nullable();
            $table->boolean('IsHaz')->default(false);
            $table->decimal('WeightCharge', 5)->default(0);
            $table->mediumText('HazNotes')->nullable();
            $table->boolean('QuestOrSCR')->nullable();
            $table->unsignedBigInteger('WTN')->default('0');
            $table->dateTime('WTNExpire')->nullable()->default('0000-00-00 00:00:00');
            $table->decimal('PLValue', 20)->default(0);
            $table->dateTime('PLExpiry')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('SupplierAlert')->nullable();
            $table->tinyInteger('IsDeleted')->default(0);
            $table->string('InvoiceGroup', 30)->nullable();
            $table->boolean('RoRoDelRem')->default(false);
            $table->decimal('OurCreditLimit', 20)->default(0);
            $table->boolean('IsProblem')->default(false);
            $table->mediumText('ProblemReason')->nullable();
            $table->dateTime('DateProblemSet')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('RequestReviewed')->default(false);
            $table->timestamp('RequestDate')->useCurrent();
            $table->smallInteger('OurPaymentTerms')->default(0);
            $table->boolean('BreakdownSupplied')->default(false);
            $table->smallInteger('BreakdownFreq')->default(2);
            $table->mediumText('BuyerNotes')->nullable();
            $table->dateTime('ProsecutionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('ProsecutionNote')->nullable();
            $table->boolean('FixedPrice')->default(false);
            $table->unsignedBigInteger('PriceList')->default('0');
            $table->boolean('pricelist_enabled')->nullable();
            $table->bigInteger('BinPriceList')->nullable();
            $table->integer('OpsRating')->default(5);
            $table->mediumText('OpsReason');
            $table->dateTime('OpsReasonDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('AccountsRating')->default(5);
            $table->mediumText('AccountsReason');
            $table->dateTime('AccountsReasonDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('RebateSupplier')->default(false);
            $table->string('OurAccountNumber', 20)->nullable();
            $table->integer('MillerSWColumn')->default(0);
            $table->dateTime('LastComplianceContact')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('RequestingUser')->default(0)->index('RequestingUser');
            $table->tinyInteger('IsPrioritySupplier')->default(0);
            $table->integer('Buyer')->default(0);
            $table->mediumText('DisputeEmail');
            $table->boolean('Clocs')->default(false);
            $table->dateTime('EAWCLExpire')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('IsCraneTest')->default(0);
            $table->dateTime('CraneExpiry')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('HasPortal')->default(0);
            $table->string('ShortCode', 10)->nullable();
            $table->integer('AccountsHandler')->default(0)->index('AccountsHandler');
            $table->boolean('IncreasesApplied')->default(false);
            $table->mediumText('AccountsNote')->nullable();
            $table->tinyInteger('HasTracker')->default(0);
            $table->dateTime('ProsecutionExpiryDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('PDATickets')->default(false);
            $table->text('ScheduleNotificationEmail')->nullable();
            $table->string('EmailDomain', 300)->nullable();
            $table->smallInteger('WTRPaymentType')->nullable()->default(0);
            $table->integer('autoInvoiceCheckingUser')->default(0);
            $table->text('WTRComments')->nullable();
            $table->integer('PrimaryContact')->nullable()->default(0);
            $table->tinyInteger('isNational')->default(0);
            $table->string('Fors', 15)->nullable();
            $table->dateTime('AccountsApprovalDate')->nullable()->default('0000-00-00 00:00:00');
            $table->unsignedBigInteger('service_level_agreement_document')->default('0');
            $table->dateTime('service_level_agreement_date')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('AccountsApprovalPriority')->nullable();
            $table->dateTime('AccountsApprovalRequestDate')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('skip_software')->nullable();
            $table->tinyInteger('weighbridge_software')->nullable();
            $table->text('software_notes')->nullable();
            $table->boolean('drivers_dbs_checked')->nullable()->default(false);
            $table->dateTime('fors_expirary')->nullable();
            $table->dateTime('IncreaseCompleteDate')->nullable();
            $table->integer('email_tickets')->default(0);
            $table->integer('email_invoices')->default(0);
            $table->integer('post_tickets')->default(0);
            $table->integer('post_invoices')->default(0);
            $table->integer('weigh_bridge_all_tickets')->default(0);
            $table->integer('digital_or_ocr')->default(1);
            $table->integer('ticket_preference')->nullable()->default(0);
            $table->integer('invoice_preference')->nullable()->default(0);
            $table->boolean('accepting_wo_auto_order')->nullable()->default(false);
            $table->integer('daily_skip_quota')->nullable();
            $table->string('auto_supplier_email', 100)->nullable();
            $table->boolean('vat_registered')->nullable()->default(true);
            $table->string('weight_request_email')->nullable();
            $table->string('breakdown_request_email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsuppliers');
    }
}
