<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGogreenOrderReturnReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gogreen_order_return_reasons', function (Blueprint $table) {
            $table->foreign(['go_green_order_number_id'], 'gogreen_order_return_reasons_gogreen_order_number_id_fk')->references(['id'])->on('gogreen_order_number');
            $table->foreign(['created_by'], 'gogreen_order_return_reasons_tblusers_ID_fk')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gogreen_order_return_reasons', function (Blueprint $table) {
            $table->dropForeign('gogreen_order_return_reasons_gogreen_order_number_id_fk');
            $table->dropForeign('gogreen_order_return_reasons_tblusers_ID_fk');
        });
    }
}
