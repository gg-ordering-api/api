<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblbinscheduleordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblbinscheduleorders', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('OrderCode', 50)->nullable();
            $table->bigInteger('ClientID')->default(0);
            $table->bigInteger('SiteID')->default(0)->index('SiteID');
            $table->bigInteger('DocID')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblbinscheduleorders');
    }
}
