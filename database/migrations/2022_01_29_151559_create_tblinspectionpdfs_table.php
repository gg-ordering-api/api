<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblinspectionpdfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblinspectionpdfs', function (Blueprint $table) {
            $table->increments('ID');
            $table->mediumText('DocName');
            $table->mediumText('FileName');
            $table->mediumText('Extension');
            $table->integer('FileType');
            $table->timestamp('DateAdded')->useCurrent();
            $table->integer('VisitID')->default(0);
            $table->integer('SiteID')->default(0);
            $table->integer('TransferID')->default(0);
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'yes', 'failed'])->nullable()->default('no');
            $table->integer('site_visit_history_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblinspectionpdfs');
    }
}
