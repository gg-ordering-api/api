<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTbltransferstationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbltransferstations', function (Blueprint $table) {
            $table->foreign(['country_id'], 'tbltransferstations_countries_id_fk')->references(['id'])->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbltransferstations', function (Blueprint $table) {
            $table->dropForeign('tbltransferstations_countries_id_fk');
        });
    }
}
