<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsupplieremailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsupplieremails', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('subject', 2000)->nullable();
            $table->text('emailBody')->nullable();
            $table->string('senderName', 300)->nullable();
            $table->integer('supplierID')->default(0);
            $table->dateTime('emailDateTime')->nullable()->default('0000-00-00 00:00:00');
            $table->timestamp('dateCreated')->useCurrent();
            $table->dateTime('dateModified')->nullable()->default('0000-00-00 00:00:00');
            $table->string('EmailDomain', 300)->nullable();
            $table->string('EmailWithoutDomain', 300)->nullable();
            $table->string('EmailMsgID', 500)->nullable();
            $table->string('originalFileName', 500)->nullable();
            $table->string('changedFileName', 500)->nullable();
            $table->tinyInteger('stage')->default(0)->index('stage')->comment('0=read,1=moved');
            $table->integer('movedBy')->default(0)->comment('userID who moved it from temp folder to supplier linkage folder');
            $table->tinyInteger('is_deleted')->default(0);
            $table->text('note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsupplieremails');
    }
}
