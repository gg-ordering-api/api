<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBinPriceIncreaseLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bin_price_increase_log', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('bin_schedule_id')->nullable();
            $table->enum('type', ['supplier', 'client'])->nullable();
            $table->string('old_lift_price')->nullable();
            $table->string('new_lift_price')->nullable();
            $table->string('old_per_tonne')->nullable();
            $table->string('new_per_tonne')->nullable();
            $table->dateTime('increase_date')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bin_price_increase_log');
    }
}
