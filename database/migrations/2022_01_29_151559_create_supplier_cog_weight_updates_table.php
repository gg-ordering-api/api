<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierCogWeightUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_cog_weight_updates', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('client_ticket')->nullable();
            $table->integer('order_number')->nullable();
            $table->integer('supplier_ticket')->nullable();
            $table->float('weight_before', 10, 0)->nullable();
            $table->float('weight_now', 10, 0)->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_cog_weight_updates');
    }
}
