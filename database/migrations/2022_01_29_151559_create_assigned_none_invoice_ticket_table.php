<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignedNoneInvoiceTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_none_invoice_ticket', function (Blueprint $table) {
            $table->integer('id', true);
            $table->unsignedInteger('assignee')->nullable()->index('assigned_none_invoice_ticket_tblusers_ID_fk');
            $table->integer('go_green_ticket_id')->nullable()->index('assigned_none_invoice_ticket_gogreen_order_number_id_fk');
            $table->timestamp('created_date')->useCurrentOnUpdate()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_none_invoice_ticket');
    }
}
