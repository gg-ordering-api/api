<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsupplierordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsupplierorders', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('OrderCode', 50)->nullable()->index('OrderCode');
            $table->integer('SupplierID')->default(0);
            $table->bigInteger('SiteID')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsupplierorders');
    }
}
