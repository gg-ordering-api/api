<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblquotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblquotes', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SiteID')->default('0')->index('SiteID');
            $table->unsignedBigInteger('ClientID')->default('0')->index('ClientID');
            $table->unsignedInteger('AssignedTo')->default('0')->index('AssignedTo');
            $table->integer('RequestedBy')->default(0)->index('RequestedBy');
            $table->timestamp('RequestDate')->useCurrent()->index('tblquotes_RequestDate_index');
            $table->tinyInteger('LateWarn')->default(0);
            $table->string('QuotedTo', 200)->nullable();
            $table->string('ContactDetails', 200)->nullable();
            $table->tinyInteger('ContactMethod')->default(0);
            $table->smallInteger('QuoteStatus')->default(0);
            $table->integer('MarginedBy')->default(0)->index('MarginedBy');
            $table->mediumText('BuyerNotes')->nullable();
            $table->smallInteger('QuotePriority')->default(2);
            $table->mediumText('QuoteCompetitors')->nullable();
            $table->mediumText('RejectionReason')->nullable();
            $table->mediumText('LossReason')->nullable();
            $table->dateTime('DateSentToManager')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('SentToClient')->default(false);
            $table->dateTime('SentToClientDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('NotesFromBuyers')->nullable();
            $table->mediumText('ChaseNotes')->nullable();
            $table->boolean('BuyerRenegotiation')->default(false);
            $table->integer('LossReasonID')->default(0);
            $table->mediumText('ComplianceNotes')->nullable();
            $table->integer('TimeRequired')->default(0);
            $table->dateTime('DateReturnedToBuyer')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('ReturnedToBuyerFrom')->default(0);
            $table->mediumText('BuyerNoteInternal')->nullable();
            $table->boolean('AutoBounceBack')->default(false);
            $table->dateTime('WonDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('LossDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('QuoteNotes')->nullable();
            $table->integer('contactID')->nullable()->index('contactID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblquotes');
    }
}
