<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlaLookupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sla_lookup', function (Blueprint $table) {
            $table->smallInteger('ID', true)->unique('ID');
            $table->integer('DocumentID')->unique('DocumentID');
            $table->time('Exchange')->nullable();
            $table->time('Delivery')->nullable();
            $table->string('Removal', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sla_lookup');
    }
}
