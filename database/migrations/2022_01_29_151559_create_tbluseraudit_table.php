<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbluserauditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbluseraudit', function (Blueprint $table) {
            $table->bigInteger('UserID')->default(0)->index('UserID');
            $table->string('PageViewed', 150)->nullable();
            $table->mediumText('QueryData')->nullable();
            $table->timestamp('TimeOfView')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbluseraudit');
    }
}
