<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSupplierPortalCredentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier_portal_credentials', function (Blueprint $table) {
            $table->foreign(['supplier_id'], 'supplier_portal_credentials_tblsuppliers_ID_fk')->references(['ID'])->on('tblsuppliers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supplier_portal_credentials', function (Blueprint $table) {
            $table->dropForeign('supplier_portal_credentials_tblsuppliers_ID_fk');
        });
    }
}
