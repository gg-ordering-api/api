<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblexportsettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblexportsettings', function (Blueprint $table) {
            $table->integer('LastImportedSite')->default(0);
            $table->integer('LastImportedMovement')->default(0);
            $table->integer('LastImportedSundry')->default(0);
            $table->integer('LastImportedStore')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblexportsettings');
    }
}
