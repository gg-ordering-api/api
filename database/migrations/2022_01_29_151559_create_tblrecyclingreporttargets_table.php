<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblrecyclingreporttargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblrecyclingreporttargets', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('RRTarget', 100)->nullable();
            $table->string('RowColour', 8)->default('#000000');
            $table->integer('status')->nullable()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblrecyclingreporttargets');
    }
}
