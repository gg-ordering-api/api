<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientInvoicingLogTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_invoicing_log_tickets', function (Blueprint $table) {
            $table->foreign(['error_id'], 'client_invoicing_log_tickets_client_invoicing_error_types_id_fk')->references(['id'])->on('client_invoicing_error_types');
            $table->foreign(['status_id'], 'client_invoicing_log_tickets_client_invoicing_status_id_fk')->references(['id'])->on('client_invoicing_status');
            $table->foreign(['log_id'], 'client_invoicing_log_tickets_client_invoicing_log_id_fk')->references(['id'])->on('client_invoicing_log')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['gg_order_number_id'], 'client_invoicing_log_tickets_gogreen_order_number_id_fk')->references(['id'])->on('gogreen_order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_invoicing_log_tickets', function (Blueprint $table) {
            $table->dropForeign('client_invoicing_log_tickets_client_invoicing_error_types_id_fk');
            $table->dropForeign('client_invoicing_log_tickets_client_invoicing_status_id_fk');
            $table->dropForeign('client_invoicing_log_tickets_client_invoicing_log_id_fk');
            $table->dropForeign('client_invoicing_log_tickets_gogreen_order_number_id_fk');
        });
    }
}
