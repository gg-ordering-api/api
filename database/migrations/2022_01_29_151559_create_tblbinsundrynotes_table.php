<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblbinsundrynotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblbinsundrynotes', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('SunID')->default(0)->index('TID');
            $table->mediumText('NoteBody')->nullable();
            $table->timestamp('CreatedOn')->useCurrent();
            $table->integer('CreatedBy')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblbinsundrynotes');
    }
}
