<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoGreenOrderNumberCogSupplierReportAssignedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('go_green_order_number_cog_supplier_report_assigned', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('go_green_order_number_id')->nullable()->index('go_green_order_number_cog__id_fk');
            $table->unsignedInteger('user_id')->nullable()->index('go_green_order_number_tblusers_ID_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('go_green_order_number_cog_supplier_report_assigned');
    }
}
