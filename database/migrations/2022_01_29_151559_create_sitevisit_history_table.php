<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitevisitHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitevisit_history', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('site_id')->default(0)->index('site_id');
            $table->integer('visitor_id')->default(0)->index('visitor_id');
            $table->dateTime('date_visited')->nullable()->default('0000-00-00 00:00:00');
            $table->text('note')->nullable();
            $table->tinyInteger('is_deleted')->default(0)->index('is_deleted');
            $table->integer('deleted_by')->default(0)->index('deleted_by');
            $table->integer('transferstation_id')->default(0)->index('transferstation_id')->comment('foreign key of tbltransferstations');
            $table->tinyInteger('type')->default(0)->index('type')->comment('0=Site for Client, 1=Transferstation for supplier');
            $table->string('address_at_time_of_visit')->nullable();
            $table->integer('email_counter')->default(0)->comment('Used to count how many times email sent');
            $table->integer('email_sent_by')->default(0);
            $table->text('suggested_note')->nullable();
            $table->integer('suggested_by')->default(0)->index('suggested_by');
            $table->integer('status')->default(0)->index('status')->comment('0=suggested,1=booked');
            $table->boolean('is_cron')->default(false)->index('is_cron')->comment('0=Inserted manually,1=inserted by cron');
            $table->tinyInteger('is_scheduled')->default(0)->index('is_scheduled');
            $table->dateTime('date_created')->nullable()->useCurrent();
            $table->dateTime('date_modified')->nullable()->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitevisit_history');
    }
}
