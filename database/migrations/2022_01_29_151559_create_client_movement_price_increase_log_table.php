<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientMovementPriceIncreaseLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_movement_price_increase_log', function (Blueprint $table) {
            $table->integer('id', true);
            $table->unsignedInteger('client_id')->nullable()->index('client_movement_price_increase_log_tblclients_ID_fk');
            $table->integer('increase_type')->nullable();
            $table->boolean('client_informed')->nullable()->default(false);
            $table->string('trans_increase', 20)->nullable();
            $table->string('wl_increase', 20)->nullable();
            $table->string('per_ton_increase', 20)->nullable();
            $table->text('ignored_sites')->nullable();
            $table->text('ignored_containers')->nullable();
            $table->dateTime('price_increase_from_date')->nullable();
            $table->unsignedInteger('created_by')->nullable()->index('client_movement_price_increase_log_tblusers_ID_fk');
            $table->timestamp('created_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_movement_price_increase_log');
    }
}
