<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWasteReportSiteNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waste_report_site_notes', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('site_id')->index();
            $table->text('note')->nullable();
            $table->dateTime('created_on')->nullable()->useCurrent()->index();
            $table->integer('created_by')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste_report_site_notes');
    }
}
