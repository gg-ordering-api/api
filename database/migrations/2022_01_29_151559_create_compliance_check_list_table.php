<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplianceCheckListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compliance_check_list', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('gogreen_order_number_id')->nullable()->index('compliance_check_list_gogreen_order_number_id_fk');
            $table->integer('rejected_reason_id')->nullable()->index('compliance_check_list_compliance_rejected_reasons_id_fk');
            $table->enum('state', ['completed', 'rejected']);
            $table->enum('team', ['accounts', 'compliance'])->nullable();
            $table->longText('checked_data')->nullable();
            $table->longText('note')->nullable();
            $table->unsignedInteger('created_by')->nullable()->index('compliance_check_list_tblusers_ID_fk');
            $table->timestamp('created_at')->useCurrent();
            $table->unsignedInteger('updated_by')->nullable()->index('compliance_check_list_tblusers_ID_fk_2');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->unsignedInteger('assigned')->nullable()->index('compliance_check_list_tblusers_ID_fk_3');
            $table->integer('parent_ticket_id')->nullable();
            $table->timestamp('completed_date')->nullable();
            $table->unsignedInteger('completed_by')->nullable()->index('compliance_check_list_tblusers_ID_fk_4');
            $table->boolean('with_supplier')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compliance_check_list');
    }
}
