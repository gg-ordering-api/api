<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblwasteamendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblwasteamends', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedInteger('ReportID')->default('0')->index('ReportID');
            $table->timestamp('DateCreated')->useCurrent();
            $table->unsignedInteger('CreatedBy')->nullable();
            $table->integer('waste_amend_type_id')->nullable();

            $table->index(['DateCreated', 'waste_amend_type_id', 'CreatedBy'], 'tblwasteamends_DateCreated_waste_amend_type_id_CreatedBy_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblwasteamends');
    }
}
