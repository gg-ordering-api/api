<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblclientsitewtnpodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblclientsitewtnpod', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('DocName', 500)->nullable()->index('DocName');
            $table->string('FileName', 500)->nullable();
            $table->string('Extension', 5)->nullable();
            $table->string('FileType', 10000)->nullable();
            $table->timestamp('DateAdded')->useCurrent();
            $table->unsignedBigInteger('AddedBy')->default('0');
            $table->unsignedBigInteger('SiteID')->default('0')->index('SiteID');
            $table->bigInteger('FileSize')->default(0);
            $table->boolean('show')->nullable()->default(true);
            $table->integer('gogreen_order_id')->nullable()->index('tblclientsitewtnpod_gogreen_order_number_id_fk');
            $table->integer('iswtn')->nullable()->default(0);
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'failed', 'yes'])->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblclientsitewtnpod');
    }
}
