<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblwastetypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblwastetypes', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('WasteName', 2000)->nullable();
            $table->string('WasteCode', 200)->nullable();
            $table->boolean('IsHaz')->default(false);
            $table->boolean('IsMetal')->default(false)->index('tblwastetypes_IsMetal_index');
            $table->decimal('DensityFactor', 10, 4)->default(0);
            $table->boolean('IsVarious')->default(false);
            $table->string('MillerSWColumn', 3);
            $table->boolean('ComplianceApproval')->default(false);
            $table->boolean('IsDifficult')->default(false);
            $table->tinyInteger('do_not_pull_rr')->default(0)->index('do_not_pull_rr');
            $table->integer('weight_required')->nullable()->default(1);
            $table->integer('primary_waste_type_id')->nullable();
            $table->float('recycledPercent', 10, 0)->nullable();
            $table->float('recoveredPercent', 10, 0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblwastetypes');
    }
}
