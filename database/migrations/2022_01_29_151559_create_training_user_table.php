<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_user', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('training_item_id')->index('training_user_training_item_id_foreign');
            $table->unsignedInteger('user_id')->index('training_user_user_id_foreign');
            $table->unsignedInteger('training_status_id')->default('0')->index('training_user_training_status_id_foreign');
            $table->dateTime('due_date')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('expiry_date')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('employee_trained_completed_date')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('manager_trained_completed_date')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('deleted')->default(0);
            $table->timestamp('created_timestamp')->useCurrentOnUpdate()->useCurrent();
            $table->integer('created_by');
            $table->timestamp('updated_timestamp')->useCurrent();
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_user');
    }
}
