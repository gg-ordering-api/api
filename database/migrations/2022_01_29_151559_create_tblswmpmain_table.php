<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblswmpmainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblswmpmain', function (Blueprint $table) {
            $table->unsignedBigInteger('SiteID')->default('0')->primary();
            $table->string('ContractorName', 300)->nullable();
            $table->string('ContractorCompany', 300)->nullable();
            $table->string('ContractorJobTitle', 300)->nullable();
            $table->string('ContractorDate', 300)->nullable();
            $table->string('ClientName', 300)->nullable();
            $table->string('ClientCompany', 300)->nullable();
            $table->string('ClientJobTitle', 300)->nullable();
            $table->string('ClientDate', 300)->nullable();
            $table->string('CompletionName', 300)->nullable();
            $table->string('CompletionCompany', 300)->nullable();
            $table->string('CompletionJobTitle', 300)->nullable();
            $table->string('CompletionDate', 300)->nullable();
            $table->string('PrincipalContractor', 300)->nullable();
            $table->mediumText('WasteTargets')->nullable();
            $table->mediumText('PreviousPerformance')->nullable();
            $table->mediumText('MinimisationInitiatives')->nullable();
            $table->mediumText('ContainmentOfWaste')->nullable();
            $table->mediumText('WasteStrategy')->nullable();
            $table->string('WasteChampion', 200)->nullable();
            $table->mediumText('TrainingInduction')->nullable();
            $table->string('TrainingOther', 1000)->nullable();
            $table->mediumText('OnSiteWaste')->nullable();
            $table->string('DateofWasteReview', 100)->nullable();
            $table->mediumText('SWMPDeviation')->nullable();
            $table->mediumText('SWMPSavings')->nullable();
            $table->mediumText('SmallReview')->nullable();
            $table->mediumText('WasteNotes')->nullable();
            $table->mediumText('GoGreenFeedback')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblswmpmain');
    }
}
