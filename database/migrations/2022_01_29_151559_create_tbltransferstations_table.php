<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbltransferstationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbltransferstations', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SupplierID')->index('SupplierID');
            $table->dateTime('ReportDate')->nullable()->default('0000-00-00 00:00:00');
            $table->string('StationName', 200)->nullable();
            $table->string('StationAddress', 500)->nullable();
            $table->string('StationPostcode', 10)->nullable();
            $table->string('WMLLicense', 200)->nullable();
            $table->boolean('CanWeigh')->default(false);
            $table->decimal('AverageGate', 5)->default(0);
            $table->mediumText('AdditionalNotes')->nullable();
            $table->string('aggper', 10)->nullable();
            $table->string('aggnote', 200)->nullable();
            $table->string('hardcoreper', 10)->nullable();
            $table->string('hardcorenote', 200)->nullable();
            $table->string('ceramicsper', 10)->nullable();
            $table->string('ceramicsnote', 200)->nullable();
            $table->string('tilesper', 10)->nullable();
            $table->string('tilesnote', 200)->nullable();
            $table->string('soilper', 10)->nullable();
            $table->string('soilnote', 200)->nullable();
            $table->string('cardboardper', 10)->nullable();
            $table->string('cardboardnote', 200)->nullable();
            $table->string('paperper', 10)->nullable();
            $table->string('papernote', 200)->nullable();
            $table->string('softper', 10)->nullable();
            $table->string('softnote', 200)->nullable();
            $table->string('hardper', 10)->nullable();
            $table->string('hardnote', 200)->nullable();
            $table->string('plateper', 10)->nullable();
            $table->string('platenote', 200)->nullable();
            $table->string('glassbottlesper', 10)->nullable();
            $table->string('glassbottlesnote', 200)->nullable();
            $table->string('timberper', 10)->nullable();
            $table->string('timbernote', 200)->nullable();
            $table->string('mdfper', 10)->nullable();
            $table->string('mdfnote', 200)->nullable();
            $table->string('plyper', 10)->nullable();
            $table->string('plynote', 200)->nullable();
            $table->string('metalper', 10)->nullable();
            $table->string('metalnote', 200)->nullable();
            $table->string('plasterper', 10)->nullable();
            $table->string('plasternote', 200)->nullable();
            $table->string('insulationper', 10)->nullable();
            $table->string('insulationnote', 200)->nullable();
            $table->string('fabricsper', 10)->nullable();
            $table->string('fabricsnote', 200)->nullable();
            $table->string('canteenper', 10)->nullable();
            $table->string('canteennote', 200)->nullable();
            $table->string('vegper', 10)->nullable();
            $table->string('vegnote', 200)->nullable();
            $table->string('otherper', 10)->nullable();
            $table->string('othernote', 200)->nullable();
            $table->unsignedBigInteger('CreatedBy')->default('0');
            $table->timestamp('InitialCreation')->useCurrent();
            $table->boolean('IsDeleted')->default(false);
            $table->string('TransLat', 50)->default('0');
            $table->string('TransLong', 50)->default('0');
            $table->integer('DestinationType')->default(0);
            $table->string('Telephone', 50)->nullable();
            $table->dateTime('GateExpiry')->nullable()->default('0000-00-00 00:00:00');
            $table->string('SupplierContact', 50)->nullable();
            $table->string('FaxNumber', 50)->nullable();
            $table->date('LastVisitDate')->nullable();
            $table->integer('TransferStatus')->default(0)->index('TransferStatus');
            $table->boolean('IsTemp')->default(false);
            $table->boolean('CheckNotRequired')->default(false);
            $table->mediumText('MillerSWColumn')->nullable();
            $table->mediumText('ActiveWML')->nullable();
            $table->integer('ActiveEAWML')->default(0);
            $table->boolean('SeparateDMR')->nullable()->default(false);
            $table->string('aggreco', 10)->nullable();
            $table->string('aggreconote', 200)->nullable();
            $table->string('hardcorereco', 10)->nullable();
            $table->string('hardcorereconote', 200)->nullable();
            $table->string('ceramicsreco', 10)->nullable();
            $table->string('ceramicsreconote', 200)->nullable();
            $table->string('tilesreco', 10)->nullable();
            $table->string('tilesreconote', 200)->nullable();
            $table->string('soilreco', 10)->nullable();
            $table->string('soilreconote', 200)->nullable();
            $table->string('cardboardreco', 10)->nullable();
            $table->string('cardboardreconote', 200)->nullable();
            $table->string('paperreco', 10)->nullable();
            $table->string('paperreconote', 200)->nullable();
            $table->string('softreco', 10)->nullable();
            $table->string('softreconote', 200)->nullable();
            $table->string('hardreco', 10)->nullable();
            $table->string('hardreconote', 200)->nullable();
            $table->string('platereco', 10)->nullable();
            $table->string('platereconote', 200)->nullable();
            $table->string('glassbottlesreco', 10)->nullable();
            $table->string('glassbottlesreconote', 200)->nullable();
            $table->string('timberreco', 10)->nullable();
            $table->string('timberreconote', 200)->nullable();
            $table->string('mdfreco', 10)->nullable();
            $table->string('mdfreconote', 200)->nullable();
            $table->string('plyreco', 10)->nullable();
            $table->string('plyreconote', 200)->nullable();
            $table->string('metalreco', 10)->nullable();
            $table->string('metalreconote', 200)->nullable();
            $table->string('plasterreco', 10)->nullable();
            $table->string('plasterreconote', 200)->nullable();
            $table->string('insulationreco', 10)->nullable();
            $table->string('insulationreconote', 200)->nullable();
            $table->string('fabricsreco', 10)->nullable();
            $table->string('fabricsreconote', 200)->nullable();
            $table->string('canteenreco', 10)->nullable();
            $table->string('canteenreconote', 200)->nullable();
            $table->string('vegreco', 10)->nullable();
            $table->string('vegreconote', 200)->nullable();
            $table->string('otherreco', 10)->nullable();
            $table->string('otherreconote', 200)->nullable();
            $table->decimal('GateRecovery', 5)->default(0);
            $table->boolean('PAS402Certified')->default(false);
            $table->string('epr_number', 100)->nullable();
            $table->string('pre_ea_ref', 100)->nullable();
            $table->integer('country_id')->nullable()->default(1)->index('tbltransferstations_countries_id_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbltransferstations');
    }
}
