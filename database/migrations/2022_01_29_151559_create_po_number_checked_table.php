<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePoNumberCheckedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_number_checked', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('po_number', 50)->nullable()->index();
            $table->unsignedInteger('client_id')->nullable()->index('po_number_checked_tblclients_ID_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_number_checked');
    }
}
