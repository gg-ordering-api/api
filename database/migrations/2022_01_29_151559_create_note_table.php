<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('note', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('note_type_id')->nullable()->index('note_note_type_id_fk');
            $table->integer('go_green_order_number_id')->nullable()->index('note_gogreen_order_number_id_fk');
            $table->integer('other_id')->nullable();
            $table->string('title')->nullable();
            $table->longText('note')->nullable();
            $table->unsignedInteger('created_by')->nullable()->index('note_tblusers_ID_fk');
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->tinyInteger('deleted')->nullable()->default(0);
            $table->unsignedInteger('deleted_by')->nullable()->index('note_tblusers_ID_fk_2');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('note');
    }
}
