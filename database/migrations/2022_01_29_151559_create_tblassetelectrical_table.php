<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblassetelectricalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblassetelectrical', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('AssetID')->default(0)->index('AssetID');
            $table->dateTime('CheckDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('DueDate')->nullable()->default('0000-00-00 00:00:00');
            $table->smallInteger('PassFail')->default(0);
            $table->integer('CheckedBy')->default(0);
            $table->timestamp('CheckTimeDate')->useCurrent();
            $table->mediumText('CheckNotes')->nullable();
            $table->tinyInteger('Warning')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblassetelectrical');
    }
}
