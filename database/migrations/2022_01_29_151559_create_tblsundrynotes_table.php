<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsundrynotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsundrynotes', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('SunID')->default(0)->index('SunID');
            $table->mediumText('NoteBody')->nullable();
            $table->timestamp('CreatedOn')->useCurrent();
            $table->integer('CreatedBy')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsundrynotes');
    }
}
