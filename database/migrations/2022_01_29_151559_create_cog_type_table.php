<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCogTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cog_type', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('name')->nullable();
            $table->boolean('active')->nullable()->default(true);
            $table->string('service', 155)->nullable();
            $table->string('image_path', 155)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cog_type');
    }
}
