<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientWasteTypeBreakdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_waste_type_breakdowns', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('client_id')->nullable()->index();
            $table->integer('waste_type_id')->nullable()->index();
            $table->float('breakdown', 10, 0)->nullable()->default(0);

            $table->unique(['client_id', 'waste_type_id'], 'client_waste_type_breakdowns_client_id_waste_type_id_uindex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_waste_type_breakdowns');
    }
}
