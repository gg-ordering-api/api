<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOcrTicketTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ocr_ticket_type', function (Blueprint $table) {
            $table->foreign(['cog_type_id'], 'ocr_transaction_type_cog_type_fk')->references(['id'])->on('cog_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocr_ticket_type', function (Blueprint $table) {
            $table->dropForeign('ocr_transaction_type_cog_type_fk');
        });
    }
}
