<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblcallbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblcallbacks', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('CompanyID')->default(0)->index('CompanyID');
            $table->integer('UserID')->default(0);
            $table->string('AlertTitle', 200)->nullable();
            $table->mediumText('AlertBody')->nullable();
            $table->dateTime('AlertTimeStamp')->nullable()->default('0000-00-00 00:00:00')->index('StaffActivityReport');
            $table->tinyInteger('Status')->default(0);
            $table->integer('CreatedBy')->default(0)->index('CreatedBy');
            $table->smallInteger('CallbackType')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblcallbacks');
    }
}
