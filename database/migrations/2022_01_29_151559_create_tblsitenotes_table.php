<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsitenotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsitenotes', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('NoteTitle', 500)->nullable();
            $table->mediumText('NoteBody')->nullable();
            $table->timestamp('CreatedOn')->useCurrent()->index('tblsitenotes_CreatedOn_index');
            $table->integer('CreatedBy')->nullable()->index('CreatedBy');
            $table->integer('SID')->nullable()->index('SID');
            $table->boolean('IsDeleted')->default(false)->index('tblsitenotes_IsDeleted_index');
            $table->smallInteger('NoteType')->default(0);

            $table->index(['IsDeleted', 'CreatedOn'], 'StaffActivityReport');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsitenotes');
    }
}
