<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_log', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('doc_id')->nullable();
            $table->string('prev')->nullable();
            $table->string('new_doc')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'failed', 'yes'])->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_log');
    }
}
