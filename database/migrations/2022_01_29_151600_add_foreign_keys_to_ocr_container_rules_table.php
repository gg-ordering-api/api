<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOcrContainerRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ocr_container_rules', function (Blueprint $table) {
            $table->foreign(['supplier_id'], 'ocr_container_rules_supplier_fk')->references(['ID'])->on('tblsuppliers');
            $table->foreign(['container_type_id'], 'ocr_container_rules_container_type_fk')->references(['ID'])->on('tblcontainers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocr_container_rules', function (Blueprint $table) {
            $table->dropForeign('ocr_container_rules_supplier_fk');
            $table->dropForeign('ocr_container_rules_container_type_fk');
        });
    }
}
