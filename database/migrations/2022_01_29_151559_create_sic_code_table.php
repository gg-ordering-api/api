<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSicCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sic_code', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('code')->nullable();
            $table->string('description')->nullable();
            $table->boolean('priority')->nullable()->default(false);
            $table->string('code_5')->nullable();
            $table->integer('code_5_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sic_code');
    }
}
