<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbl101Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl101', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('SiteID');
            $table->string('Year', 200);
            $table->string('Improvement', 200);
            $table->string('Target', 200);
            $table->tinyInteger('InputType');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl101');
    }
}
