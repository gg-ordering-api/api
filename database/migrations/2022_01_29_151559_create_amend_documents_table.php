<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmendDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amend_documents', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('filename')->nullable();
            $table->string('docname')->nullable();
            $table->integer('amend_id');
            $table->integer('is_deleted')->nullable()->default(0);
            $table->integer('user_id')->nullable();
            $table->dateTime('added_date')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'failed', 'yes'])->default('no');

            $table->index(['amend_id', 'is_deleted'], 'amend_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amend_documents');
    }
}
