<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToExposureTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exposure_tickets', function (Blueprint $table) {
            $table->foreign(['exposure_id'])->references(['id'])->on('credit_exposures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exposure_tickets', function (Blueprint $table) {
            $table->dropForeign('exposure_tickets_exposure_id_foreign');
        });
    }
}
