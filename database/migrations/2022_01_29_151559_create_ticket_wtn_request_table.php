<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketWtnRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_wtn_request', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('ticket_id')->nullable()->index();
            $table->integer('user_id')->nullable()->index();
            $table->string('requested_email_address')->nullable();
            $table->dateTime('timestamp')->nullable()->useCurrent()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_wtn_request');
    }
}
