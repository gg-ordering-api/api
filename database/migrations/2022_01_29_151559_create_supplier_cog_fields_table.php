<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierCogFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_cog_fields', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('supplier_cog_id')->nullable()->index('supplier_cog_fields_supplier_cog_id_fk');
            $table->integer('cog_type_id')->nullable()->index('supplier_cog_fields_cog_type_id_fk');
            $table->string('invoice_number')->nullable();
            $table->string('invoice_date')->nullable();
            $table->string('net_total')->nullable();
            $table->string('grand_total')->nullable();
            $table->string('order_number')->nullable();
            $table->string('sundry_number')->nullable();
            $table->string('supplier_ticket_number')->nullable();
            $table->string('transaction_date')->nullable();
            $table->string('weight')->nullable();
            $table->string('order_total')->nullable();
            $table->string('date_format')->nullable();
            $table->string('invoice_date_format')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_cog_fields');
    }
}
