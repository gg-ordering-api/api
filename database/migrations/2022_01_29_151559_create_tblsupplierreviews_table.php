<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsupplierreviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsupplierreviews', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id');
            $table->integer('supplier_id');
            $table->integer('rating');
            $table->longText('review_details');
            $table->dateTime('date_submitted')->default('0000-00-00 00:00:00');
            $table->integer('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsupplierreviews');
    }
}
