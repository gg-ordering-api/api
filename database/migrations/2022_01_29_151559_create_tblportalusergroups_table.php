<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblportalusergroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblportalusergroups', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('ClientID')->default(0);
            $table->string('GroupName', 50)->nullable();
            $table->boolean('CostReports')->default(false);
            $table->boolean('SkipMovements')->default(false);
            $table->boolean('ScheduleMovements')->default(false);
            $table->boolean('RecyclingReport')->default(false);
            $table->boolean('LeagueTables')->default(false);
            $table->boolean('CRMP')->default(false);
            $table->boolean('CanInvoice')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblportalusergroups');
    }
}
