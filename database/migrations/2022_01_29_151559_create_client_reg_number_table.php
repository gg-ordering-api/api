<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientRegNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_reg_number', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->index();
            $table->char('reg_number', 25);
            $table->boolean('subscribed_to_credit_save')->nullable()->default(false);
            $table->string('country_code', 10)->default('GB');

            $table->index(['client_id', 'reg_number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_reg_number');
    }
}
