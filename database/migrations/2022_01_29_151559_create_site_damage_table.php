<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteDamageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_damage', function (Blueprint $table) {
            $table->integer('id', true);
            $table->unsignedInteger('site_id')->nullable()->index('site_damage_tblsites_ID_fk');
            $table->string('damage_name')->nullable();
            $table->text('damage_desc')->nullable();
            $table->unsignedInteger('created_by')->nullable()->index('site_damage_tblusers_ID_fk');
            $table->timestamp('created_at')->useCurrent();
            $table->unsignedInteger('updated_by')->nullable()->index('site_damage_tblusers_ID_fk_2');
            $table->timestamp('updated_at')->useCurrent();
            $table->boolean('completed')->default(false);
            $table->unsignedInteger('deleted_by')->nullable()->index('site_damage_tblusers_ID_fk_3');
            $table->softDeletes();
            $table->boolean('deleted')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_damage');
    }
}
