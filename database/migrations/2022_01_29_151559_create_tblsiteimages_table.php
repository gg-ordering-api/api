<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsiteimagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsiteimages', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('VisitID')->nullable()->default(0);
            $table->mediumText('ImageName')->nullable();
            $table->mediumText('ImagePath')->nullable();
            $table->timestamp('DateAdded')->nullable()->useCurrent();
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'yes', 'failed'])->nullable()->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsiteimages');
    }
}
