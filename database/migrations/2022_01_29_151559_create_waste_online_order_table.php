<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWasteOnlineOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waste_online_order', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('waste_online_order_id')->nullable()->index('waste_online_order_waste_online_order_id_IDX');
            $table->unsignedInteger('supplier_id')->nullable()->index('waste_online_order_tblsuppliers_ID_fk');
            $table->unsignedInteger('waste_type_id')->nullable()->index('waste_online_order_tblwastetypes_ID_fk');
            $table->unsignedInteger('container_id')->nullable()->index('waste_online_order_tblcontainers_ID_fk');
            $table->unsignedInteger('transfer_station_id')->nullable()->index('waste_online_order_tbltransferstations_ID_fk');
            $table->integer('waste_online_address_id')->nullable();
            $table->unsignedInteger('client_id')->nullable()->index('waste_online_order_tblclients_ID_fk');
            $table->unsignedInteger('site_id')->nullable()->index('waste_online_order_tblsites_ID_fk');
            $table->unsignedInteger('dispute_type_id')->nullable();
            $table->dateTime('delivery_date')->nullable()->default('0000-00-00 00:00:00');
            $table->decimal('supplier_charge', 10)->nullable();
            $table->decimal('customer_charge', 10)->nullable()->default(0);
            $table->string('supplier_ticket_number', 100)->nullable();
            $table->string('supplier_invoice_number', 100)->nullable();
            $table->string('customer_name', 100)->nullable();
            $table->string('business_name', 100)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable()->useCurrent();
            $table->tinyInteger('is_paid')->nullable();
            $table->tinyInteger('is_invoiced')->nullable();
            $table->dateTime('invoice_date')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('is_disputed')->nullable()->index();
            $table->integer('disputed_by')->nullable();
            $table->integer('dispute_cleared_by')->nullable();
            $table->longText('dispute_reason')->nullable();
            $table->tinyInteger('supplier_paid')->nullable();
            $table->dateTime('supplier_paid_date')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('supplier_paid_by')->nullable();
            $table->tinyInteger('is_deleted')->nullable()->default(0);
            $table->string('movement_type', 100)->nullable();
            $table->string('dispute_contact', 100)->nullable();
            $table->string('dispute_name', 100)->nullable();
            $table->dateTime('disputed_date')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('supplier_invoice_checked_date')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('supplier_invoice_checked_by')->nullable();
            $table->string('po_number', 50)->nullable();
            $table->string('dispute_extra', 240)->nullable();
            $table->tinyInteger('supplier_tonnage_type')->nullable();
            $table->decimal('supplier_inc_min_tonnes', 10)->nullable()->default(0);
            $table->decimal('supplier_weight_rate', 10)->nullable()->default(0);
            $table->decimal('ticket_weight', 10, 4)->nullable()->default(0);
            $table->string('supplier_transfer_ticket_number', 50)->nullable();
            $table->text('dispute_clear_reason')->nullable();
            $table->dateTime('dispute_clear_date')->nullable();
            $table->string('supplier_weighbridge_ticket_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste_online_order');
    }
}
