<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientInvoicingLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_invoicing_log', function (Blueprint $table) {
            $table->foreign(['cron_run_id'], 'client_invoicing_log_cron_run_id_fk')->references(['id'])->on('client_invoicing_cron_run_log');
            $table->foreign(['invoice_pdf_id'], 'client_invoicing_log_invoice_pdf_fk')->references(['ID'])->on('tblinvoicepdfs');
            $table->foreign(['status_id'], 'client_invoicing_log_status_fk')->references(['id'])->on('client_invoicing_status');
            $table->foreign(['type_id'], 'client_invoicing_log_type_fk')->references(['type_id'])->on('ocr_order_type');
            $table->foreign(['client_id'], 'client_invoicing_log_client_fk')->references(['ID'])->on('tblclients');
            $table->foreign(['error_id'], 'client_invoicing_log_error_fk')->references(['id'])->on('client_invoicing_error_types');
            $table->foreign(['site_id'], 'client_invoicing_log_sites_fk')->references(['ID'])->on('tblsites');
            $table->foreign(['gg_order_number_id'], 'client_invoicing_log_ticket_id_fk')->references(['id'])->on('gogreen_order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_invoicing_log', function (Blueprint $table) {
            $table->dropForeign('client_invoicing_log_cron_run_id_fk');
            $table->dropForeign('client_invoicing_log_invoice_pdf_fk');
            $table->dropForeign('client_invoicing_log_status_fk');
            $table->dropForeign('client_invoicing_log_type_fk');
            $table->dropForeign('client_invoicing_log_client_fk');
            $table->dropForeign('client_invoicing_log_error_fk');
            $table->dropForeign('client_invoicing_log_sites_fk');
            $table->dropForeign('client_invoicing_log_ticket_id_fk');
        });
    }
}
