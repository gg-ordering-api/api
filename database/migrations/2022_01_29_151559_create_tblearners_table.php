<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblearnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblearners', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('ClientID')->default(0)->index('ClientID');
            $table->integer('UserID')->default(0)->index('UserID');
            $table->decimal('Commission', 4)->default(1);
            $table->dateTime('EndDate')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('EarnerType')->default(0);
            $table->integer('AddedBy')->default(0);
            $table->timestamp('AddedOn')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblearners');
    }
}
