<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblquotealertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblquotealerts', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('QuoteID')->default(0);
            $table->integer('UserID')->default(0);
            $table->mediumText('AlertBody')->nullable();
            $table->dateTime('NextAlertDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('LastActionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('NumberOfTimesSnoozed')->default(0);
            $table->dateTime('ManagerLastAlertedDate')->nullable()->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblquotealerts');
    }
}
