<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientDebtStatusLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_debt_status_log', function (Blueprint $table) {
            $table->foreign(['client_debt_status_id'], 'client_debt_status_log_client_debt_status_id_fk')->references(['id'])->on('client_debt_status');
            $table->foreign(['note_id'], 'client_on_stop_status_log_note_id_fk')->references(['id'])->on('note');
            $table->foreign(['client_debt_log_id'], 'client_debt_status_log_client_debt_log_id_fk')->references(['id'])->on('client_debt_log');
            $table->foreign(['letter_sent_by'], 'client_debt_status_log_tblusers_ID_fk')->references(['ID'])->on('tblusers');
            $table->foreign(['created_by'], 'client_on_stop_status_log_tblusers_ID_fk')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_debt_status_log', function (Blueprint $table) {
            $table->dropForeign('client_debt_status_log_client_debt_status_id_fk');
            $table->dropForeign('client_on_stop_status_log_note_id_fk');
            $table->dropForeign('client_debt_status_log_client_debt_log_id_fk');
            $table->dropForeign('client_debt_status_log_tblusers_ID_fk');
            $table->dropForeign('client_on_stop_status_log_tblusers_ID_fk');
        });
    }
}
