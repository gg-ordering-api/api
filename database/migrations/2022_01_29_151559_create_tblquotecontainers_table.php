<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblquotecontainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblquotecontainers', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('QuoteID')->default('0')->index('QuoteID');
            $table->integer('ContainerSize')->default(0)->index('ContainerSize');
            $table->integer('WasteType')->default(0)->index('WasteType');
            $table->dateTime('DateRequired')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('IsWaitLoad')->default(false);
            $table->decimal('WLHours', 5)->default(0);
            $table->boolean('OutOfHoursReq')->default(false);
            $table->boolean('BaySusReq')->default(false);
            $table->boolean('PermitReq')->default(false);
            $table->smallInteger('ContainsPlasterboard')->default(0);
            $table->mediumInteger('PlasterboardPercent')->default(0);
            $table->unsignedBigInteger('PrimaryChoice')->default('0')->index('PrimaryChoice');
            $table->unsignedBigInteger('SecondaryChoice')->default('0');
            $table->boolean('CheckBeforeRepeat')->default(false);
            $table->integer('EstimatedGroupSkips')->default(0);
            $table->mediumText('VariousWasteTypes')->nullable();
            $table->integer('TimeRequired')->default(0);
            $table->smallInteger('liftfrequency')->default(0);
            $table->smallInteger('liftquantity')->default(0);
            $table->decimal('RecycleRate', 10)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblquotecontainers');
    }
}
