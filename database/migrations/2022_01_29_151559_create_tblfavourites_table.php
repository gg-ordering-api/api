<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblfavouritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblfavourites', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('UserID')->default(0)->index('UserID');
            $table->mediumText('QueryString')->nullable();
            $table->mediumText('PageName')->nullable();
            $table->mediumText('DetailedQueryString')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblfavourites');
    }
}
