<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblportalsiteassignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblportalsiteassignments', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('UserID')->default(0);
            $table->bigInteger('SiteID')->default(0);

            $table->index(['UserID', 'SiteID'], 'UserID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblportalsiteassignments');
    }
}
