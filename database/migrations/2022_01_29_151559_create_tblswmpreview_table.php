<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblswmpreviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblswmpreview', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('SiteID')->default(0)->index('SiteID');
            $table->string('ReviewerName', 200)->nullable();
            $table->timestamp('ReviewDate')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblswmpreview');
    }
}
