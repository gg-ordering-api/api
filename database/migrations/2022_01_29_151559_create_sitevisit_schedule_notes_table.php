<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitevisitScheduleNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitevisit_schedule_notes', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('site_id')->default(0)->unique('site_id')->comment('Foreign key of tblsites');
            $table->text('note')->nullable();
            $table->tinyInteger('is_schedule_enabled')->default(1)->index('is_schedule_enabled')->comment('0=Disabled,1=Enabled');
            $table->integer('last_modified_by')->default(0)->index('last_modified_by');
            $table->dateTime('date_modified')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('date_created')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitevisit_schedule_notes');
    }
}
