<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOcrTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ocr_ticket', function (Blueprint $table) {
            $table->foreign(['ocr_migration_log_id'], 'ocr_ticket_ocr_migration_log_id_fk')->references(['id'])->on('ocr_migration_log');
            $table->foreign(['original_status_id'], 'ocr_ticket_ocr_ticket_status_id_fk_2')->references(['id'])->on('ocr_ticket_status');
            $table->foreign(['gogreen_order_number_id'], 'ocr_ticket_gogreen_order_numbera_id_fk')->references(['id'])->on('gogreen_order_number');
            $table->foreign(['status_id'], 'ocr_ticket_ocr_ticket_status_id_fk')->references(['id'])->on('ocr_ticket_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocr_ticket', function (Blueprint $table) {
            $table->dropForeign('ocr_ticket_ocr_migration_log_id_fk');
            $table->dropForeign('ocr_ticket_ocr_ticket_status_id_fk_2');
            $table->dropForeign('ocr_ticket_gogreen_order_numbera_id_fk');
            $table->dropForeign('ocr_ticket_ocr_ticket_status_id_fk');
        });
    }
}
