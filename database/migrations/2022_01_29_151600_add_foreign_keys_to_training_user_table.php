<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTrainingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_user', function (Blueprint $table) {
            $table->foreign(['training_status_id'])->references(['id'])->on('training_status');
            $table->foreign(['training_item_id'])->references(['id'])->on('training_item');
            $table->foreign(['user_id'])->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_user', function (Blueprint $table) {
            $table->dropForeign('training_user_training_status_id_foreign');
            $table->dropForeign('training_user_training_item_id_foreign');
            $table->dropForeign('training_user_user_id_foreign');
        });
    }
}
