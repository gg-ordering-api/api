<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblpricelistmasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblpricelistmaster', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SupplierID')->default('0')->index('SupplierID');
            $table->string('PricelistName', 200)->nullable();
            $table->timestamp('CreationDate')->useCurrent();
            $table->integer('CreationUser')->default(0);
            $table->dateTime('UpdateDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('UpdateUser')->default(0);
            $table->dateTime('ValidFrom')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('ValidUntil')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('ServiceRegions')->nullable();
            $table->integer('WasteDestination')->default(0);
            $table->integer('PreferredPriceList')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpricelistmaster');
    }
}
