<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsitevisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsitevisits', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('ClientID')->default(0);
            $table->integer('SiteID')->default(0);
            $table->integer('SupplierID')->default(0);
            $table->integer('TransferID')->default(0);
            $table->integer('UserID')->default(0)->index('UserID');
            $table->dateTime('TimeDateBooking')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('ManagerNote')->nullable();
            $table->mediumText('MissedReason')->nullable();
            $table->boolean('VisitCompleted')->default(false);
            $table->boolean('VisitMissed')->default(false);
            $table->boolean('UserCompleted')->default(false);
            $table->boolean('ManagerApproved')->default(false);
            $table->tinyInteger('Status')->default(0);
            $table->mediumText('StartDate');
            $table->mediumText('EndDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsitevisits');
    }
}
