<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcrTicketTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocr_ticket_type', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('cog_type_id')->nullable()->index('ocr_transaction_type_cog_type_fk');
            $table->string('type', 100)->nullable()->unique('ocr_ticket_type_type_uindex');
            $table->integer('document_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocr_ticket_type');
    }
}
