<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblswmpsnapshotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblswmpsnapshots', function (Blueprint $table) {
            $table->increments('ID');
            $table->timestamp('DateAdded')->useCurrent();
            $table->mediumText('DocName');
            $table->mediumText('Extension');
            $table->mediumText('FileType');
            $table->integer('SiteID')->nullable()->default(0);
            $table->string('AddedBy', 100)->nullable();
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'yes', 'failed'])->nullable()->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblswmpsnapshots');
    }
}
