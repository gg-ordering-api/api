<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketSupplierInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_supplier_invoice', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('go_green_order_id')->index('ticket_supplier_invoice_gogreen_order_number_id_fk');
            $table->unsignedInteger('supplier_invoice_id')->index('ticket_supplier_invoice_tblsupplierinvoices_ID_fk');
            $table->decimal('value', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_supplier_invoice');
    }
}
