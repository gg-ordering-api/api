<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcrErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocr_errors', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('transaction_id')->nullable();
            $table->integer('error_code')->nullable();
            $table->longText('data')->nullable();
            $table->tinyInteger('completed')->nullable();
            $table->dateTime('completed_date')->nullable();
            $table->unsignedInteger('completed_by')->nullable()->index('ocr_errors_tblusers_ID_fk');
            $table->timestamp('created_date')->nullable()->useCurrent();
            $table->integer('ocr_migration_log_id')->nullable();
            $table->integer('ignored')->nullable()->default(0);
            $table->unsignedInteger('ignored_by')->nullable()->index('ocr_errors_tblusers_ID_fk_2');
            $table->dateTime('ignored_date')->nullable();
            $table->text('ignored_reason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocr_errors');
    }
}
