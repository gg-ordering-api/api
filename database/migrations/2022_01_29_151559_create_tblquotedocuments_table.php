<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblquotedocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblquotedocuments', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('DocName', 500)->nullable();
            $table->string('FileName', 500)->nullable();
            $table->string('Extension', 5)->nullable();
            $table->string('FileType', 10000)->nullable();
            $table->timestamp('DateAdded')->useCurrent();
            $table->unsignedBigInteger('AddedBy')->default('0');
            $table->unsignedBigInteger('QuoteID')->default('0')->index('QuoteID');
            $table->integer('category_id')->nullable()->default(0);
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'failed', 'yes'])->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblquotedocuments');
    }
}
