<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblbinticketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblbintickets', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('ClientID')->default(0)->index('ClientID');
            $table->bigInteger('SiteID')->index('SiteID');
            $table->bigInteger('GroupID')->default(0)->index('GroupID');
            $table->decimal('TicketWeight', 10)->default(0);
            $table->string('TicketDay', 10)->nullable();
            $table->timestamp('DateCreated')->useCurrent();
            $table->dateTime('LiftDate')->nullable()->default('0000-00-00 00:00:00');
            $table->decimal('SupplierCharge', 10)->default(0);
            $table->decimal('Margin', 10)->default(0);
            $table->decimal('ClientCharge', 10)->default(0);
            $table->decimal('SupplierInclusive', 10)->default(0);
            $table->decimal('ClientInclusive', 10)->default(0);
            $table->decimal('SupplierPerTonne', 10)->default(0);
            $table->decimal('PerTonneMargin', 10)->default(0);
            $table->decimal('ClientPerTonne', 10)->default(0);
            $table->decimal('RecycleRate', 10)->default(0);
            $table->bigInteger('Supplier')->default(0)->index('Supplier');
            $table->bigInteger('WasteDestination')->default(0);
            $table->boolean('DivertedFromLandfill')->default(false);
            $table->integer('CreatedBy')->default(0)->index('CreatedBy');
            $table->boolean('IsDeleted')->default(false);
            $table->integer('LiftQuantity')->default(0);
            $table->boolean('IsInvoiced')->default(false);
            $table->dateTime('InvoicedOn')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('InvoiceDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('InvoicedBy')->default(0);
            $table->unsignedBigInteger('InvoiceNumber')->default('0')->index('InvoiceNumber');
            $table->boolean('InvoiceSent')->default(false);
            $table->boolean('InvoicePaid')->default(false);
            $table->integer('DeletedBy')->default(0);
            $table->dateTime('DeletionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('WeightAddedBy')->default(0);
            $table->dateTime('DateWeightAdded')->nullable()->default('0000-00-00 00:00:00');
            $table->string('SupplierWeightContact', 100)->nullable();
            $table->boolean('LateLift')->default(false);
            $table->boolean('MissedLift')->default(false);
            $table->integer('BillingCode')->default(0);
            $table->mediumText('SupplierNotes')->nullable();
            $table->bigInteger('BelongsToWR')->default(0)->index('BelongsToWR');
            $table->string('SupplierInvNo', 100)->nullable()->index('SupplierInvNo');
            $table->smallInteger('SuppDisputeType')->default(0)->index('SuppDisputeType');
            $table->mediumText('SupplierDisputeReason')->nullable();
            $table->mediumText('SupplierDisputeClearReason')->nullable();
            $table->string('SuppTicketNo', 100)->nullable();
            $table->boolean('SuppIsDisputed')->default(false);
            $table->dateTime('SuppDateDisputed')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SuppDisputedBy')->default(0)->index('SuppDisputedBy');
            $table->dateTime('SuppDateDisputeCleared')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SuppDisputeClearedBy')->default(0);
            $table->boolean('SuppIsPaid')->default(false);
            $table->dateTime('SuppPaidOn')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SuppPaidBy')->default(0);
            $table->integer('SuppInvCheckedBy')->default(0);
            $table->dateTime('SuppCheckDate')->nullable()->default('0000-00-00 00:00:00');
            $table->smallInteger('ClientDisputeType')->default(0);
            $table->mediumText('ClientDisputeReason')->nullable();
            $table->integer('ClientDisputedBy')->default(0);
            $table->boolean('ClientIsDisputed')->default(false)->index('bt_clientdisputed');
            $table->dateTime('ClientDateDisputed')->nullable()->default('0000-00-00 00:00:00')->index('tblbintickets_ClientDateDisputed_index');
            $table->dateTime('ClientDateDisputeCleared')->nullable()->default('0000-00-00 00:00:00')->index('tblbintickets_ClientDateDisputeCleared_index');
            $table->integer('ClientDisputeClearedBy')->default(0);
            $table->mediumText('ClientDisputeClearReason')->nullable();
            $table->integer('LeadOwnerBDM')->default(0);
            $table->mediumText('DisputerName')->nullable();
            $table->mediumText('DisputerContact')->nullable();
            $table->mediumText('ClientDisputerName')->nullable();
            $table->mediumText('ClientDisputerContact')->nullable();
            $table->decimal('SuppDisputeAdjust', 7)->default(0);
            $table->boolean('DoNotInvoice')->default(false);
            $table->integer('InvBlockedBy')->default(0);
            $table->string('dispute_extra', 240)->nullable();
            $table->string('supplier_transfer_ticket_number')->nullable();

            $table->index(['SuppDisputeClearedBy', 'SuppDateDisputeCleared'], 'tblbintickets_SuppDisputeClearedBy_SuppDateDisputeCleared_index');
            $table->index(['SuppDateDisputed', 'SuppDisputedBy'], 'tblbintickets_SuppDateDisputed_SuppDisputedBy_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblbintickets');
    }
}
