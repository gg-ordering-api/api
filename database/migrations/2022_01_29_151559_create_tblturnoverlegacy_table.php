<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblturnoverlegacyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblturnoverlegacy', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('ClientID')->default(0);
            $table->decimal('SupplierSpend', 20)->default(0);
            $table->decimal('ClientTurnover', 20)->default(0);
            $table->decimal('MarginPercent', 5)->default(0);
            $table->integer('AssignedUser')->default(0);
            $table->integer('TurnoverMonth')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblturnoverlegacy');
    }
}
