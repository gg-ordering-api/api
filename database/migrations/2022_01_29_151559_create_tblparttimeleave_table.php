<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblparttimeleaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblparttimeleave', function (Blueprint $table) {
            $table->integer('ID', true);
            $table->unsignedSmallInteger('UserID')->unique('UserID');
            $table->float('LeaveDays', 10, 0)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblparttimeleave');
    }
}
