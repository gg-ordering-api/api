<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblopsjobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblopsjobs', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('QuoteID')->default('0');
            $table->timestamp('DateSubmitted')->useCurrent()->index('tblopsjobs_DateSubmitted_index');
            $table->mediumText('OpsNotes')->nullable();
            $table->boolean('Status')->default(false);
            $table->integer('PickedupBy')->default(0)->index('PickedupBy');
            $table->dateTime('CompletionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->smallInteger('BookingPriority')->default(0);
            $table->integer('BookingQuantity')->default(1);
            $table->mediumText('ordered_by')->nullable();
            $table->mediumText('container')->nullable();
            $table->timestamp('delivery_date')->nullable();
            $table->string('purchase_order')->nullable();
            $table->mediumText('site_contact')->nullable();
            $table->mediumText('margin_notes')->nullable();
            $table->text('payment_type')->nullable()->default('OnAccount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblopsjobs');
    }
}
