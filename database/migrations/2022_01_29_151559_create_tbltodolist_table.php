<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbltodolistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbltodolist', function (Blueprint $table) {
            $table->increments('ID');
            $table->mediumText('title')->nullable();
            $table->dateTime('DateRequired')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('AssignedTo')->default(0)->index('AssignedTo');
            $table->boolean('status')->default(false);
            $table->timestamp('CreationDate')->useCurrent();
            $table->integer('creator_id')->default(0);
            $table->dateTime('DateCompleted')->nullable()->default('0000-00-00 00:00:00');
            $table->char('description')->nullable();
            $table->char('object_type')->nullable();
            $table->integer('object_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbltodolist');
    }
}
