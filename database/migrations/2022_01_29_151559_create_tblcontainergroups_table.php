<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblcontainergroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblcontainergroups', function (Blueprint $table) {
            $table->increments('ID')->unique('tblcontainergroups_ID_uindex');
            $table->integer('ContainerType')->default(0)->index('ContainerType');
            $table->integer('WasteType')->default(0)->index('WasteType');
            $table->boolean('IsHazardous')->default(false);
            $table->string('PONumber', 50)->nullable();
            $table->bigInteger('SiteID')->default(0)->index('SiteID');
            $table->integer('CreatedBy')->default(0);
            $table->timestamp('CreatedOn')->useCurrent();
            $table->string('SICProducer', 500)->nullable();
            $table->boolean('HasRentals')->default(false);
            $table->string('DistanceText', 20)->nullable();
            $table->integer('DistanceMeters')->default(0);
            $table->decimal('IncreaseTrans', 10)->default(0);
            $table->decimal('IncreaseTonnage', 10)->default(0);
            $table->decimal('IncreaseWLHour', 10)->default(0);
            $table->boolean('IncreasesCompleted')->default(false);
            $table->dateTime('DateOfIncreases')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('VariousWasteTypes')->nullable();
            $table->boolean('PremRequired')->default(false);
            $table->mediumText('PremDescription')->nullable();
            $table->smallInteger('IncreaseIncMin')->default(0);
            $table->decimal('IncreaseIncMinTonnes', 10)->default(0);
            $table->bigInteger('GroupQuoteID')->nullable();
            $table->bigInteger('ContainerQuoteID')->nullable();
            $table->boolean('PermitRequired')->default(false)->index('PermitRequired');
            $table->dateTime('increase_from_date')->nullable();
            $table->boolean('site_held')->nullable()->default(false);
            $table->boolean('container_ticked')->nullable()->default(false);
            $table->boolean('client_increase_completed')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblcontainergroups');
    }
}
