<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblrequestedmovementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblrequestedmovement', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SiteID')->default('0')->index('tblrequestedmovement_SiteID_index');
            $table->unsignedBigInteger('ClientID')->default('0')->index('tblrequestedmovement_ClientID_index');
            $table->string('MovementType', 50)->nullable();
            $table->integer('SentFrom');
            $table->timestamp('DateSubmited')->useCurrent()->index('tblrequestedmovement_DateSubmited_index');
            $table->mediumText('OpsNotes')->nullable();
            $table->boolean('Status')->default(false);
            $table->integer('PickupBy')->default(0);
            $table->timestamp('PickupDate')->nullable()->default('0000-00-00 00:00:00');
            $table->timestamp('CompletionDate')->nullable()->default('0000-00-00 00:00:00')->index('tblrequestedmovement_CompletionDate_index');
            $table->dateTime('RequestedDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('Complete')->default(false);
            $table->mediumText('Notes')->nullable();
            $table->timestamp('EditDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('EditBy')->nullable();
            $table->boolean('OutOfHours')->nullable()->default(false);

            $table->index(['PickupBy', 'CompletionDate'], 'tblrequestedmovement_PickupBy_CompletionDate_index');
            $table->index(['SentFrom', 'DateSubmited'], 'tblrequestedmovement_SentFrom_DateSubmited_index');
            $table->index(['PickupBy', 'PickupDate'], 'tblrequestedmovement_PickupBy_PickupDate_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblrequestedmovement');
    }
}
