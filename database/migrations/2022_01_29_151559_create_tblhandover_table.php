<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblhandoverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblhandover', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('clientID');
            $table->smallInteger('account_type')->comment('1=New ,2=Lost. 3=BDM, 4=Internal, 5=Reactivated');
            $table->decimal('potential_annual_spent', 12);
            $table->smallInteger('no_of_buyers')->default(0);
            $table->smallInteger('no_of_environmental')->default(0);
            $table->tinyInteger('removed_from_salesportal')->default(0);
            $table->tinyInteger('added_to_accountsporal')->default(0);
            $table->integer('created_by');
            $table->integer('last_modified_by');
            $table->mediumText('notes_from_acct_manager');
            $table->integer('wonLast3MonthsSites')->nullable();
            $table->integer('lostLast3MonthsSites')->nullable();
            $table->integer('pendingLast3MonthsSites')->nullable();
            $table->integer('wonLast13MonthsSites')->nullable();
            $table->integer('lostLast13MonthsSites')->nullable();
            $table->integer('pendingLast13MonthsSites')->nullable();
            $table->mediumText('notes_from_teamleader');
            $table->tinyInteger('status')->default(0)->comment('0=Pending,1=Accept,2=Declined');
            $table->mediumText('decline_reason')->nullable();
            $table->mediumText('account_notes')->nullable();
            $table->integer('department_id')->nullable()->default(0);
            $table->boolean('isApprovedByManager')->default(false);
            $table->dateTime('date_created')->default('0000-00-00 00:00:00');
            $table->dateTime('date_modified')->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblhandover');
    }
}
