<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTblsupplierinvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblsupplierinvoices', function (Blueprint $table) {
            $table->foreign(['supplier_invoice_type_id'], 'tblsupplierinvoices_supplier_invoice_type_id_fk')->references(['id'])->on('supplier_invoice_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblsupplierinvoices', function (Blueprint $table) {
            $table->dropForeign('tblsupplierinvoices_supplier_invoice_type_id_fk');
        });
    }
}
