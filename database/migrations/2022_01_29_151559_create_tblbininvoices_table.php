<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblbininvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblbininvoices', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('ClientID')->default(0)->index('ClientID');
            $table->bigInteger('SiteID')->default(0);
            $table->timestamp('CreationDate')->useCurrent();
            $table->decimal('InvoiceValue', 20)->default(0);
            $table->boolean('IsPaid')->default(false);
            $table->integer('MarkedPaidBy')->default(0)->index('MarkedPaidBy');
            $table->boolean('SentToClient')->default(false);
            $table->boolean('ExportedToSage')->default(false);
            $table->dateTime('InvoiceDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('PaidDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('DateSentClient')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SendingUser')->default(0);
            $table->integer('OutstandingBalance')->default(0);
            $table->boolean('IsCreditInvoice')->default(false);
            $table->boolean('ExportedToTradex')->default(false);
            $table->boolean('disputeStatusUpdate')->default(false);
            $table->boolean('queue')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblbininvoices');
    }
}
