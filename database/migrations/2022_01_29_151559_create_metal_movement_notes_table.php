<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetalMovementNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metal_movement_notes', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('note', 500)->nullable();
            $table->integer('tblmovements_ID')->default(0)->index('tblmovements_ID')->comment('Primary key of tblmovements');
            $table->integer('last_modified_by')->default(0);
            $table->dateTime('date_updated')->nullable()->default('0000-00-00 00:00:00');
            $table->timestamp('date_created')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metal_movement_notes');
    }
}
