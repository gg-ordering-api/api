<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblbinscheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblbinschedule', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('SiteID')->default(0)->index('SiteID');
            $table->bigInteger('ClientID')->default(0)->index('ClientID');
            $table->integer('WasteType')->default(0);
            $table->integer('ContainerType')->default(0);
            $table->decimal('ContainerWeight', 10, 4)->default(0);
            $table->decimal('SupplierLiftPrice', 10)->default(0);
            $table->decimal('LiftMargin', 10)->default(0);
            $table->decimal('ClientLiftPrice', 10)->default(0);
            $table->decimal('SupplierPerTonne', 10)->default(0);
            $table->decimal('PerTonneMargin', 10)->default(0);
            $table->decimal('ClientPerTonne', 10)->default(0);
            $table->decimal('SupplierIncluding', 10)->default(0);
            $table->decimal('ClientIncluding', 10)->default(0);
            $table->decimal('RecycleRate', 10)->default(0);
            $table->integer('UsedSupplier')->default(0)->index('UsedSupplier');
            $table->integer('WasteDestination')->default(0);
            $table->boolean('DivertedLandfill')->default(true);
            $table->smallInteger('MondayNumber')->default(0);
            $table->smallInteger('MondayFreq')->default(0);
            $table->smallInteger('TuesdayNumber')->default(0);
            $table->smallInteger('TuesdayFreq')->default(0);
            $table->smallInteger('WednesdayNumber')->default(0);
            $table->smallInteger('WednesdayFreq')->default(0);
            $table->smallInteger('ThursdayNumber')->default(0);
            $table->smallInteger('ThursdayFreq')->default(0);
            $table->smallInteger('FridayNumber')->default(0);
            $table->smallInteger('FridayFreq')->default(0);
            $table->smallInteger('SaturdayNumber')->default(0);
            $table->smallInteger('SaturdayFreq')->default(0);
            $table->smallInteger('SundayNumber')->default(0);
            $table->smallInteger('SundayFreq')->default(0);
            $table->dateTime('LastCommitted')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('LastEditBy')->default(0);
            $table->dateTime('LastEditDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('BillingCode')->default(0);
            $table->integer('isQuote')->default(0)->comment('If its greater than zero its quote id');
            $table->integer('QuoteContainerID')->default(0)->comment('tblquotecontainers ID');
            $table->dateTime('DateOfIncreases')->nullable();
            $table->dateTime('increase_from_date')->nullable();
            $table->decimal('SupplierLiftPriceNew', 10)->nullable()->default(0);
            $table->decimal('SupplierPerTonneNew', 10)->nullable()->default(0);
            $table->decimal('ClientLiftPriceNew', 10)->nullable()->default(0);
            $table->decimal('ClientPerTonneNew', 10)->nullable()->default(0);
            $table->dateTime('supplier_increase_from_date')->nullable();
            $table->dateTime('supplierIncreaseDate')->nullable();
            $table->boolean('site_held')->nullable()->default(false);
            $table->boolean('container_ticked')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblbinschedule');
    }
}
