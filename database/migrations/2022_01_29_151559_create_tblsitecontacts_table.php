<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsitecontactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsitecontacts', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('FirstName', 100)->nullable();
            $table->string('Surname', 100)->nullable();
            $table->string('EmailAddress', 100)->nullable();
            $table->string('Telephone', 20)->nullable();
            $table->string('Fax', 20)->nullable();
            $table->string('Mobile', 20)->nullable();
            $table->string('JobTitle', 50)->nullable();
            $table->bigInteger('SiteID')->default(0)->index('Company');
            $table->timestamp('DateCreated')->useCurrent();
            $table->dateTime('LastUpdated')->nullable()->default('0000-00-00 00:00:00');
            $table->bigInteger('ReportsTo')->default(0);
            $table->boolean('IsDeleted')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsitecontacts');
    }
}
