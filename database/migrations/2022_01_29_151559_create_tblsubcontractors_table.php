<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsubcontractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsubcontractors', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SiteID')->default('0')->index('SiteID');
            $table->unsignedBigInteger('SubID')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsubcontractors');
    }
}
