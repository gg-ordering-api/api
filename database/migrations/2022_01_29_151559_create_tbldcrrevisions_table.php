<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbldcrrevisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldcrrevisions', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('DocID')->default(0);
            $table->string('FileName', 500)->nullable();
            $table->string('Extension', 5)->nullable();
            $table->string('FileType', 1000)->nullable();
            $table->integer('version')->default(0);
            $table->timestamp('DateAdded')->useCurrent();
            $table->bigInteger('AddedBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldcrrevisions');
    }
}
