<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierCogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_cog', function (Blueprint $table) {
            $table->integer('id', true);
            $table->unsignedInteger('supplier_id')->nullable()->index('supplier_cog_tblsuppliers_ID_fk');
            $table->string('template_abbreviation')->nullable();
            $table->string('template_name')->nullable();
            $table->string('folder')->nullable();
            $table->boolean('on_cron')->nullable()->default(false);
            $table->integer('cog_rule_id')->default(1);
            $table->string('default_weights')->nullable();
            $table->dateTime('exported_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_cog');
    }
}
