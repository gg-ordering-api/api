<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbldisputenotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldisputenotes', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('NoteTitle', 100)->nullable();
            $table->mediumText('NoteBody')->nullable();
            $table->timestamp('CreatedOn')->useCurrent();
            $table->integer('CreatedBy')->nullable();
            $table->string('SupplierInvoiceNo', 50)->nullable()->index('SupplierInvoiceNo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldisputenotes');
    }
}
