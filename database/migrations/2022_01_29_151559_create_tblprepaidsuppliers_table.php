<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblprepaidsuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblprepaidsuppliers', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('Supplier')->default(0);
            $table->string('InvoiceNumber', 200)->nullable();
            $table->dateTime('InvoiceExpiry')->nullable()->default('0000-00-00 00:00:00');
            $table->timestamp('SetDate')->useCurrent();
            $table->integer('SetUser')->default(0);
            $table->bigInteger('ClientID')->default(0);
            $table->bigInteger('SiteID')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblprepaidsuppliers');
    }
}
