<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblportalusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblportalusers', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('ClientID')->default('0');
            $table->unsignedSmallInteger('PermissionID')->nullable()->default('0');
            $table->boolean('super_user')->nullable()->default(false);
            $table->string('Username', 200)->nullable();
            $table->string('FullName', 200)->nullable();
            $table->string('EmailAddress', 200)->nullable();
            $table->string('UserPassword', 200)->nullable();
            $table->dateTime('LastLoginTime')->nullable()->default('0000-00-00 00:00:00');
            $table->string('LastLoginIP', 20)->nullable();
            $table->smallInteger('LoginFailures')->default(0);
            $table->string('SecretKey', 64)->nullable();
            $table->string('ResetKey', 64)->nullable();
            $table->dateTime('ResetRequestTime')->nullable()->default('0000-00-00 00:00:00');
            $table->string('Note', 500)->nullable();
            $table->dateTime('added_date')->nullable()->default('0000-00-00 00:00:00');
            $table->string('SecretKeyVThree', 100)->nullable();
            $table->dateTime('key_expiry_date')->nullable()->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblportalusers');
    }
}
