<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbllossesandgainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbllossesandgains', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('TicketID')->default(0)->index('TicketID');
            $table->decimal('ActionValue', 10)->default(0);
            $table->mediumText('ActionDescription')->nullable();
            $table->integer('ActionUser')->default(0)->index('ActionUser');
            $table->dateTime('ActionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('CreationUser')->default(0);
            $table->timestamp('CreationDate')->useCurrent();
            $table->tinyInteger('ActionType')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbllossesandgains');
    }
}
