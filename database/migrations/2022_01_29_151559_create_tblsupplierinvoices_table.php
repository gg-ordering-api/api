<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsupplierinvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsupplierinvoices', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('SupplierID')->default(0)->index('SupplierID');
            $table->decimal('NetValue', 20)->default(0);
            $table->decimal('GrossValue', 20)->default(0);
            $table->string('InvoiceNumber', 50)->index('InvoiceNumber');
            $table->dateTime('InvoiceDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('BinsSkips')->default(false);
            $table->integer('CheckingUser')->default(0);
            $table->timestamp('CheckingDate')->useCurrent();
            $table->decimal('VATValue', 10)->default(0);
            $table->decimal('CheckedValue', 10)->default(0);
            $table->decimal('BalanceValue', 10);
            $table->boolean('ExportedToSage')->default(false);
            $table->boolean('OkToExport')->default(false);
            $table->mediumText('ExportData')->nullable();
            $table->dateTime('exported_to_sage_at')->nullable();
            $table->boolean('IsCreditInvoice')->default(false);
            $table->boolean('disputeStatusUpdate')->default(false);
            $table->boolean('new_checking_screen')->nullable()->default(false);
            $table->integer('is_metal')->nullable()->default(0);
            $table->integer('approved_to_export')->nullable()->default(0);
            $table->integer('supplier_invoice_type_id')->nullable()->default(1)->index('tblsupplierinvoices_supplier_invoice_type_id_fk');
            $table->dateTime('exported_at')->nullable();
            $table->boolean('original_ocr')->nullable()->default(false);
            $table->timestamp('created_date')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsupplierinvoices');
    }
}
