<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblpremcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblpremcodes', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SiteID')->index('SiteID');
            $table->string('PremisesCode', 20)->nullable();
            $table->dateTime('ExpiryDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('WasteType')->nullable()->default(0);
            $table->bigInteger('HazCert')->default(0);
            $table->boolean('ExpiryWarned')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpremcodes');
    }
}
