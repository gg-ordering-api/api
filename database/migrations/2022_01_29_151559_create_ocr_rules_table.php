<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcrRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocr_rules', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('supplier_id');
            $table->integer('ticket_type_id')->index('ocr_rules_ticket_id_fk');
            $table->integer('cog_rule_id')->index('ocr_rules_cog_rule_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocr_rules');
    }
}
