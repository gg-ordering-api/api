<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsupplierbinweightchargeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsupplierbinweightcharge', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('SupplierID')->default(0);
            $table->integer('ContainerID')->default(0);
            $table->tinyInteger('WasteType')->nullable();
            $table->decimal('SupplierIncTonnes', 8, 4)->nullable();
            $table->decimal('SupplierPerLift', 10)->nullable();
            $table->decimal('ChargePerTonne', 10)->nullable();
            $table->timestamp('LastUpdated')->useCurrent();
            $table->unsignedInteger('LastUpdatedBy')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsupplierbinweightcharge');
    }
}
