<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblassetmanagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblassetmanagement', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('OldID', 20)->nullable();
            $table->mediumText('AssetDescription')->nullable();
            $table->integer('AssetLocation')->default(0)->index('AssetLocation');
            $table->mediumText('SerialNumber')->nullable();
            $table->boolean('IsElectrical')->default(false);
            $table->string('VendorSupplier', 200)->nullable();
            $table->dateTime('PurchaseDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('Notes')->nullable();
            $table->tinyInteger('IsRemoved')->default(0);
            $table->integer('AddedBy')->default(0);
            $table->timestamp('DateAdded')->useCurrent();
            $table->boolean('TestNotRequired')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblassetmanagement');
    }
}
