<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGogreenOrderReturnReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gogreen_order_return_reasons', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('go_green_order_number_id')->nullable()->index('gogreen_order_return_reasons_gogreen_order_number_id_fk');
            $table->longText('reason')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->unsignedInteger('created_by')->nullable()->index('gogreen_order_return_reasons_tblusers_ID_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gogreen_order_return_reasons');
    }
}
