<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcrContainerRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocr_container_rules', function (Blueprint $table) {
            $table->integer('id', true)->unique('ocr_supplier_container_rule_id_uindex');
            $table->unsignedInteger('container_type_id')->nullable()->index('ocr_container_rules_container_type_fk');
            $table->unsignedInteger('supplier_id')->nullable()->index('ocr_container_rules_supplier_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocr_container_rules');
    }
}
