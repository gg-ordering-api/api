<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOcrOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ocr_order', function (Blueprint $table) {
            $table->foreign(['type_id'], 'ocr_order_ocr_order_type_type_id_fk')->references(['type_id'])->on('ocr_order_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocr_order', function (Blueprint $table) {
            $table->dropForeign('ocr_order_ocr_order_type_type_id_fk');
        });
    }
}
