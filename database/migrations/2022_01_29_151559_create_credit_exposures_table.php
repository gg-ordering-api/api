<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditExposuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_exposures', function (Blueprint $table) {
            $table->increments('id');
            $table->char('reg_number', 25)->index();
            $table->decimal('exposure_limit', 10);
            $table->integer('director');
            $table->tinyInteger('deleted');
            $table->timestamp('created_timestamp')->useCurrent();
            $table->integer('created_by');
            $table->timestamp('updated_timestamp')->useCurrentOnUpdate()->useCurrent();
            $table->integer('updated_by');
            $table->boolean('active')->nullable()->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_exposures');
    }
}
