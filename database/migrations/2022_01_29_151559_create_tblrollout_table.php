<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblrolloutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblrollout', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('SiteID')->default(0)->index('SiteID');
            $table->integer('ClientID')->default(0)->index('ClientID')->comment('linked to tblclients');
            $table->integer('ContainerSize')->default(0)->comment('Container size (int linked to tblcontainers)');
            $table->integer('WasteType')->default(0)->comment('Container waste type (int linked to tblwastetypes)');
            $table->integer('CurrentSupplier')->default(0)->comment('Current Supplier (int linked to tblsuppliers)');
            $table->integer('LiftDay')->default(0)->comment('0=Sun,1=Mon,2=Tue,3=Wed,4=Thu,5=Fri,6=Sat');
            $table->integer('LiftQuantity')->default(0);
            $table->integer('LiftFrequency')->default(0)->comment('0=Unknown,1=Weekly,2=Fortnightly,3=3 Weekly,4=Monthly,5=Quarterly');
            $table->dateTime('ContractRequested')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('ContractReceived')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('ContractAnniversaryDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('NoticePeriodStart')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('NoticePeriodEnd')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('DateNoticeOfTerminationSubmitted')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('DateNoticeOfTerminationAccepted')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('SiteTerminationDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('DateOfRolloutCallToSite')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('DateQuoteSubmitted')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('QuoteID')->default(0)->comment('Linked to tblquotes ID');
            $table->dateTime('DateQuoteReturned')->nullable()->default('0000-00-00 00:00:00')->comment('Date Quote returned and supplier contacted with Order');
            $table->integer('BinScheduleID')->default(0)->comment('Linked to tblbinschedule ID');
            $table->dateTime('NewContainerDeliveryDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('PurchaseOrderSentToSupplier')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('SupplierConfirmedServiceRollout')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('ContainerDeliveryConfirmedWithSite')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('FirstLiftConfirmed')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('RollOutForThisContainerCompleted')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('GeneralNotes')->nullable();
            $table->smallInteger('RolloutStatus')->default(0)->comment('Linked to tblrolloutstatus');
            $table->tinyInteger('RolloutScheduleContainerDay')->nullable()->default(0)->comment('0=Sun,1=Mon,2=Tue,3=Wed,4=Thu,5=Fri,6=Sat');
            $table->integer('DuplicatedFrom')->default(0)->comment('Its ID from which this record was duplicated');
            $table->integer('ContractDocumentID')->default(0);
            $table->decimal('Percentage', 5)->default(0);
            $table->string('SupplierEmail', 250)->nullable();
            $table->tinyInteger('ContractRequestedCb')->default(0);
            $table->tinyInteger('ContractReceivedCb')->default(0);
            $table->tinyInteger('DateNoticeOfTerminationSubmittedCb')->default(0);
            $table->tinyInteger('DateNoticeOfTerminationAcceptedCb')->default(0);
            $table->tinyInteger('DateOfRolloutCallToSiteCb')->default(0);
            $table->tinyInteger('PurchaseOrderSentToSupplierCb')->default(0);
            $table->tinyInteger('ContainerDeliveryConfirmedWithSiteCb')->default(0);
            $table->tinyInteger('SupplierConfirmedServiceRolloutCb')->default(0);
            $table->tinyInteger('FirstLiftConfirmedCb')->default(0);
            $table->tinyInteger('RollOutForThisContainerCompletedCb')->default(0);
            $table->tinyInteger('ContractAnniversaryDateCb')->default(0);
            $table->tinyInteger('NoticePeriodStartCb')->default(0);
            $table->tinyInteger('NoticePeriodEndCb')->default(0);
            $table->tinyInteger('SiteTerminationDateCb')->default(0);
            $table->tinyInteger('NewContainerDeliveryDateCb')->default(0);
            $table->integer('CreatedBy')->default(0);
            $table->timestamp('DateCreated')->useCurrent();
            $table->integer('ModifiedBy')->default(0);
            $table->dateTime('DateModified')->nullable()->default('0000-00-00 00:00:00');
            $table->string('RolloutScheduleContainerText', 500)->nullable();
            $table->dateTime('FirstLiftDate')->nullable()->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblrollout');
    }
}
