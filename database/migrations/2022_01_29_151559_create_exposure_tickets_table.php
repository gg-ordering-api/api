<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExposureTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exposure_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exposure_id')->index('exposure_tickets_exposure_id_foreign');
            $table->integer('ticket_id');
            $table->char('ticket_type', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exposure_tickets');
    }
}
