<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPoNumberCheckedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('po_number_checked', function (Blueprint $table) {
            $table->foreign(['client_id'], 'po_number_checked_tblclients_ID_fk')->references(['ID'])->on('tblclients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('po_number_checked', function (Blueprint $table) {
            $table->dropForeign('po_number_checked_tblclients_ID_fk');
        });
    }
}
