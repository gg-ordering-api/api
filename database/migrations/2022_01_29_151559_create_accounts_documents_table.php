<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts_documents', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('accounts_documents_types_id')->nullable();
            $table->string('file_type', 5);
            $table->string('file_name');
            $table->string('file_hash', 64);
            $table->longText('path');
            $table->unsignedInteger('created_by');
            $table->timestamp('created_at')->useCurrentOnUpdate()->useCurrent();
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'failed', 'yes'])->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts_documents');
    }
}
