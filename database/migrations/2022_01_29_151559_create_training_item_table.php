<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_item', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('training_type_id')->index('training_item_training_type_id_foreign');
            $table->integer('department_id')->default(0);
            $table->unsignedInteger('training_role_id')->nullable()->index('training_item_training_role_id_fk');
            $table->char('name', 200)->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('deleted')->default(0);
            $table->timestamp('created_timestamp')->useCurrent();
            $table->integer('created_by');
            $table->timestamp('updated_timestamp')->useCurrentOnUpdate()->useCurrent();
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_item');
    }
}
