<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSiteDamageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_damage', function (Blueprint $table) {
            $table->foreign(['created_by'], 'site_damage_tblusers_ID_fk')->references(['ID'])->on('tblusers');
            $table->foreign(['deleted_by'], 'site_damage_tblusers_ID_fk_3')->references(['ID'])->on('tblusers');
            $table->foreign(['site_id'], 'site_damage_tblsites_ID_fk')->references(['ID'])->on('tblsites');
            $table->foreign(['updated_by'], 'site_damage_tblusers_ID_fk_2')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_damage', function (Blueprint $table) {
            $table->dropForeign('site_damage_tblusers_ID_fk');
            $table->dropForeign('site_damage_tblusers_ID_fk_3');
            $table->dropForeign('site_damage_tblsites_ID_fk');
            $table->dropForeign('site_damage_tblusers_ID_fk_2');
        });
    }
}
