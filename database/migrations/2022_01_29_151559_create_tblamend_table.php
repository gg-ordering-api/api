<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblamendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblamend', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SiteID')->default('0')->index('tblamend_SiteID_index');
            $table->unsignedBigInteger('ClientID')->default('0')->index('tblamend_ClientID_index');
            $table->integer('SentFrom');
            $table->timestamp('DateSubmitted')->useCurrent()->index('tblamend_DateSubmitted_index');
            $table->text('Notes');
            $table->tinyInteger('Status')->default(1);
            $table->integer('PickupBy')->default(0);
            $table->dateTime('PickupDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('CompletionDate')->nullable()->default('0000-00-00 00:00:00')->index('tblamend_CompletionDate_index');
            $table->text('CompletionNote')->nullable();
            $table->integer('AssignedTo')->nullable()->default(0);
            $table->smallInteger('BookingPriority')->nullable()->default(0);
            $table->integer('TID')->default(0)->index('tblamend_TID_index');
            $table->tinyInteger('Department')->default(0);
            $table->tinyInteger('ForDepartment')->default(0);
            $table->boolean('TicketType')->default(false);
            $table->integer('CategoryID')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblamend');
    }
}
