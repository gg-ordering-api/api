<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbl208Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl208', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SiteID')->index('SiteID');
            $table->string('KPITarget', 200)->nullable();
            $table->string('Planned', 200)->nullable();
            $table->string('Likely', 200)->nullable();
            $table->mediumText('KPIComments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl208');
    }
}
