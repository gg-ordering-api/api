<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToAppraisalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal', function (Blueprint $table) {
            $table->foreign(['appraisal_type_id'], 'appraisal_appraisal_type_id_fk')->references(['id'])->on('appraisal_type');
            $table->foreign(['appraisal_status_id'], 'appraisal_appraisal_status_id_fk')->references(['id'])->on('appraisal_status');
            $table->foreign(['user_id'], 'appraisal_tblusers_ID_fk')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal', function (Blueprint $table) {
            $table->dropForeign('appraisal_appraisal_type_id_fk');
            $table->dropForeign('appraisal_appraisal_status_id_fk');
            $table->dropForeign('appraisal_tblusers_ID_fk');
        });
    }
}
