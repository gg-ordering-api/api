<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteDamageDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_damage_document', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('site_damage_id')->nullable()->index('site_damage_document_site_damage_id_fk');
            $table->unsignedInteger('documents_id')->nullable()->index('site_damage_document_documents_id_fk');
            $table->boolean('deleted')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_damage_document');
    }
}
