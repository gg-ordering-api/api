<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTblclientsitewtnpodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblclientsitewtnpod', function (Blueprint $table) {
            $table->foreign(['gogreen_order_id'], 'tblclientsitewtnpod_gogreen_order_number_id_fk')->references(['id'])->on('gogreen_order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblclientsitewtnpod', function (Blueprint $table) {
            $table->dropForeign('tblclientsitewtnpod_gogreen_order_number_id_fk');
        });
    }
}
