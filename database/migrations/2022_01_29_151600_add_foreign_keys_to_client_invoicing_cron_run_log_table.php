<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientInvoicingCronRunLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_invoicing_cron_run_log', function (Blueprint $table) {
            $table->foreign(['created_by'], 'client_invoicing_cron_run_log_users_fk')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_invoicing_cron_run_log', function (Blueprint $table) {
            $table->dropForeign('client_invoicing_cron_run_log_users_fk');
        });
    }
}
