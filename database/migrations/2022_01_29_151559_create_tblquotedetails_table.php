<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblquotedetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblquotedetails', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('QuoteID')->default('0')->index('QuoteID');
            $table->unsignedBigInteger('ContainerID')->default('0')->index('ContainerID');
            $table->integer('ActualContainer')->default(0);
            $table->dateTime('ActualDate')->nullable()->default('0000-00-00 00:00:00');
            $table->bigInteger('Supplier')->default(0)->index('Supplier');
            $table->bigInteger('WasteDestination')->default(0);
            $table->mediumText('BuyerNote')->nullable();
            $table->decimal('SupplierTransCharge', 10)->default(0);
            $table->decimal('SupplierWeightCharge', 10)->default(0);
            $table->decimal('SupplierIncMinTonnes', 10)->default(0);
            $table->boolean('TonnageType')->default(false);
            $table->decimal('SupplierRebate', 10)->default(0);
            $table->decimal('SupplierBaySusCharge', 10)->default(0);
            $table->decimal('SupplierPermitCharge', 10)->default(0);
            $table->decimal('SupplierOutOfHoursCharge', 10)->default(0);
            $table->decimal('SupplierWLCharge', 10)->default(0);
            $table->decimal('SupplierWLHours', 10)->default(0);
            $table->decimal('TransMarkup', 10)->default(25);
            $table->decimal('TonnageMarkup', 10)->default(25);
            $table->decimal('RebatePercent', 10)->default(50);
            $table->decimal('WLMarkup', 10)->default(25);
            $table->decimal('PermMarkup', 10)->default(25);
            $table->decimal('OOHMarkup', 10)->default(25);
            $table->decimal('BaySusMarkup', 10)->default(25);
            $table->decimal('ClientTransport', 10)->default(0);
            $table->decimal('ClientPerTonne', 10)->default(0);
            $table->decimal('ClientIncMinTonnes', 10)->default(0);
            $table->decimal('ClientRebate', 10)->default(0);
            $table->decimal('ClientWLCharge', 10)->default(0);
            $table->decimal('ClientWLHours', 10)->default(0);
            $table->decimal('ClientPermitCharge', 10)->default(0);
            $table->decimal('ClientOOHCharge', 10)->default(0);
            $table->decimal('ClientBaySusCharge', 10)->default(0);
            $table->boolean('IsBooked')->default(false);
            $table->smallInteger('ClientTonneageType')->default(0);
            $table->boolean('ReqComplianceApproval')->default(false);
            $table->boolean('ComplianceApproved')->default(false);
            $table->integer('ComplianceApprover')->default(0)->index('ComplianceApprover');
            $table->dateTime('ComplianceApprovalDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('ComplianceNote')->nullable();
            $table->boolean('ComplianceRejected')->default(false);
            $table->dateTime('TimeSentToCompliance')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('SupplierContact')->nullable();
            $table->boolean('CanProvideBreakdown')->default(false);
            $table->string('SupplierContactEmail', 50)->nullable();
            $table->timestamp('QuoteTime')->useCurrent();
            $table->boolean('CanProvideLights')->default(false);
            $table->boolean('HasTracker')->default(false);
            $table->boolean('RecommendedSupplier')->default(false);
            $table->boolean('HasRebate')->default(false);
            $table->mediumText('SpecialRateNotes')->nullable();

            $table->index(['ComplianceApprover', 'TimeSentToCompliance', 'ComplianceApproved'], 'StaffActivityReport');
            $table->index(['TimeSentToCompliance', 'QuoteID'], 'tblquotedetails_TimeSentToCompliance_QuoteID_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblquotedetails');
    }
}
