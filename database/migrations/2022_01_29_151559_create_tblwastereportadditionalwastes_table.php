<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblwastereportadditionalwastesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblwastereportadditionalwastes', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('ReportID')->default(0)->index('ReportID');
            $table->integer('WasteID')->default(0);
            $table->decimal('Addper', 20, 4)->default(0);
            $table->decimal('Addyd', 20, 4)->default(0);
            $table->double('Addconv', 20, 4)->default(0);
            $table->decimal('Addtotal', 20, 4)->default(0);
            $table->decimal('Addrecper', 20, 4)->default(0);
            $table->decimal('Addrectonnes', 20, 4)->default(0);
            $table->mediumText('Addnotes')->nullable();
            $table->mediumText('VariousWasteTypes')->nullable();
            $table->decimal('Addrecoper', 10, 4)->default(0);
            $table->decimal('Addrecotonnes', 10, 4)->default(0);
            $table->mediumText('Addreconote')->nullable();
            $table->text('tickets')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblwastereportadditionalwastes');
    }
}
