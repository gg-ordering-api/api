<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbloldportalusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbloldportalusers', function (Blueprint $table) {
            $table->string('EmailAddress', 300)->nullable();
            $table->string('UserName', 300)->nullable();
            $table->string('PassWord', 300)->nullable();
            $table->string('InternalCode', 100)->nullable();
            $table->increments('ID');
            $table->string('FullName', 300)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbloldportalusers');
    }
}
