<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOcrMigrationLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ocr_migration_log', function (Blueprint $table) {
            $table->foreign(['ocr_error_id'], 'ocr_migration_log_ocr_errors_id_fk')->references(['id'])->on('ocr_errors');
            $table->foreign(['go_greeen_order_id'], 'ocr_migration_log_gogreen_order_number_id_fk')->references(['id'])->on('gogreen_order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocr_migration_log', function (Blueprint $table) {
            $table->dropForeign('ocr_migration_log_ocr_errors_id_fk');
            $table->dropForeign('ocr_migration_log_gogreen_order_number_id_fk');
        });
    }
}
