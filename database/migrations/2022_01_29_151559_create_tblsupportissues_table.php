<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsupportissuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsupportissues', function (Blueprint $table) {
            $table->integer('ID', true);
            $table->unsignedMediumInteger('UserID')->index('UserID');
            $table->string('Title')->nullable();
            $table->text('Details')->nullable();
            $table->string('Priority', 45)->nullable();
            $table->string('Category', 45)->nullable();
            $table->unsignedMediumInteger('GitlabIssueID')->nullable();
            $table->text('URL')->nullable();
            $table->dateTime('AddedDate')->nullable()->default('0000-00-00 00:00:00');
            $table->unsignedBigInteger('GitlabProjectID')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsupportissues');
    }
}
