<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcrOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocr_order', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('ticket_id');
            $table->integer('type_id')->index('ocr_order_ocr_order_type_type_id_fk');
            $table->integer('year');
            $table->string('check_digit', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocr_order');
    }
}
