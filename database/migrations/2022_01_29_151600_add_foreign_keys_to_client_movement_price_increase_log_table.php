<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientMovementPriceIncreaseLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_movement_price_increase_log', function (Blueprint $table) {
            $table->foreign(['client_id'], 'client_movement_price_increase_log_tblclients_ID_fk')->references(['ID'])->on('tblclients');
            $table->foreign(['created_by'], 'client_movement_price_increase_log_tblusers_ID_fk')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_movement_price_increase_log', function (Blueprint $table) {
            $table->dropForeign('client_movement_price_increase_log_tblclients_ID_fk');
            $table->dropForeign('client_movement_price_increase_log_tblusers_ID_fk');
        });
    }
}
