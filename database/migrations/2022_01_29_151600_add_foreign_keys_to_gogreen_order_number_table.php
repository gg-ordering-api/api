<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGogreenOrderNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gogreen_order_number', function (Blueprint $table) {
            $table->foreign(['linked_order'], 'gogreen_order_number_gogreen_order_number_id_fkiii')->references(['id'])->on('gogreen_order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gogreen_order_number', function (Blueprint $table) {
            $table->dropForeign('gogreen_order_number_gogreen_order_number_id_fkiii');
        });
    }
}
