<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutoSupplierSkipPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_supplier_skip_price', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('supplier_id');
            $table->integer('container_id');
            $table->integer('waste_type_id')->nullable();
            $table->decimal('price')->nullable();
            $table->decimal('permit_price')->nullable();
            $table->string('postcode', 10)->nullable();
            $table->integer('rank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_supplier_skip_price');
    }
}
