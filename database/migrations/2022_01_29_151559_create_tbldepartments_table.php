<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbldepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldepartments', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('DepName', 100)->nullable();
            $table->integer('DirectorID')->default(0)->index('DirectorID');
            $table->string('NotificationEmail', 50)->nullable();
            $table->string('PhoneGroup', 15)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldepartments');
    }
}
