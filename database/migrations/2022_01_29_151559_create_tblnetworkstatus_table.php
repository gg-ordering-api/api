<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblnetworkstatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblnetworkstatus', function (Blueprint $table) {
            $table->tinyInteger('EmailStatus')->default(0);
            $table->tinyInteger('InternetStatus')->default(0);
            $table->tinyInteger('ShareStatus')->default(0);
            $table->tinyInteger('PortalStatus')->default(0);
            $table->tinyInteger('WebsiteStatus')->default(0);
            $table->tinyInteger('SIDStatus')->default(0);
            $table->mediumText('NotesStatus')->nullable();
            $table->dateTime('LastUpdated')->nullable()->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblnetworkstatus');
    }
}
