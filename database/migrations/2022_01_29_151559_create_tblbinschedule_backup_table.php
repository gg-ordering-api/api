<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblbinscheduleBackupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblbinschedule_backup', function (Blueprint $table) {
            $table->unsignedInteger('ID')->nullable();
            $table->bigInteger('SiteID')->nullable();
            $table->bigInteger('ClientID')->nullable();
            $table->integer('WasteType')->nullable();
            $table->integer('ContainerType')->nullable();
            $table->decimal('ContainerWeight', 10, 4)->nullable();
            $table->decimal('SupplierLiftPrice', 10)->nullable();
            $table->decimal('LiftMargin', 10)->nullable();
            $table->decimal('ClientLiftPrice', 10)->nullable();
            $table->decimal('SupplierPerTonne', 10)->nullable();
            $table->decimal('PerTonneMargin', 10)->nullable();
            $table->decimal('ClientPerTonne', 10)->nullable();
            $table->decimal('SupplierIncluding', 10)->nullable();
            $table->decimal('ClientIncluding', 10)->nullable();
            $table->decimal('RecycleRate', 10)->nullable();
            $table->integer('UsedSupplier')->nullable();
            $table->integer('WasteDestination')->nullable();
            $table->tinyInteger('DivertedLandfill')->nullable();
            $table->smallInteger('MondayNumber')->nullable();
            $table->smallInteger('MondayFreq')->nullable();
            $table->smallInteger('TuesdayNumber')->nullable();
            $table->smallInteger('TuesdayFreq')->nullable();
            $table->smallInteger('WednesdayNumber')->nullable();
            $table->smallInteger('WednesdayFreq')->nullable();
            $table->smallInteger('ThursdayNumber')->nullable();
            $table->smallInteger('ThursdayFreq')->nullable();
            $table->smallInteger('FridayNumber')->nullable();
            $table->smallInteger('FridayFreq')->nullable();
            $table->smallInteger('SaturdayNumber')->nullable();
            $table->smallInteger('SaturdayFreq')->nullable();
            $table->smallInteger('SundayNumber')->nullable();
            $table->smallInteger('SundayFreq')->nullable();
            $table->dateTime('LastCommitted')->nullable();
            $table->integer('LastEditBy')->nullable();
            $table->dateTime('LastEditDate')->nullable();
            $table->integer('BillingCode')->nullable();
            $table->integer('isQuote')->nullable();
            $table->integer('QuoteContainerID')->nullable();
            $table->dateTime('DateOfIncreases')->nullable();
            $table->dateTime('increase_from_date')->nullable();
            $table->decimal('SupplierLiftPriceNew', 10)->nullable();
            $table->decimal('SupplierPerTonneNew', 10)->nullable();
            $table->decimal('ClientLiftPriceNew', 10)->nullable();
            $table->decimal('ClientPerTonneNew', 10)->nullable();
            $table->dateTime('supplier_increase_from_date')->nullable();
            $table->dateTime('supplierIncreaseDate')->nullable();
            $table->tinyInteger('site_held')->nullable();
            $table->tinyInteger('container_ticked')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblbinschedule_backup');
    }
}
