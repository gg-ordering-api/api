<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientInvoicingTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_invoicing_type', function (Blueprint $table) {
            $table->foreign(['ocr_order_type_id'], 'client_invoicing_type_order_type_fk')->references(['type_id'])->on('ocr_order_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_invoicing_type', function (Blueprint $table) {
            $table->dropForeign('client_invoicing_type_order_type_fk');
        });
    }
}
