<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblapikeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblapikeys', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('APIKey', 200)->nullable();
            $table->string('APIUsername', 50)->nullable();
            $table->string('APIPassword', 200)->nullable();
            $table->string('APIDescription', 200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblapikeys');
    }
}
