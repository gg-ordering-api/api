<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_status', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code', 25)->nullable()->index();
            $table->string('name')->nullable();
            $table->tinyInteger('deleted')->default(0);
            $table->timestamp('created_timestamp')->useCurrent();
            $table->integer('created_by');
            $table->timestamp('updated_timestamp')->useCurrentOnUpdate()->useCurrent();
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_status');
    }
}
