<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblpremcodesOLDTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblpremcodesOLD', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SiteID');
            $table->string('PremisesCode', 20)->nullable();
            $table->string('ExpiryDate', 10)->nullable();
            $table->integer('WasteType')->nullable()->default(0);
            $table->bigInteger('HazCert')->default(0);
            $table->boolean('ExpiryWarned')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpremcodesOLD');
    }
}
