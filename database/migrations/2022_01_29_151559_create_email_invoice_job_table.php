<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailInvoiceJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_invoice_job', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('status_id');
            $table->string('info', 500)->nullable();
            $table->integer('triggered_by');
            $table->dateTime('triggered_at')->useCurrent();
            $table->dateTime('finished_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_invoice_job');
    }
}
