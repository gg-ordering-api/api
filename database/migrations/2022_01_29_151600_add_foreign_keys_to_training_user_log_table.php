<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTrainingUserLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_user_log', function (Blueprint $table) {
            $table->foreign(['training_user_id'])->references(['id'])->on('training_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_user_log', function (Blueprint $table) {
            $table->dropForeign('training_user_log_training_user_id_foreign');
        });
    }
}
