<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTrainingItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_item', function (Blueprint $table) {
            $table->foreign(['training_type_id'])->references(['id'])->on('training_type');
            $table->foreign(['training_role_id'], 'training_item_training_role_id_fk')->references(['id'])->on('training_role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_item', function (Blueprint $table) {
            $table->dropForeign('training_item_training_type_id_foreign');
            $table->dropForeign('training_item_training_role_id_fk');
        });
    }
}
