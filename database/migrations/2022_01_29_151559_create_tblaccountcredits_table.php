<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblaccountcreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblaccountcredits', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('ClientID')->default(0);
            $table->decimal('PayInValue', 20)->default(0);
            $table->integer('PayInUser');
            $table->timestamp('PayInTime')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblaccountcredits');
    }
}
