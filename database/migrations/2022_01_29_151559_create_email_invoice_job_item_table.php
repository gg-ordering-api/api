<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailInvoiceJobItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_invoice_job_item', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('job_id');
            $table->integer('invoice_id');
            $table->string('info', 500)->nullable();
            $table->string('sent_to_email', 181)->nullable();
            $table->string('status', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_invoice_job_item');
    }
}
