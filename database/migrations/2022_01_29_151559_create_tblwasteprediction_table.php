<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblwastepredictionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblwasteprediction', function (Blueprint $table) {
            $table->unsignedMediumInteger('WID')->default('0');
            $table->unsignedInteger('SiteID')->default('0');
            $table->decimal('Tonnes', 10, 4)->unsigned()->default(0);

            $table->primary(['WID', 'SiteID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblwasteprediction');
    }
}
