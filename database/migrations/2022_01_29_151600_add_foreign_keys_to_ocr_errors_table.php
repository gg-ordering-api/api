<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOcrErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ocr_errors', function (Blueprint $table) {
            $table->foreign(['completed_by'], 'ocr_errors_tblusers_ID_fk')->references(['ID'])->on('tblusers');
            $table->foreign(['ignored_by'], 'ocr_errors_tblusers_ID_fk_2')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocr_errors', function (Blueprint $table) {
            $table->dropForeign('ocr_errors_tblusers_ID_fk');
            $table->dropForeign('ocr_errors_tblusers_ID_fk_2');
        });
    }
}
