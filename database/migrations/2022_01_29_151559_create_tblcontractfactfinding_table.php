<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblcontractfactfindingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblcontractfactfinding', function (Blueprint $table) {
            $table->unsignedBigInteger('ClientID')->unique('ClientID');
            $table->integer('MarketSector');
            $table->integer('CurrentProvider');
            $table->smallInteger('NoLocations');
            $table->boolean('NationalContract')->default(false);
            $table->date('ContractExpiryDate')->nullable();
            $table->date('ActionDate')->nullable();
            $table->integer('ActionUser')->default(0);
            $table->dateTime('ActionUserDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('IsUrgent')->default(false);
            $table->decimal('Turnover', 15)->default(0);
            $table->decimal('CreditRating', 10)->default(0);
            $table->string('WhoElseUsed', 300)->nullable();
            $table->string('ReasonsForCompetitor', 300)->nullable();
            $table->string('WhoTheyWorkFor', 300)->nullable();
            $table->string('AreaCovered', 1000)->nullable();
            $table->string('WhoCanOrder', 300)->nullable();
            $table->integer('AnnualWasteSpend')->nullable();
            $table->dateTime('LastRingRound')->nullable()->default('0000-00-00 00:00:00');
            $table->string('PortalReportsViewed', 300)->nullable();
            $table->string('SisterCompanies', 300)->nullable();
            $table->string('PortalUsedEfficiently', 100)->nullable();
            $table->string('ScheduleOfWorks', 1000)->nullable();
            $table->string('HowOrderNosWork', 1000)->nullable();
            $table->string('PercentWorkWon', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblcontractfactfinding');
    }
}
