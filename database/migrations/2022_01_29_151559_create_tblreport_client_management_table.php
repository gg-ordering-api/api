<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblreportClientManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblreport_client_management', function (Blueprint $table) {
            $table->increments('ID');
            $table->decimal('ThisYearTurnOver', 20)->default(0);
            $table->integer('ClientID')->default(0);
            $table->decimal('LastYearTurnOver', 20)->default(0);
            $table->decimal('ActualGrowth', 10)->default(0);
            $table->decimal('PercentageGrowth', 5)->default(0);
            $table->integer('TotalQuotes')->default(0);
            $table->integer('WonQuotes')->default(0);
            $table->integer('LostQuotes')->default(0);
            $table->integer('PendingQuotes')->default(0);
            $table->mediumText('Reason')->nullable();
            $table->integer('CompletionUser')->default(0);
            $table->dateTime('CompletionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->date('ReportDate')->nullable();
            $table->boolean('IsLocked')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblreport_client_management');
    }
}
