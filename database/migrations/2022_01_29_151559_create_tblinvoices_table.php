<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblinvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblinvoices', function (Blueprint $table) {
            $table->increments('ID')->index('ID');
            $table->bigInteger('ClientID')->default(0)->index('ClientID');
            $table->bigInteger('SiteID')->default(0);
            $table->string('PONumber', 100)->nullable();
            $table->timestamp('CreationDate')->useCurrent();
            $table->decimal('InvoiceValue', 20)->default(0);
            $table->boolean('IsPaid')->default(false);
            $table->integer('MarkedPaidBy')->default(0)->index('MarkedPaidBy');
            $table->boolean('SentToClient')->default(false);
            $table->boolean('ExportedToSage')->default(false);
            $table->dateTime('InvoiceDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('PaidDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('DateSentClient')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SendingUser')->default(0);
            $table->decimal('OutstandingBalance', 20)->default(0);
            $table->boolean('AllPODReceived')->default(false);
            $table->boolean('IsCreditInvoice')->default(false);
            $table->boolean('ExportedToTradex')->default(false);
            $table->boolean('IsSWMPCharge')->default(false);
            $table->boolean('IsWTR')->default(false);
            $table->boolean('disputeStatusUpdate')->default(false);
            $table->boolean('queue')->nullable()->default(false);
            $table->tinyInteger('invoiceisent')->nullable()->default(0);
            $table->integer('is_pcard')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblinvoices');
    }
}
