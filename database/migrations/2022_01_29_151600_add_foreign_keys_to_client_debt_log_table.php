<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientDebtLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_debt_log', function (Blueprint $table) {
            $table->foreign(['client_id'], 'client_on_stop_log_tblclients_ID_fk')->references(['ID'])->on('tblclients');
            $table->foreign(['updated_by'], 'client_on_stop_log_tblusers_ID_fk_2')->references(['ID'])->on('tblusers');
            $table->foreign(['client_debt_status_id'], 'client_debt_log_client_debt_status_id_fk')->references(['id'])->on('client_debt_status');
            $table->foreign(['created_by'], 'client_on_stop_log_tblusers_ID_fk')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_debt_log', function (Blueprint $table) {
            $table->dropForeign('client_on_stop_log_tblclients_ID_fk');
            $table->dropForeign('client_on_stop_log_tblusers_ID_fk_2');
            $table->dropForeign('client_debt_log_client_debt_status_id_fk');
            $table->dropForeign('client_on_stop_log_tblusers_ID_fk');
        });
    }
}
