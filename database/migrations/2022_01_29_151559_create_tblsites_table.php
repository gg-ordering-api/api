<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsites', function (Blueprint $table) {
            $table->increments('ID')->unique('tblsites_ID_uindex');
            $table->string('SiteName', 200)->nullable();
            $table->mediumText('SiteAddress')->nullable();
            $table->string('SitePostcode', 10)->nullable();
            $table->string('SiteContact', 200)->nullable();
            $table->string('SiteTelephone', 100)->nullable();
            $table->string('SiteEmail', 200)->nullable();
            $table->decimal('SiteValue', 20)->default(0);
            $table->decimal('AllowableLandfill', 20)->default(0);
            $table->mediumInteger('SiteType')->default(1);
            $table->mediumInteger('SiteStatus')->default(0);
            $table->bigInteger('CreatedBy')->default(0)->index('CreatedBy');
            $table->timestamp('DateCreated')->useCurrent()->index('DateCreated');
            $table->dateTime('LastUpdated')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('IsCompleted')->default(false);
            $table->bigInteger('CompanyID')->default(0)->index('CompanyID');
            $table->dateTime('StartDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('EndDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('IsSwmp')->default(false);
            $table->string('SiteLong', 100)->nullable();
            $table->string('SiteLat', 100)->nullable();
            $table->decimal('SiteArea', 10)->nullable();
            $table->boolean('IsWasteReport')->default(false);
            $table->smallInteger('WRFrequency')->default(0);
            $table->dateTime('WRDueDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('EstSkips')->default(0);
            $table->mediumText('SiteLimitations')->nullable();
            $table->string('PermitCode', 50)->nullable();
            $table->unsignedBigInteger('PermitFile')->default('0');
            $table->dateTime('PermitExpiry')->nullable()->default('0000-00-00 00:00:00');
            $table->string('SitePO', 50)->nullable();
            $table->mediumText('SiteAlert')->nullable();
            $table->bigInteger('WarningValue')->default(0);
            $table->integer('WarningPercent')->default(60);
            $table->mediumText('WarningEmails')->nullable();
            $table->boolean('ProgressWarned')->default(false);
            $table->string('CustomerSiteIdentifier', 50)->nullable();
            $table->boolean('SiteOnStop')->default(false);
            $table->decimal('CarbonUsage', 20, 4)->default(0);
            $table->decimal('CarbonUsageKGL', 20, 4)->default(0);
            $table->boolean('WorksNights')->default(false);
            $table->mediumText('AdditionalPricingInfo')->nullable();
            $table->boolean('SWMPNotifiedWon')->default(false);
            $table->decimal('TransIncrease', 10)->default(0);
            $table->decimal('WLIncrease', 10)->default(0);
            $table->decimal('PerTonIncrease', 10, 0)->default(0);
            $table->dateTime('SiteAlertSetDate')->nullable()->default('0000-00-00 00:00:00');
            $table->string('CommercialPO', 50)->nullable();
            $table->boolean('ComplianceChecked')->default(false);
            $table->integer('CompCheckedBy')->default(0);
            $table->dateTime('CompCheckDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('WastePackSent')->default(false);
            $table->integer('WastePackSentBy')->default(0);
            $table->dateTime('WastePackSentDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('CheckNotes')->nullable();
            $table->dateTime('LastSupplierDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('SiteVisited')->default(false);
            $table->mediumText('CorrespondenceAddress')->nullable();
            $table->tinyInteger('IsMcdonalds')->default(0);
            $table->integer('ClientCustomer')->default(0)->index('ClientCustomer');
            $table->string('DescriptionOfWorks', 75)->nullable();
            $table->string('ApprovalDirector', 100)->nullable();
            $table->dateTime('ApprovalDate')->nullable()->default('0000-00-00 00:00:00');
            $table->string('SiteManager', 60)->nullable();
            $table->mediumText('AccountsNote')->nullable();
            $table->boolean('ExpiryWarned')->default(false);
            $table->tinyInteger('DOCSigned')->default(0);
            $table->dateTime('DOCExpiry')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('IsProblemSite')->default(false);
            $table->boolean('IsPermSite')->default(false);
            $table->boolean('IsTeepSite')->default(false);
            $table->mediumText('TeepNotes')->nullable();
            $table->decimal('WasteGreen', 6)->default(0);
            $table->decimal('WasteOrange', 6)->default(0);
            $table->decimal('WasteRed', 6)->default(0);
            $table->smallInteger('RRTarget')->default(0);
            $table->boolean('POApproved')->default(false);
            $table->string('AdministrativeRegion', 20)->nullable();
            $table->smallInteger('RequiresRollout')->default(0);
            $table->dateTime('RolloutCompletionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('PrimaryRolloutHandler')->default(0);
            $table->tinyInteger('PhysicalSiteAuditRequired')->default(0)->index('PhysicalSiteAuditRequired');
            $table->dateTime('PhysicalSiteAuditCompleted')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('TelephoneAudit')->default(0);
            $table->dateTime('TelephoneAuditCompleted')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('AdditionalAuditsRequired')->nullable()->default(0);
            $table->dateTime('SiteRolloutCompleted')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('RolloutPricelistDocumentID')->default(0);
            $table->text('PDAEmailAlerts')->nullable();
            $table->integer('PrimaryContact')->nullable()->default(0);
            $table->string('wtnnotificationemail')->nullable();
            $table->integer('client_region_id')->nullable();
            $table->integer('SiteAlertSetBy')->nullable();
            $table->longText('scheduled_services_notes')->nullable()->default('<b>Site Opening Times:</b> <br /><b>Bin Access Times:</b> <br /><b>Bin Location:</b> <br /><b>Bin Access Instructions:</b>');
            $table->integer('site_bank_holiday_requirement_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsites');
    }
}
