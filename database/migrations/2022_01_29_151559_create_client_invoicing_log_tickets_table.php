<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientInvoicingLogTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_invoicing_log_tickets', function (Blueprint $table) {
            $table->integer('id', true)->index();
            $table->integer('log_id')->nullable()->index();
            $table->integer('ticket_id')->nullable();
            $table->string('po_number', 100)->nullable()->index();
            $table->integer('gg_order_number_id')->nullable()->index();
            $table->integer('status_id')->nullable()->default(1)->index('client_invoicing_log_tickets_client_invoicing_status_id_fk');
            $table->integer('error_id')->nullable()->index('client_invoicing_log_tickets_client_invoicing_error_types_id_fk');
            $table->dateTime('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_invoicing_log_tickets');
    }
}
