<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblmeetingminutesagendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblmeetingminutesagendas', function (Blueprint $table) {
            $table->integer('ID', true);
            $table->integer('AgendaItem')->default(0);
            $table->text('Title')->nullable();
            $table->text('ActionBy')->nullable();
            $table->date('DeadlineDate')->nullable();
            $table->integer('MeetingMinutesID')->default(0);
            $table->integer('ClientID')->default(0);
            $table->dateTime('DateCreated')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('DateModified')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('is_completed')->default(0);
            $table->integer('CreatedBy')->default(0);
            $table->integer('ModifiedBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblmeetingminutesagendas');
    }
}
