<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsiteinspectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsiteinspection', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('ClientID')->default(0);
            $table->integer('SiteID')->default(0);
            $table->integer('VisitID')->default(0);
            $table->dateTime('InspectionDate')->default('0000-00-00 00:00:00');
            $table->mediumText('SiteAuditor');
            $table->mediumText('SecondAuditor');
            $table->mediumText('StartTime');
            $table->mediumText('EndTime');
            $table->mediumText('SiteAddress');
            $table->integer('SiteType')->default(0);
            $table->tinyInteger('IsPhotos')->default(0);
            $table->tinyInteger('TypeOfProject')->default(0);
            $table->tinyInteger('TypeOfSite')->default(0);
            $table->string('TypeOfProperty')->nullable();
            $table->tinyInteger('TypeOfCommercial')->default(0);
            $table->tinyInteger('Access')->default(0);
            $table->tinyInteger('Position')->default(0);
            $table->tinyInteger('Segregated')->default(0);
            $table->tinyInteger('Contamination')->default(0);
            $table->tinyInteger('SkipFilled')->default(0);
            $table->tinyInteger('Shrubs')->default(0);
            $table->tinyInteger('Inert')->default(0);
            $table->tinyInteger('Hazardous')->default(0);
            $table->tinyInteger('Cardboard')->default(0);
            $table->tinyInteger('CorrectSkip')->default(0);
            $table->tinyInteger('Signs')->default(0);
            $table->mediumText('DesOfWaste');
            $table->mediumText('AccessNotes');
            $table->mediumText('PositionNotes');
            $table->mediumText('SegregatedNotes');
            $table->mediumText('ContaminationNotes');
            $table->mediumText('SkipFilledNotes');
            $table->mediumText('ShrubsNotes');
            $table->mediumText('InertNotes');
            $table->mediumText('HazardousNotes');
            $table->mediumText('CardboardNotes');
            $table->mediumText('CorrectSkipNotes');
            $table->mediumText('SignsNotes');
            $table->tinyInteger('WCL')->default(0);
            $table->tinyInteger('WML')->default(0);
            $table->tinyInteger('SWMP')->default(0);
            $table->mediumText('WCLNotes');
            $table->mediumText('WMLNotes');
            $table->mediumText('SWMPNotes');
            $table->dateTime('SWMPDate')->default('0000-00-00 00:00:00');
            $table->integer('RefNumber')->default(0);
            $table->dateTime('DateRevision')->default('0000-00-00 00:00:00');
            $table->mediumText('SuppOne');
            $table->mediumText('SuppTwo');
            $table->mediumText('SuppThree');
            $table->mediumText('SuppNameNumberComments');
            $table->tinyInteger('SuppOneDate')->default(0);
            $table->tinyInteger('SuppTwoDate')->default(0);
            $table->tinyInteger('SuppThreeDate')->default(0);
            $table->mediumText('SuppDateComments');
            $table->tinyInteger('SuppOneAddress')->default(0);
            $table->tinyInteger('SuppTwoAddress')->default(0);
            $table->tinyInteger('SuppThreeAddress')->default(0);
            $table->mediumText('SuppAddressComments');
            $table->tinyInteger('SuppOneWCL')->default(0);
            $table->tinyInteger('SuppTwoWCL')->default(0);
            $table->tinyInteger('SuppThreeWCL')->default(0);
            $table->mediumText('SuppWCLComments');
            $table->tinyInteger('SuppOneWML')->default(0);
            $table->tinyInteger('SuppTwoWML')->default(0);
            $table->tinyInteger('SuppThreeWML')->default(0);
            $table->mediumText('SuppWMLComments');
            $table->tinyInteger('SuppOneTicket')->default(0);
            $table->tinyInteger('SuppTwoTicket')->default(0);
            $table->tinyInteger('SuppThreeTicket')->default(0);
            $table->mediumText('SuppTicketComments');
            $table->tinyInteger('SuppOneDERW')->default(0);
            $table->tinyInteger('SuppTwoDERW')->default(0);
            $table->tinyInteger('SuppThreeDERW')->default(0);
            $table->mediumText('SuppDERWComments');
            $table->tinyInteger('SuppOneVehicle')->default(0);
            $table->tinyInteger('SuppTwoVehicle')->default(0);
            $table->tinyInteger('SuppThreeVehicle')->default(0);
            $table->mediumText('SuppVehicleComments');
            $table->mediumText('TypeOfProjectNotes');
            $table->mediumText('TypeOfSiteNotes');
            $table->mediumText('TypeOfPropertyNotes');
            $table->mediumText('TypeOfCommercialNotes');
            $table->integer('VisitCompleted')->default(0);
            $table->tinyInteger('SuppOneLOW')->default(0);
            $table->tinyInteger('SuppTwoLOW')->default(0);
            $table->tinyInteger('SuppThreeLOW')->default(0);
            $table->mediumText('SuppLOWComments');
            $table->tinyInteger('SuppOneWaste')->default(0);
            $table->tinyInteger('SuppTwoWaste')->default(0);
            $table->tinyInteger('SuppThreeWaste')->default(0);
            $table->mediumText('SuppWasteComments');
            $table->tinyInteger('SuppOneSkip')->default(0);
            $table->tinyInteger('SuppTwoSkip')->default(0);
            $table->tinyInteger('SuppThreeSkip')->default(0);
            $table->mediumText('SuppSkipComments');
            $table->tinyInteger('SuppOneCustomer')->default(0);
            $table->tinyInteger('SuppTwoCustomer')->default(0);
            $table->tinyInteger('SuppThreeCustomer')->default(0);
            $table->mediumText('SuppCustomerComments');
            $table->tinyInteger('SuppOneDriver')->default(0);
            $table->tinyInteger('SuppTwoDriver')->default(0);
            $table->tinyInteger('SuppThreeDriver')->default(0);
            $table->mediumText('SuppDriverComments');
            $table->tinyInteger('SuppOneSIC')->default(0);
            $table->tinyInteger('SuppTwoSIC')->default(0);
            $table->tinyInteger('SuppThreeSIC')->default(0);
            $table->mediumText('SuppSICComments');
            $table->tinyInteger('SuppOneHierarchy')->default(0);
            $table->tinyInteger('SuppTwoHierarchy')->default(0);
            $table->tinyInteger('SuppThreeHierarchy')->default(0);
            $table->mediumText('SuppHierarchyComments');
            $table->mediumText('OtherInformation');
            $table->mediumText('SkipDimensions');
            $table->string('SupplierNameAndNo1', 300)->nullable();
            $table->string('SupplierNameAndNo2', 300)->nullable();
            $table->string('auditorMet')->nullable();
            $table->mediumText('identifiedActions')->nullable();
            $table->tinyInteger('pointsCheck')->nullable();
            $table->mediumText('pointsCheckComments')->nullable();
            $table->tinyInteger('imageTaken')->nullable();
            $table->mediumText('inspectionNotes')->nullable();
            $table->mediumText('imageTakenComments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsiteinspection');
    }
}
