<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGoGreenTicketClickedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('go_green_ticket_clicked', function (Blueprint $table) {
            $table->foreign(['user_id'], 'go_green_ticket_clicked_tblusers_ID_fk')->references(['ID'])->on('tblusers');
            $table->foreign(['go_green_order_number_id'], 'go_green_ticket_clicked_gogreen_order_number_id_fk')->references(['id'])->on('gogreen_order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('go_green_ticket_clicked', function (Blueprint $table) {
            $table->dropForeign('go_green_ticket_clicked_tblusers_ID_fk');
            $table->dropForeign('go_green_ticket_clicked_gogreen_order_number_id_fk');
        });
    }
}
