<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_role', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 50)->nullable()->index('training_type_name_index');
            $table->tinyInteger('deleted')->default(0);
            $table->timestamp('created_timestamp')->useCurrent();
            $table->integer('created_by');
            $table->timestamp('updated_timestamp')->useCurrent();
            $table->integer('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_role');
    }
}
