<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsupplierrebateinvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsupplierrebateinvoices', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('SupplierID')->default(0)->index('SupplierID');
            $table->timestamp('CreationDate')->useCurrent();
            $table->dateTime('InvoiceDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('IsPaid')->default(false);
            $table->integer('MarkedPaidBy')->default(0);
            $table->dateTime('PaidDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('SentToSupplier')->default(false);
            $table->integer('SendingUser')->default(0);
            $table->dateTime('DateSentToSupplier')->nullable()->default('0000-00-00 00:00:00');
            $table->decimal('NetTotal', 7)->default(0);
            $table->decimal('VATValue', 7)->default(0);
            $table->decimal('GrossValue', 7)->default(0);
            $table->boolean('ExportedToSage')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsupplierrebateinvoices');
    }
}
