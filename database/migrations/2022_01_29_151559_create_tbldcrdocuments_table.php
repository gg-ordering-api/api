<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbldcrdocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldcrdocuments', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('ParentFolder')->default(0);
            $table->string('DocumentName', 100)->nullable();
            $table->string('DocumentLocation', 200)->nullable();
            $table->string('FirstIssue', 75)->nullable();
            $table->string('CurrentVersion', 75)->nullable();
            $table->mediumText('Comments')->nullable();
            $table->string('FileName', 500)->nullable();
            $table->string('Extension', 5)->nullable();
            $table->string('FileType', 1000)->nullable();
            $table->timestamp('DateAdded')->useCurrent();
            $table->bigInteger('AddedBy')->default(0);
            $table->dateTime('ExpiryDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('Notes')->nullable();
            $table->tinyInteger('is_hidden')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldcrdocuments');
    }
}
