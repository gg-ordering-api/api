<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblbincontainergroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblbincontainergroups', function (Blueprint $table) {
            $table->increments('ID');
            $table->bigInteger('SiteID')->default(0)->index('SiteID');
            $table->bigInteger('ClientID')->default(0)->index('ClientID');
            $table->bigInteger('WasteType')->default(0)->index('WasteType');
            $table->bigInteger('ContainerType')->default(0)->index('ContainerType');
            $table->integer('CreatedBy')->default(0);
            $table->timestamp('CreatedOn')->useCurrent();
            $table->smallInteger('ManagerApproved')->default(1);
            $table->dateTime('ApprovalDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('ApprovingUser')->default(0)->index('ApprovingUser');
            $table->mediumText('ApprovingReason')->nullable();
            $table->decimal('IncreaseLiftPrice', 10)->default(0);
            $table->boolean('IncreaseSet')->default(false);
            $table->boolean('IncreaseApplied')->default(false);
            $table->string('SupplierIncreaseAgree', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblbincontainergroups');
    }
}
