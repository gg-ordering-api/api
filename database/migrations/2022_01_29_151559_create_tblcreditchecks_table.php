<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblcreditchecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblcreditchecks', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('ClientID')->default('0')->index('ClientID');
            $table->decimal('CreditLimit', 10)->default(0);
            $table->decimal('SuggestedLimit', 10)->default(0);
            $table->decimal('CheckingAgency', 10)->default(0);
            $table->timestamp('DateOfCheck')->nullable()->useCurrent();
            $table->string('RegNumber', 50)->nullable()->index('tblcreditchecks_RegNumber_index');
            $table->string('IncorpDate', 200)->nullable();
            $table->decimal('TurnOver', 20)->default(0);
            $table->boolean('IsApproved')->default(false);
            $table->string('RatingFigure', 5)->nullable();
            $table->string('RatingText', 100)->nullable();
            $table->string('LastChangeDate', 50)->nullable();
            $table->decimal('InsuranceMaxLimit', 10)->default(0);
            $table->text('endorsements')->nullable();
            $table->binary('result')->nullable();
            $table->string('CreditSafeId', 100)->nullable();
            $table->dateTime('CreditSafeUpdateDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('cifUpdateDate')->nullable()->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblcreditchecks');
    }
}
