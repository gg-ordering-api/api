<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblSage200DisputesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblSage200Disputes', function (Blueprint $table) {
            $table->integer('idtblSage200Disputes', true);
            $table->string('Type', 2)->nullable();
            $table->string('AccountRef', 8)->nullable();
            $table->string('Reference', 20)->nullable();
            $table->string('Details', 45)->nullable();
            $table->boolean('DisputeFlag')->nullable();
            $table->boolean('SID_Updated')->nullable();
            $table->dateTime('DateTimeCreated')->nullable()->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblSage200Disputes');
    }
}
