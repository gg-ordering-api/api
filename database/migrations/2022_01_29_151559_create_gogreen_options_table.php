<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGogreenOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gogreen_options', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('gogreen_order_id')->nullable()->unique('gogreen_options_gogreen_order_id_uindex');
            $table->boolean('zero_charge')->nullable()->default(false);
            $table->boolean('missing_pod')->nullable()->default(false);
            $table->unsignedInteger('created_by')->nullable()->index('gogreen_options_tblusers_ID_fk');
            $table->dateTime('created_at')->nullable()->useCurrent();
            $table->unsignedInteger('updated_by')->nullable()->index('gogreen_options_tblusers_ID_fk_2');
            $table->dateTime('updated_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gogreen_options');
    }
}
