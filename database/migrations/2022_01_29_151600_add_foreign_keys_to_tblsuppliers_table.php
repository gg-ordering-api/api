<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTblsuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tblsuppliers', function (Blueprint $table) {
            $table->foreign(['country_id'], 'tblsuppliers_countries_id_fk')->references(['id'])->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tblsuppliers', function (Blueprint $table) {
            $table->dropForeign('tblsuppliers_countries_id_fk');
        });
    }
}
