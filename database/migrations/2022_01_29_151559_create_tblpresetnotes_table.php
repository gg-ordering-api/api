<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblpresetnotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblpresetnotes', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('NoteTitle', 100)->nullable();
            $table->mediumText('NoteBody')->nullable();
            $table->string('Departments', 500)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpresetnotes');
    }
}
