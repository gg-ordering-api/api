<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblcreditinsuranceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblcreditinsurance', function (Blueprint $table) {
            $table->unsignedBigInteger('ClientID')->default('0')->primary();
            $table->boolean('HasInsurance')->default(false);
            $table->smallInteger('ApprovalType')->default(0);
            $table->bigInteger('CSDocument')->default(0);
            $table->mediumText('InsuranceNotes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblcreditinsurance');
    }
}
