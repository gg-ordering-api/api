<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTicketSupplierInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_supplier_invoice', function (Blueprint $table) {
            $table->foreign(['supplier_invoice_id'], 'ticket_supplier_invoice_tblsupplierinvoices_ID_fk')->references(['ID'])->on('tblsupplierinvoices');
            $table->foreign(['go_green_order_id'], 'ticket_supplier_invoice_gogreen_order_number_id_fk')->references(['id'])->on('gogreen_order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_supplier_invoice', function (Blueprint $table) {
            $table->dropForeign('ticket_supplier_invoice_tblsupplierinvoices_ID_fk');
            $table->dropForeign('ticket_supplier_invoice_gogreen_order_number_id_fk');
        });
    }
}
