<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbldisputehandlingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldisputehandling', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('InvoiceNumber', 50)->nullable();
            $table->integer('HandlingUser')->default(0)->index('HandlingUser');
            $table->mediumText('HandleNotes')->nullable();
            $table->timestamp('HandlerSetDate')->useCurrent();
            $table->tinyInteger('Warning')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldisputehandling');
    }
}
