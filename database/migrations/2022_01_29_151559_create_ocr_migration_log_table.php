<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcrMigrationLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocr_migration_log', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('transaction_id')->nullable();
            $table->integer('ocr_error_id')->nullable()->index('ocr_migration_log_ocr_errors_id_fk');
            $table->integer('go_greeen_order_id')->nullable()->index('ocr_migration_log_gogreen_order_number_id_fk');
            $table->unsignedInteger('supplier_invoice_id')->nullable()->index('ocr_migration_log_tblsupplierinvoices_ID_fk');
            $table->string('type')->nullable();
            $table->timestamp('created_date')->useCurrent();
            $table->dateTime('assigned_date')->nullable();
            $table->integer('assigned_to')->nullable();
            $table->integer('go_green_order_id')->nullable();
            $table->integer('manual_import')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocr_migration_log');
    }
}
