<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index('appraisal_tblusers_ID_fk');
            $table->unsignedInteger('appraisal_status_id')->index('appraisal_appraisal_status_id_fk');
            $table->unsignedInteger('appraisal_type_id')->index('appraisal_appraisal_type_id_fk');
            $table->binary('data');
            $table->dateTime('due_date')->nullable()->default('0000-00-00 00:00:00');
            $table->binary('employee_signature');
            $table->dateTime('employee_date')->nullable()->default('0000-00-00 00:00:00');
            $table->binary('manager_signature');
            $table->dateTime('manager_date')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('deleted')->default(0);
            $table->timestamp('created_timestamp')->useCurrent();
            $table->integer('created_by');
            $table->timestamp('updated_timestamp')->useCurrentOnUpdate()->useCurrent();
            $table->integer('updated_by');
            $table->binary('hr_signature')->nullable();
            $table->dateTime('hr_date')->nullable()->default('0000-00-00 00:00:00');
            $table->string('manager_print', 50)->nullable();
            $table->string('hr_print', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal');
    }
}
