<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblclientoptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblclientoptions', function (Blueprint $table) {
            $table->unsignedBigInteger('ClientID')->default('0')->unique('ClientID')->comment('Links to ID field in tblclients');
            $table->smallInteger('SWMPReqs')->default(0);
            $table->mediumText('SWMPNote')->nullable();
            $table->boolean('NoRoRo')->default(false);
            $table->boolean('WeightLimitRoRo')->default(false);
            $table->decimal('RoRoMaxTonnage', 10)->default(0);
            $table->boolean('FullTickets')->default(false);
            $table->boolean('WTNTickets')->default(false);
            $table->boolean('ReqPO')->default(false);
            $table->tinyInteger('POType')->default(0);
            $table->smallInteger('WRReqs')->default(0);
            $table->mediumText('WRNote')->nullable();
            $table->decimal('POValue', 20)->default(0);
            $table->mediumText('ServiceLevelNotes')->nullable();
            $table->smallInteger('Frequency')->default(0);
            $table->smallInteger('WRDueDate')->default(0);
            $table->smallInteger('WeeklyDueDay')->default(0);
            $table->mediumText('SelectedSitesNote')->nullable();
            $table->mediumText('WREmail')->nullable();
            $table->boolean('PortalUpload')->default(false);
            $table->boolean('WarnOnWeight')->default(false);
            $table->integer('IncreaseType')->default(1);
            $table->decimal('TransIncrease', 10)->default(0);
            $table->decimal('WLIncrease', 10)->default(0);
            $table->decimal('PerTonIncrease', 10, 0)->default(0);
            $table->decimal('LateTarget', 10)->default(95);
            $table->boolean('ClientInformed')->default(false);
            $table->mediumText('GlobalAlert')->nullable();
            $table->boolean('WRSupplierBreakdown')->default(false);
            $table->boolean('CheckCondition')->default(false);
            $table->mediumText('WastePackNote')->nullable();
            $table->boolean('EmailSkip')->default(false);
            $table->boolean('EmailSun')->default(false);
            $table->boolean('HoldBack')->default(false);
            $table->boolean('SigReq')->default(false);
            $table->boolean('CountdownPO')->default(false);
            $table->boolean('WBReq')->default(false);
            $table->decimal('WasteGreen', 6)->default(0);
            $table->decimal('WasteOrange', 6)->default(0);
            $table->decimal('WasteRed', 6)->default(0);
            $table->smallInteger('RRTarget')->default(0);
            $table->boolean('PortalInvoices')->default(false);
            $table->tinyInteger('LetterType')->default(0);
            $table->boolean('BypassAutoOnStop')->default(false);
            $table->boolean('SmartwasteRequired')->default(false);
            $table->text('SmartwasteNote')->nullable();
            $table->boolean('WTNNotify')->default(false);
            $table->string('NewPortalTransferDate', 45)->nullable();
            $table->boolean('ChargeOnRemoval')->nullable()->default(false);
            $table->string('SamplePo', 50)->nullable();
            $table->integer('AccmgrAuth')->nullable();
            $table->integer('enable_pcard')->default(0);
            $table->boolean('wtn_compliance_check')->nullable()->default(false);
            $table->longText('price_increase_ignored_sites')->nullable();
            $table->longText('price_increase_ignored_containers')->nullable();
            $table->dateTime('price_increase_from_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblclientoptions');
    }
}
