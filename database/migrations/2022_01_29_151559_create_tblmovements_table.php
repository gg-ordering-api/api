<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblmovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblmovements', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('ClientID')->index('ClientID');
            $table->unsignedBigInteger('SiteID')->index('SiteID');
            $table->integer('GroupID')->default(0)->index('GroupID');
            $table->integer('LinkedTicket')->nullable()->default(0);
            $table->string('PONumber', 50)->nullable();
            $table->smallInteger('MovementType')->default(0);
            $table->decimal('WLHours', 6)->default(0);
            $table->bigInteger('Supplier')->default(0)->index('Supplier');
            $table->string('OrderedBy', 200)->nullable();
            $table->mediumText('SupplierNotes')->nullable();
            $table->string('EtaNotes')->nullable();
            $table->dateTime('DateRequired')->nullable()->default('0000-00-00 00:00:00');
            $table->decimal('SuppTransCharge', 10)->default(0);
            $table->decimal('TransMarkup', 10)->default(0);
            $table->decimal('TransClientCharge', 10)->default(0);
            $table->decimal('SuppWeightRate', 10)->default(0);
            $table->decimal('WeightMarkup', 10)->default(0);
            $table->decimal('ClientWeightCharge', 10)->default(0);
            $table->decimal('SupplierIncMinTonnes', 10)->default(0);
            $table->decimal('ClientIncMinTonnes', 10)->default(0);
            $table->decimal('TonnesClientCharge', 10)->default(0);
            $table->smallInteger('TonneageType')->default(0);
            $table->decimal('SuppWLRate', 10)->default(0);
            $table->decimal('WLMarkup', 10)->default(0);
            $table->decimal('WLClientRate', 10)->default(0);
            $table->integer('CreatedBy')->default(0)->index('CreatedBy');
            $table->timestamp('CreatedOn')->useCurrent()->index('tblmovements__indexCreatedOn');
            $table->smallInteger('TicketStatus')->default(1)->index('TicketStatus');
            $table->boolean('IsInvoiced')->default(false);
            $table->bigInteger('GGOrderNumber')->default(0)->index('GGOrderNumber');
            $table->boolean('OrderSubmitted')->default(false);
            $table->unsignedBigInteger('ParentTicket')->default('0');
            $table->decimal('SuppWLHours', 5)->default(0);
            $table->decimal('ClientWLHours', 5)->default(0);
            $table->integer('WasteDestination')->default(0)->index('WasteDestination');
            $table->decimal('TicketWeight', 10, 4)->default(0);
            $table->string('VHCReg', 10)->nullable();
            $table->string('SupplierContact', 200)->nullable();
            $table->mediumText('ComplianceNotes')->nullable();
            $table->integer('WeightAddedBy')->default(0);
            $table->integer('RateSetBy')->default(0);
            $table->boolean('IsCompleted')->default(false);
            $table->dateTime('DateWeightAdded')->nullable()->default('0000-00-00 00:00:00')->index('tblmovements_DateWeightAdded_index');
            $table->decimal('RebateRate', 10)->default(0);
            $table->string('InvoiceNumber', 50)->nullable()->index('InvoiceNumber');
            $table->boolean('IsPaid')->default(false);
            $table->dateTime('InvoiceDate')->nullable()->default('0000-00-00 00:00:00');
            $table->decimal('ActualRebate', 20)->default(0);
            $table->decimal('RebateToClient', 10)->default(0);
            $table->string('SupplierWeightContact', 200)->nullable();
            $table->integer('VHCAddedBy')->default(0);
            $table->string('OldTicketNumber', 20)->nullable();
            $table->bigInteger('BelongsToWR')->default(0)->index('BelongsToWR');
            $table->boolean('OkInvoice')->default(false);
            $table->dateTime('DeletionDate')->nullable()->default('0000-00-00 00:00:00')->index('tblmovements__indexDeletionDate');
            $table->integer('DeletedBy')->default(0)->index('tblmovements_DeletedBy_index');
            $table->string('SupplierInvNo', 100)->nullable()->index('SupplierInvNo');
            $table->string('SuppTicketNo', 100)->nullable();
            $table->boolean('SuppIsDisputed')->default(false)->index('m_disputed');
            $table->dateTime('SuppDateDisputed')->nullable()->default('0000-00-00 00:00:00')->index('tblmovements__indexSuppDateDisputed');
            $table->integer('SuppDisputedBy')->default(0)->index('SuppDisputedBy');
            $table->dateTime('SuppDateDisputeCleared')->nullable()->default('0000-00-00 00:00:00')->index('tblmovements__indexSuppDateDisputeCleared');
            $table->integer('SuppDisputeClearedBy')->default(0);
            $table->boolean('SuppIsPaid')->default(false);
            $table->dateTime('SuppPaidOn')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SuppPaidBy')->default(0);
            $table->mediumText('TicketApproveReason')->nullable();
            $table->boolean('ComplianceCopyTicket')->default(false);
            $table->integer('SuppInvCheckedBy')->default(0);
            $table->dateTime('SuppCheckDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('SupplierOrderConfirmedBy')->nullable();
            $table->dateTime('OrderConfirmationDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('OrderConfirmedUser')->default(0)->index('tblmovements_OrderConfirmedUser_index');
            $table->boolean('TicketReceived')->default(false);
            $table->mediumText('DisputeReason')->nullable();
            $table->smallInteger('DisputeType')->default(0);
            $table->mediumText('DisputeClearReason')->nullable();
            $table->boolean('ClientIsDisputed')->default(false)->index('m_clientdisputed');
            $table->dateTime('ClientDateDisputed')->nullable()->default('0000-00-00 00:00:00')->index('tblmovements__indexClientDateDisputed');
            $table->integer('ClientDisputedBy')->default(0)->index('ClientDisputedBy');
            $table->mediumText('ClientDisputeReason')->nullable();
            $table->smallInteger('ClientDisputeType')->default(0);
            $table->dateTime('ClientDateDisputeCleared')->nullable()->default('0000-00-00 00:00:00')->index('tblmovements__indexClientDateDisputeCleared');
            $table->integer('ClientDisputeClearedBy')->default(0)->index('ClientDisputeClearedBy');
            $table->mediumText('ClientDisputeClearReason')->nullable();
            $table->decimal('CarbonUsageKGKM', 20, 4)->default(0);
            $table->decimal('CarbonUsageKGL', 20, 4)->default(0);
            $table->smallInteger('SuppTonneageType')->default(0);
            $table->integer('TicketApprovedBy')->default(0)->index('TicketApprovedBy');
            $table->dateTime('TicketApprovedDate')->nullable()->default('0000-00-00 00:00:00')->index('tblmovements__indexTicketApprovedDate');
            $table->boolean('IsLate')->default(false);
            $table->boolean('RoRoProcessed')->default(false);
            $table->integer('LeadOwnerBDM')->default(0);
            $table->dateTime('LastComplianceUpdate')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('IsDummy')->nullable()->default(0);
            $table->mediumText('DisputerName')->nullable();
            $table->mediumText('DisputerContact')->nullable();
            $table->mediumText('ClientDisputerName')->nullable();
            $table->mediumText('ClientDisputerContact')->nullable();
            $table->boolean('MarginsProcessed')->default(false);
            $table->integer('OrigCreator')->default(0);
            $table->boolean('AccountsSent')->default(false);
            $table->boolean('NoRebate')->default(false);
            $table->boolean('WTNCreated')->default(false);
            $table->decimal('AccountsSupplierCharge', 10)->default(0);
            $table->decimal('SuppDisputeAdjust', 7)->default(0);
            $table->string('PONotes', 300)->nullable();
            $table->boolean('AccountsTicketsReceived')->default(false);
            $table->boolean('DoNotInvoice')->default(false)->index('tblmovements_DoNotInvoice_index');
            $table->integer('InvBlockedBy')->default(0);
            $table->boolean('WasteReturnCompleted')->default(false);
            $table->dateTime('WasteReturnCompletionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->string('DriverName', 50)->nullable();
            $table->integer('MarkedLateBy')->default(0);
            $table->boolean('HazProcessed')->default(false);
            $table->boolean('PermitConfirmed')->default(false);
            $table->string('PermitConfirmedWith', 50)->nullable();
            $table->integer('PermitConfirmedBy')->default(0);
            $table->dateTime('PermitConfirmedOn')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('PermitNotes')->nullable();
            $table->boolean('WTNProblemTicket')->default(false);
            $table->mediumText('note')->nullable();
            $table->boolean('SmartwasteProcessed')->default(false);
            $table->boolean('SmartwasteComplete')->default(false);
            $table->boolean('is24Late')->default(false);
            $table->tinyInteger('isamendedticket')->default(0);
            $table->tinyInteger('isAmendedSupplierWeight')->nullable()->default(0);
            $table->boolean('LastMovement')->nullable()->default(false);
            $table->string('dispute_extra', 240)->nullable();
            $table->tinyInteger('haz_sent')->nullable()->default(0);
            $table->integer('haz_sent_user_id')->nullable();
            $table->timestamp('haz_sent_date')->nullable();
            $table->string('payment_type', 50)->default('OnAccount');
            $table->string('supplier_transfer_ticket_number', 50)->nullable();
            $table->string('supplier_weighbridge_ticket_number')->nullable();
            $table->boolean('has_tolerance')->nullable()->default(false);
            $table->float('tolerance_value', 10)->nullable();
            $table->unsignedInteger('inv_no_copy')->nullable();
            $table->integer('sent_to_adflex')->nullable()->default(0);
            $table->integer('request_weights')->nullable()->default(1);
            $table->integer('ignore')->nullable()->default(0);
            $table->integer('ignored_by')->nullable();
            $table->dateTime('ignored_on')->nullable();

            $table->index(['SuppDateDisputed', 'SuppDateDisputeCleared'], 'StaffActivityReportSuppDispute');
            $table->index(['DateRequired', 'TicketStatus', 'IsCompleted'], 'tblmovements_DateRequired_TicketStatus_IsCompleted_index');
            $table->index(['WeightAddedBy', 'DateWeightAdded'], 'tblmovements_WeightAddedBy_DateWeightAdded_index');
            $table->index(['TicketStatus', 'DateRequired', 'MarginsProcessed'], 'tblmovements_TicketStatus_DateRequired_MarginsProcessed_index');
            $table->index(['WeightAddedBy', 'DateWeightAdded'], 'StaffActivityReport');
            $table->index(['OrderConfirmedUser', 'OrderConfirmationDate', 'TicketStatus', 'ClientID'], 'OrderConfirmedUser');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblmovements');
    }
}
