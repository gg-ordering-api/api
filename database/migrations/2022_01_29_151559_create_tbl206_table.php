<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbl206Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl206', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SiteID')->index('SiteID');
            $table->integer('WasteType')->default(0);
            $table->string('CDE', 1)->nullable();
            $table->string('WeekNumbers', 50)->nullable();
            $table->string('QuantTonnes', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl206');
    }
}
