<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrimaryWasteTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('primary_waste_types', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('name')->nullable()->unique('primary_waste_types_name_uindex');
            $table->string('shortName', 100)->nullable();
            $table->float('recycledPercent', 10, 0)->nullable();
            $table->float('recoveredPercent', 10, 0)->nullable();
            $table->integer('status')->nullable()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('primary_waste_types');
    }
}
