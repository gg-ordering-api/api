<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsuppliercontactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsuppliercontacts', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('FirstName', 100)->nullable();
            $table->string('Surname', 100)->nullable();
            $table->string('EmailAddress', 100)->nullable();
            $table->string('Telephone', 20)->nullable();
            $table->string('Fax', 20)->nullable();
            $table->string('Mobile', 20)->nullable();
            $table->string('JobTitle', 50)->nullable();
            $table->bigInteger('Company')->default(0)->index('Company');
            $table->timestamp('DateCreated')->useCurrent();
            $table->dateTime('LastUpdated')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('IsDeleted')->default(false);
            $table->boolean('PrefAccountsContact')->default(false);
            $table->boolean('PrefSalesContact')->default(false);
            $table->boolean('PrefComplianceContact')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsuppliercontacts');
    }
}
