<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('note', function (Blueprint $table) {
            $table->foreign(['note_type_id'], 'note_note_type_id_fk')->references(['id'])->on('note_type');
            $table->foreign(['deleted_by'], 'note_tblusers_ID_fk_2')->references(['ID'])->on('tblusers');
            $table->foreign(['go_green_order_number_id'], 'note_gogreen_order_number_id_fk')->references(['id'])->on('gogreen_order_number');
            $table->foreign(['created_by'], 'note_tblusers_ID_fk')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('note', function (Blueprint $table) {
            $table->dropForeign('note_note_type_id_fk');
            $table->dropForeign('note_tblusers_ID_fk_2');
            $table->dropForeign('note_gogreen_order_number_id_fk');
            $table->dropForeign('note_tblusers_ID_fk');
        });
    }
}
