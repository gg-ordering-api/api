<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToWasteOnlineOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('waste_online_order', function (Blueprint $table) {
            $table->foreign(['client_id'], 'waste_online_order_tblclients_ID_fk')->references(['ID'])->on('tblclients');
            $table->foreign(['site_id'], 'waste_online_order_tblsites_ID_fk')->references(['ID'])->on('tblsites');
            $table->foreign(['transfer_station_id'], 'waste_online_order_tbltransferstations_ID_fk')->references(['ID'])->on('tbltransferstations');
            $table->foreign(['container_id'], 'waste_online_order_tblcontainers_ID_fk')->references(['ID'])->on('tblcontainers');
            $table->foreign(['supplier_id'], 'waste_online_order_tblsuppliers_ID_fk')->references(['ID'])->on('tblsuppliers');
            $table->foreign(['waste_type_id'], 'waste_online_order_tblwastetypes_ID_fk')->references(['ID'])->on('tblwastetypes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('waste_online_order', function (Blueprint $table) {
            $table->dropForeign('waste_online_order_tblclients_ID_fk');
            $table->dropForeign('waste_online_order_tblsites_ID_fk');
            $table->dropForeign('waste_online_order_tbltransferstations_ID_fk');
            $table->dropForeign('waste_online_order_tblcontainers_ID_fk');
            $table->dropForeign('waste_online_order_tblsuppliers_ID_fk');
            $table->dropForeign('waste_online_order_tblwastetypes_ID_fk');
        });
    }
}
