<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcrTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocr_ticket', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ocr_migration_log_id')->nullable()->index('ocr_ticket_ocr_migration_log_id_fk');
            $table->integer('gogreen_order_number_id')->nullable()->index('ocr_ticket_gogreen_order_numbera_id_fk');
            $table->integer('status_id')->nullable()->index('ocr_ticket_ocr_ticket_status_id_fk');
            $table->string('order_number')->nullable();
            $table->string('sundry_number')->nullable();
            $table->string('supplier_ticket_number')->nullable();
            $table->decimal('order_total', 10)->nullable();
            $table->decimal('weight', 10)->nullable();
            $table->longText('data')->nullable();
            $table->dateTime('ticket_date')->nullable();
            $table->integer('original_status_id')->nullable()->index('ocr_ticket_ocr_ticket_status_id_fk_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocr_ticket');
    }
}
