<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbl207Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl207', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SiteID')->index('SiteID');
            $table->integer('WasteType')->default(0);
            $table->string('Treatment', 200);
            $table->string('OnOffSite', 10);
            $table->unsignedBigInteger('SupplierID')->default('0');
            $table->mediumText('Projectamendments')->nullable();
            $table->mediumText('Projectreasons')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl207');
    }
}
