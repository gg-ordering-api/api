<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientDebtStatusLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_debt_status_log', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('client_debt_log_id')->nullable()->index('client_debt_status_log_client_debt_log_id_fk');
            $table->integer('client_debt_status_id')->nullable()->index('client_debt_status_log_client_debt_status_id_fk');
            $table->integer('note_id')->nullable()->index('client_on_stop_status_log_note_id_fk');
            $table->boolean('notified')->nullable()->default(false);
            $table->unsignedInteger('created_by')->nullable()->index('client_on_stop_status_log_tblusers_ID_fk');
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->boolean('approved')->nullable()->default(false);
            $table->timestamp('approved_date')->nullable();
            $table->integer('approved_by')->nullable();
            $table->boolean('letter_sent')->nullable()->default(false);
            $table->timestamp('letter_sent_date')->nullable();
            $table->unsignedInteger('letter_sent_by')->nullable()->index('client_debt_status_log_tblusers_ID_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_debt_status_log');
    }
}
