<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOcrRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ocr_rules', function (Blueprint $table) {
            $table->foreign(['ticket_type_id'], 'ocr_rules_ticket_id_fk')->references(['id'])->on('ocr_ticket_type');
            $table->foreign(['cog_rule_id'], 'ocr_rules_cog_rule_fk')->references(['id'])->on('cog_rules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocr_rules', function (Blueprint $table) {
            $table->dropForeign('ocr_rules_ticket_id_fk');
            $table->dropForeign('ocr_rules_cog_rule_fk');
        });
    }
}
