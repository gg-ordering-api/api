<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClientInvoicingConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_invoicing_config', function (Blueprint $table) {
            $table->foreign(['cron_run_id'], 'client_invoicing_config__run_id_fk')->references(['id'])->on('client_invoicing_cron_run_log');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_invoicing_config', function (Blueprint $table) {
            $table->dropForeign('client_invoicing_config__run_id_fk');
        });
    }
}
