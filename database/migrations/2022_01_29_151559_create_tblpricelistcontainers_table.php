<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblpricelistcontainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblpricelistcontainers', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('PLID')->default('0')->index('PLID');
            $table->integer('ContainerType')->default(0);
            $table->integer('WasteType')->default(0);
            $table->decimal('TransportPrice', 20)->default(0);
            $table->decimal('PerTonnePrice', 20)->default(0);
            $table->decimal('IncMinTonnes', 10)->default(0);
            $table->boolean('IsIncIsMin')->default(true);
            $table->decimal('WLPerHour', 10)->default(0);
            $table->decimal('WLIncHours', 10)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpricelistcontainers');
    }
}
