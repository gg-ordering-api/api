<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisputeLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispute_log', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('supplier_id')->nullable();
            $table->string('to', 200)->nullable();
            $table->string('subject', 220)->nullable();
            $table->integer('invoice_id')->nullable();
            $table->dateTime('date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispute_log');
    }
}
