<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientInvoicingConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_invoicing_config', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('cron_active')->nullable()->default(0);
            $table->dateTime('updated_at')->nullable();
            $table->integer('cron_run_id')->nullable()->index('client_invoicing_config__run_id_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_invoicing_config');
    }
}
