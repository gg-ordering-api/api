<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGogreenOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gogreen_options', function (Blueprint $table) {
            $table->foreign(['gogreen_order_id'], 'gogreen_options_gogreen_order_number_id_fk')->references(['id'])->on('gogreen_order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gogreen_options', function (Blueprint $table) {
            $table->dropForeign('gogreen_options_gogreen_order_number_id_fk');
        });
    }
}
