<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblportalfeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblportalfeatures', function (Blueprint $table) {
            $table->integer('ClientID')->default(0)->primary();
            $table->boolean('LeagueTables')->default(false);
            $table->boolean('OnlineOrdering')->default(false);
            $table->boolean('CostReporting')->default(false);
            $table->boolean('SkipMovements')->default(false);
            $table->boolean('BinMovements')->default(false);
            $table->boolean('RecyclingReports')->default(false);
            $table->boolean('GlobalReporting')->default(false);
            $table->boolean('CRMP')->default(false);
            $table->boolean('KPIReporting')->default(false);
            $table->boolean('MixedGate')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblportalfeatures');
    }
}
