<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierStatementDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_statement_dates', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('active')->nullable()->default(1);
            $table->string('name', 50)->unique('dates_lookup_name_uindex');
            $table->dateTime('month_start_at');
            $table->dateTime('month_end_at');
            $table->boolean('email_sent')->nullable()->default(false);
            $table->dateTime('email_sent_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_statement_dates');
    }
}
