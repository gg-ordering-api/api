<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbldcrfoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbldcrfolders', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('FolderName', 30)->nullable();
            $table->bigInteger('ParentFolder')->default(0);
            $table->integer('PrimaryOwner')->default(0);
            $table->string('QMSLocation', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbldcrfolders');
    }
}
