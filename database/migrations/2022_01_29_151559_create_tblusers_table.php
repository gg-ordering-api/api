<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblusers', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('FullName', 100)->nullable();
            $table->string('EmailAddress', 50)->nullable();
            $table->string('JobTitle', 100)->nullable();
            $table->string('UserName', 100)->nullable()->index('tblusers_UserName_index');
            $table->string('password_sid')->nullable();
            $table->integer('UserGroupId')->default(0)->index('UserGroupId');
            $table->integer('DepartmentId')->default(0)->index('DepartmentId');
            $table->tinyInteger('UserStatus')->default(0)->index('UserStatus');
            $table->integer('ReportsTo')->default(0)->index('ReportsTo');
            $table->dateTime('LastLogin')->nullable()->default('0000-00-00 00:00:00');
            $table->string('LayoutConfig', 2000)->nullable();
            $table->string('InternalExtension', 5)->nullable();
            $table->string('DirectDial', 20)->nullable();
            $table->boolean('IsDirector')->default(false);
            $table->boolean('InTraining')->default(false);
            $table->boolean('CanSelfMargin')->default(false);
            $table->boolean('CanSelfApprove')->default(false);
            $table->string('OldPassword', 200)->nullable();
            $table->dateTime('LastResetDate')->nullable()->default('0000-00-00 00:00:00');
            $table->tinyInteger('IsDisputes')->default(0);
            $table->string('EmailPassword', 50)->nullable();
            $table->integer('PinNumber')->nullable();
            $table->boolean('DisplayInPhoneList')->default(true);
            $table->date('StartDate')->default('2017-01-01');
            $table->date('EndDate')->nullable();
            $table->boolean('IsBuyer')->default(false);
            $table->string('ProfilePhoto', 300)->nullable();
            $table->boolean('CanOnStopSites')->default(false);
            $table->boolean('HasLeave')->default(true);
            $table->string('additional_departments')->nullable()->comment('comma separated additional department ids');
            $table->string('ResetKey', 64)->nullable();
            $table->dateTime('ResetRequestTime')->nullable()->default('0000-00-00 00:00:00');
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->dateTime('next_appraisal_date')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('nextAppraisalType');
            $table->string('temp_tel')->nullable();
            $table->boolean('is_hr_hub_manager')->nullable()->default(false);
            $table->string('hr_hub_exclude_users', 250)->nullable();
            $table->integer('hr_hub_training_only')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblusers');
    }
}
