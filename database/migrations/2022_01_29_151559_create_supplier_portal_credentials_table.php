<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierPortalCredentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_portal_credentials', function (Blueprint $table) {
            $table->integer('id', true);
            $table->unsignedInteger('supplier_id')->nullable()->index('supplier_portal_credentials_tblsuppliers_ID_fk');
            $table->string('url')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_portal_credentials');
    }
}
