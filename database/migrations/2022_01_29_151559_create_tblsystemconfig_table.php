<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsystemconfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsystemconfig', function (Blueprint $table) {
            $table->string('GGSIC', 10)->nullable();
            $table->string('GGWTN', 50)->nullable();
            $table->dateTime('GGWTNExpiry')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('GGAddress')->nullable();
            $table->string('GGPostCode', 10)->nullable();
            $table->string('GGSkipPhone', 20)->nullable();
            $table->string('GGBinPhone', 20)->nullable();
            $table->string('GGFax', 20)->nullable();
            $table->boolean('RealtimeFinancials')->default(false);
            $table->dateTime('CutoffDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsystemconfig');
    }
}
