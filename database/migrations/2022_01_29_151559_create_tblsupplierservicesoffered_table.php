<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsupplierservicesofferedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsupplierservicesoffered', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('SupplierID')->default(0)->index('SupplierID');
            $table->integer('ServiceID')->default(0)->index('ServiceID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsupplierservicesoffered');
    }
}
