<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTodonotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('todonotes', function (Blueprint $table) {
            $table->foreign(['todo_id'])->references(['ID'])->on('tbltodolist');
            $table->foreign(['user_id'])->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('todonotes', function (Blueprint $table) {
            $table->dropForeign('todonotes_todo_id_foreign');
            $table->dropForeign('todonotes_user_id_foreign');
        });
    }
}
