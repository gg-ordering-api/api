<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsuppliernotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsuppliernotes', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('NoteTitle', 100)->nullable();
            $table->mediumText('NoteBody')->nullable();
            $table->timestamp('CreatedOn')->useCurrent();
            $table->integer('CreatedBy')->nullable()->index('CreatedBy');
            $table->integer('CID')->nullable()->index('CID');
            $table->boolean('IsDeleted')->default(false);
            $table->smallInteger('NoteType')->default(0);

            $table->index(['CreatedOn', 'IsDeleted', 'CID'], 'StaffActivityReport');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsuppliernotes');
    }
}
