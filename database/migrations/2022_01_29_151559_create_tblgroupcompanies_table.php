<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblgroupcompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblgroupcompanies', function (Blueprint $table) {
            $table->integer('CompanyA')->default(0)->index('CompanyA');
            $table->integer('CompanyB')->default(0)->index('CompanyB');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblgroupcompanies');
    }
}
