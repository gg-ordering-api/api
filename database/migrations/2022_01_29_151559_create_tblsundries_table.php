<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblsundriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblsundries', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedBigInteger('SiteID')->default('0')->index('SiteID');
            $table->string('SundryName', 100)->nullable();
            $table->dateTime('SundryDate')->nullable()->default('0000-00-00 00:00:00')->index('tblsundries__indexSundryDate');
            $table->decimal('ValueAmount', 10)->default(0);
            $table->boolean('CreditDebit')->default(false);
            $table->integer('RaisedBy')->default(0)->index('RaisedBy');
            $table->boolean('IsInvoiced')->default(false);
            $table->dateTime('InvoicedOn')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('IsDeleted')->default(false);
            $table->integer('Deletedby')->default(0);
            $table->decimal('SupplierCharge', 10)->default(0);
            $table->string('InvoiceNumber', 50)->nullable()->index('InvoiceNumber');
            $table->boolean('IsPaid')->default(false);
            $table->integer('UsedSupplier')->default(0)->index('UsedSupplier');
            $table->string('SupplierContact', 200)->nullable();
            $table->boolean('OrderSent')->default(true);
            $table->string('SupplierInvNo', 100)->nullable()->index('SupplierInvNo');
            $table->string('SuppTicketNo', 100)->nullable();
            $table->boolean('SuppIsDisputed')->default(false)->index('s_disputed');
            $table->dateTime('SuppDateDisputed')->nullable()->default('0000-00-00 00:00:00')->index('tblsundries__indexSuppDateDisputed');
            $table->integer('SuppDisputedBy')->default(0)->index('SuppDisputedBy');
            $table->dateTime('SuppDateDisputeCleared')->nullable()->default('0000-00-00 00:00:00')->index('tblsundries__indexSuppDateDisputeCleared');
            $table->integer('SuppDisputeClearedBy')->default(0);
            $table->boolean('SuppIsPaid')->default(false);
            $table->dateTime('SuppPaidOn')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SuppPaidBy')->default(0);
            $table->mediumText('SundryReason')->nullable();
            $table->boolean('IsApproved')->default(false);
            $table->integer('ApprovedBy')->default(0);
            $table->dateTime('ApprovalDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SuppInvCheckedBy')->default(0);
            $table->dateTime('SuppCheckDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('OkInvoice')->default(false);
            $table->string('PONumber', 200)->nullable();
            $table->mediumText('SupplierOrderConfirmedBy')->nullable();
            $table->dateTime('OrderConfirmationDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('OrderConfirmedUser')->default(0);
            $table->boolean('TicketReceived')->default(false);
            $table->mediumText('DisputeReason')->nullable();
            $table->smallInteger('DisputeType')->default(0);
            $table->mediumText('SupplierNotes')->nullable();
            $table->mediumText('DisputeClearReason')->nullable();
            $table->boolean('ClientIsDisputed')->default(false)->index('s_clientdisputed');
            $table->dateTime('ClientDateDisputed')->nullable()->default('0000-00-00 00:00:00')->index('tblsundries__indexClientDateDisputed');
            $table->integer('ClientDisputedBy')->default(0)->index('ClientDisputedBy');
            $table->mediumText('ClientDisputeReason')->nullable();
            $table->smallInteger('ClientDisputeType')->default(0);
            $table->dateTime('ClientDateDisputeCleared')->nullable()->default('0000-00-00 00:00:00')->index('tblsundries__indexClientDateDisputeCleared');
            $table->integer('ClientDisputeClearedBy')->default(0);
            $table->mediumText('ClientDisputeClearReason')->nullable();
            $table->mediumText('ApprovalReason')->nullable();
            $table->boolean('IsSWMPCharge')->default(false);
            $table->integer('LeadOwnerBDM')->default(0);
            $table->integer('TicketRelated')->nullable()->default(0);
            $table->mediumText('DisputerName')->nullable();
            $table->mediumText('DisputerContact')->nullable();
            $table->mediumText('ClientDisputerName')->nullable();
            $table->mediumText('ClientDisputerContact')->nullable();
            $table->timestamp('CreationDate')->useCurrent();
            $table->boolean('CanProvideLights')->default(false);
            $table->boolean('AccountsSent')->default(false);
            $table->boolean('IsPermit')->default(false);
            $table->decimal('SuppDisputeAdjust', 7)->default(0);
            $table->boolean('AccountsTicketsReceived')->default(false);
            $table->boolean('DoNotInvoice')->default(false);
            $table->integer('InvBlockedBy')->default(0);
            $table->boolean('IsRebateCharge')->default(false);
            $table->boolean('RebateIsInvoiced')->default(false);
            $table->integer('RebateInvoiceUser')->default(0);
            $table->dateTime('RebateInvoiceDate')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('RebateInvoiceNumber')->default(0);
            $table->tinyInteger('MetalWasteTransferNotesReceived')->default(0);
            $table->decimal('SundryMargin', 10)->default(0);
            $table->string('dispute_extra', 240)->nullable();
            $table->string('supplier_transfer_ticket_number', 50)->nullable();
            $table->string('supplier_weighbridge_ticket_number')->nullable();
            $table->unsignedInteger('inv_no_copy')->nullable();
            $table->string('payment_type', 50)->default('OnAccount');
            $table->integer('sent_to_adflex')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblsundries');
    }
}
