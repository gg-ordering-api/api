<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblmeetingminutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblmeetingminutes', function (Blueprint $table) {
            $table->integer('ID', true);
            $table->dateTime('MeetingDate')->nullable()->default('0000-00-00 00:00:00');
            $table->text('AttendeesNames')->nullable();
            $table->integer('ClientID')->nullable();
            $table->dateTime('DateCreated')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('DateModified')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('CreatedBy')->default(0);
            $table->integer('ModifiedBy')->nullable()->default(0);
            $table->text('MeetingLocation')->nullable();
            $table->tinyInteger('type')->default(0)->comment('0=client,1=supplier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblmeetingminutes');
    }
}
