<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientInvoicingCronRunLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_invoicing_cron_run_log', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('completed')->default(0);
            $table->dateTime('created_at');
            $table->unsignedInteger('created_by')->index('client_invoicing_cron_run_log_users_fk');
            $table->dateTime('completed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_invoicing_cron_run_log');
    }
}
