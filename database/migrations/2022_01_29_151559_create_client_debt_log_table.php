<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientDebtLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_debt_log', function (Blueprint $table) {
            $table->integer('id', true);
            $table->unsignedInteger('client_id')->nullable()->index('client_on_stop_log_tblclients_ID_fk');
            $table->integer('client_debt_status_id')->nullable()->index('client_debt_log_client_debt_status_id_fk');
            $table->unsignedInteger('created_by')->nullable()->index('client_on_stop_log_tblusers_ID_fk');
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->unsignedInteger('updated_by')->nullable()->index('client_on_stop_log_tblusers_ID_fk_2');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_debt_log');
    }
}
