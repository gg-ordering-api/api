<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSiteDamageDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_damage_document', function (Blueprint $table) {
            $table->foreign(['site_damage_id'], 'site_damage_document_site_damage_id_fk')->references(['id'])->on('site_damage');
            $table->foreign(['documents_id'], 'site_damage_document_documents_id_fk')->references(['id'])->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_damage_document', function (Blueprint $table) {
            $table->dropForeign('site_damage_document_site_damage_id_fk');
            $table->dropForeign('site_damage_document_documents_id_fk');
        });
    }
}
