<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblcipherstaffactivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblcipherstaffactivity', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('ActivityID')->default(0);
            $table->integer('UserID')->default(0);
            $table->dateTime('ActivityStart')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('ActivityEnd')->nullable()->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblcipherstaffactivity');
    }
}
