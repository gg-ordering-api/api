<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblcontactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblcontacts', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('FirstName', 100)->nullable();
            $table->string('Surname', 100)->nullable();
            $table->string('EmailAddress', 100)->nullable();
            $table->string('Telephone', 20)->nullable();
            $table->string('Fax', 20)->nullable();
            $table->string('Mobile', 20)->nullable();
            $table->string('JobTitle', 50)->nullable();
            $table->bigInteger('Company')->default(0)->index('Company');
            $table->timestamp('DateCreated')->useCurrent();
            $table->dateTime('LastUpdated')->nullable()->default('0000-00-00 00:00:00');
            $table->bigInteger('ReportsTo')->default(0);
            $table->boolean('DecisionMaker')->default(false);
            $table->boolean('KeyContact')->default(false);
            $table->boolean('IsDeleted')->default(false);
            $table->string('OfficeLocation', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblcontacts');
    }
}
