<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblpartialpaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblpartialpayment', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('InvoiceNumber')->default(0);
            $table->integer('ClientID')->default(0);
            $table->timestamp('DatePaid')->useCurrent();
            $table->decimal('AmountPaid', 20);
            $table->integer('UserPaidBy')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpartialpayment');
    }
}
