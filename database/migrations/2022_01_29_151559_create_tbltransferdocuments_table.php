<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbltransferdocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbltransferdocuments', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('DocName', 500)->nullable();
            $table->string('FileName', 500)->nullable();
            $table->string('Extension', 5)->nullable();
            $table->string('FileType', 10000)->nullable();
            $table->timestamp('DateAdded')->useCurrent();
            $table->unsignedBigInteger('AddedBy')->default('0');
            $table->bigInteger('FacilityID')->default(0)->index('FacilityID');
            $table->dateTime('ExpiryDate')->default('0000-00-00 00:00:00');
            $table->boolean('IsArchived')->default(false);
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'failed', 'yes'])->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbltransferdocuments');
    }
}
