<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblclientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblclients', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('ShortCode', 10)->nullable()->index('ShortCode');
            $table->string('ClientName', 500)->nullable()->index('ClientName');
            $table->string('Address', 500)->nullable();
            $table->string('PostCode', 10)->nullable();
            $table->string('Telephone', 50)->nullable();
            $table->string('Fax', 50)->nullable();
            $table->integer('ClientStatus')->default(0)->index('ClientStatus');
            $table->integer('PrimaryContact')->default(0);
            $table->integer('CurrentOwner')->default(0);
            $table->timestamp('DateCreated')->useCurrent();
            $table->dateTime('DateUpdated')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('SourceSalesPerson')->default(0)->index('SourceSalesPerson');
            $table->mediumText('BusinessType')->nullable();
            $table->mediumText('BusinessDescription')->nullable();
            $table->string('SICCodes', 500)->nullable();
            $table->boolean('BillingPeriod')->default(false);
            $table->boolean('OnStop')->default(false);
            $table->smallInteger('ServiceLevel')->default(0)->index('ServiceLevel');
            $table->smallInteger('BillingType')->default(0);
            $table->boolean('EmailInvoices')->default(false);
            $table->string('InvoiceEmail', 200)->nullable();
            $table->dateTime('TradingClientConversionDate')->nullable()->default('0000-00-00 00:00:00');
            $table->string('WebAddress', 200)->nullable();
            $table->string('VATNumber', 18)->nullable();
            $table->mediumText('ImportantNote')->nullable();
            $table->string('AdditionalImportantInfo', 1000)->nullable();
            $table->integer('CreditController')->default(0)->index('CreditController');
            $table->mediumText('CurrentCompetitor')->nullable();
            $table->mediumText('CreditNote')->nullable();
            $table->string('PaymentTerms', 10)->nullable();
            $table->smallInteger('PaymentPerformance')->default(0);
            $table->dateTime('ContractStart')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('ContractEnd')->nullable()->default('0000-00-00 00:00:00');
            $table->smallInteger('BinBillingPeriod')->default(0);
            $table->smallInteger('BinBillingType')->default(0);
            $table->dateTime('DateAssignedToCurrent')->nullable()->default('0000-00-00 00:00:00');
            $table->integer('ClientSize')->default(0);
            $table->integer('BDMUser')->default(0)->index('BDMUser');
            $table->unsignedInteger('LeadSource')->default('0');
            $table->integer('PreviousOwner')->default(0);
            $table->boolean('IsRebateCustomer')->default(false);
            $table->dateTime('RebateDueDate')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('SetInactiveDate')->nullable()->default('0000-00-00 00:00:00');
            $table->mediumText('InvoiceNotes')->nullable();
            $table->tinyInteger('SetPrice')->default(0);
            $table->decimal('AccountCredit', 20)->default(0);
            $table->mediumText('GlobalAccountsNote')->nullable();
            $table->string('BinBillingCode', 10)->default('4000');
            $table->tinyInteger('BlockNewQuote')->default(0);
            $table->tinyInteger('ManagerQuoteApprove')->default(0);
            $table->boolean('ClocsFors')->default(false);
            $table->integer('SkipItemsPerInvoice')->default(0);
            $table->smallInteger('AgreementType')->default(0);
            $table->string('AgreementOther', 200)->nullable();
            $table->string('RecyclingTarget', 100)->nullable();
            $table->integer('ClientBillingCode')->nullable();
            $table->boolean('Retention')->nullable();
            $table->dateTime('ManagerQuoteApproveDate')->nullable()->default('0000-00-00 00:00:00');
            $table->boolean('TradexExport')->default(false);
            $table->bigInteger('PriceList')->default(0);
            $table->decimal('RebatePercent', 5)->default(0);
            $table->boolean('is_highly_important')->default(false);
            $table->boolean('display_supplier_ticket_num')->default(false);
            $table->integer('gbcustomer')->nullable()->default(0);
            $table->text('p_card_token')->nullable();
            $table->integer('wtn_required')->nullable()->default(0);
            $table->integer('wtn_app_db_id')->nullable();
            $table->integer('joint_wtn')->nullable()->default(0)->index();
            $table->boolean('invoice_run_exclusion')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblclients');
    }
}
