<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoGreenTicketClickedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('go_green_ticket_clicked', function (Blueprint $table) {
            $table->integer('id', true)->unique('go_green_ticket_clicked_id_uindex');
            $table->integer('go_green_order_number_id')->nullable()->index('go_green_ticket_clicked_gogreen_order_number_id_fk');
            $table->timestamp('clicked')->nullable();
            $table->unsignedInteger('user_id')->nullable()->index('go_green_ticket_clicked_tblusers_ID_fk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('go_green_ticket_clicked');
    }
}
