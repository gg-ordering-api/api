<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcrDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocr_document', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('transaction_id')->nullable();
            $table->integer('ocr_migration_log_id')->nullable()->index('ocr_document_ocr_migration_log_id_fk');
            $table->string('file_name')->nullable();
            $table->string('file_path')->nullable();
            $table->dateTime('created_at')->nullable()->useCurrent();
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'failed', 'yes'])->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocr_document');
    }
}
