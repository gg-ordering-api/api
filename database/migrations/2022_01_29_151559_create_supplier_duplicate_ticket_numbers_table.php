<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierDuplicateTicketNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_duplicate_ticket_numbers', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('gogreen_order_number_id')->nullable()->index('supplier_duplicate_ticket_numbers_gogreen_order_number_id_fk');
            $table->unsignedInteger('supplier_id')->nullable()->index('supplier_duplicate_ticket_numbers_tblsuppliers_ID_fk');
            $table->enum('status', ['Active', 'Ignored', 'Completed'])->nullable()->default('Active');
            $table->unsignedInteger('created_by')->nullable()->index('supplier_duplicate_ticket_numbers_tblusers_ID_fk');
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->unsignedInteger('updated_by')->nullable()->index('supplier_duplicate_ticket_numbers_tblusers_ID_fk_2');
            $table->timestamp('updated_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_duplicate_ticket_numbers');
    }
}
