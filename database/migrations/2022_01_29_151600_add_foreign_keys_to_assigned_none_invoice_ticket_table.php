<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToAssignedNoneInvoiceTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assigned_none_invoice_ticket', function (Blueprint $table) {
            $table->foreign(['assignee'], 'assigned_none_invoice_ticket_tblusers_ID_fk')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assigned_none_invoice_ticket', function (Blueprint $table) {
            $table->dropForeign('assigned_none_invoice_ticket_tblusers_ID_fk');
        });
    }
}
