<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientDisputeDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_dispute_document', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('invoice_id');
            $table->string('dispute_type', 50)->default('adhoc');
            $table->string('document_name', 181)->nullable();
            $table->string('file_name', 181)->nullable();
            $table->string('extension', 5)->nullable();
            $table->string('file_type', 500)->nullable();
            $table->timestamp('date_added')->useCurrent();
            $table->unsignedInteger('added_by')->default('0');
            $table->integer('document_store_id')->nullable();
            $table->enum('migrated', ['no', 'yes', 'failed'])->nullable()->default('no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_dispute_document');
    }
}
