<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToComplianceCheckListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compliance_check_list', function (Blueprint $table) {
            $table->foreign(['gogreen_order_number_id'], 'compliance_check_list_gogreen_order_number_id_fk')->references(['id'])->on('gogreen_order_number');
            $table->foreign(['updated_by'], 'compliance_check_list_tblusers_ID_fk_2')->references(['ID'])->on('tblusers');
            $table->foreign(['completed_by'], 'compliance_check_list_tblusers_ID_fk_4')->references(['ID'])->on('tblusers');
            $table->foreign(['rejected_reason_id'], 'compliance_check_list_compliance_rejected_reasons_id_fk')->references(['id'])->on('compliance_rejected_reasons');
            $table->foreign(['created_by'], 'compliance_check_list_tblusers_ID_fk')->references(['ID'])->on('tblusers');
            $table->foreign(['assigned'], 'compliance_check_list_tblusers_ID_fk_3')->references(['ID'])->on('tblusers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compliance_check_list', function (Blueprint $table) {
            $table->dropForeign('compliance_check_list_gogreen_order_number_id_fk');
            $table->dropForeign('compliance_check_list_tblusers_ID_fk_2');
            $table->dropForeign('compliance_check_list_tblusers_ID_fk_4');
            $table->dropForeign('compliance_check_list_compliance_rejected_reasons_id_fk');
            $table->dropForeign('compliance_check_list_tblusers_ID_fk');
            $table->dropForeign('compliance_check_list_tblusers_ID_fk_3');
        });
    }
}
