<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientInvoicingLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_invoicing_log', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('gg_order_number_id')->nullable()->index('ticketId');
            $table->string('invoice_id', 50)->nullable()->index('invoiceId');
            $table->integer('cron_run_id')->index('client_invoicing_log_cron_run_id_fk');
            $table->unsignedInteger('client_id')->nullable()->index('client_invoicing_log_client_fk');
            $table->integer('error_id')->nullable()->index('client_invoicing_log_error_fk');
            $table->integer('status_id')->nullable()->default(1)->index('client_invoicing_log_status_fk');
            $table->unsignedInteger('site_id')->nullable()->index('client_invoicing_log_sites_fk');
            $table->integer('type_id')->nullable()->index('client_invoicing_log_type_fk');
            $table->unsignedInteger('invoice_pdf_id')->nullable()->index('client_invoicing_log_invoice_pdf_fk');
            $table->dateTime('created_at');
            $table->dateTime('completed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_invoicing_log');
    }
}
