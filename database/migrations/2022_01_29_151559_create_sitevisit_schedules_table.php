<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitevisitSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitevisit_schedules', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('site_id')->default(0)->index('site_id');
            $table->date('date_scheduled')->nullable();
            $table->integer('last_modified_by');
            $table->dateTime('date_modified')->nullable()->default('0000-00-00 00:00:00');
            $table->dateTime('date_created')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitevisit_schedules');
    }
}
