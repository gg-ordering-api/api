<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOcrDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ocr_document', function (Blueprint $table) {
            $table->foreign(['ocr_migration_log_id'], 'ocr_document_ocr_migration_log_id_fk')->references(['id'])->on('ocr_migration_log');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ocr_document', function (Blueprint $table) {
            $table->dropForeign('ocr_document_ocr_migration_log_id_fk');
        });
    }
}
