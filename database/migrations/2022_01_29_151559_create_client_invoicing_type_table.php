<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientInvoicingTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_invoicing_type', function (Blueprint $table) {
            $table->integer('id', true)->index();
            $table->integer('ocr_order_type_id')->nullable()->index('client_invoicing_type_order_type_fk');
            $table->string('type', 50)->nullable();
            $table->string('method', 100)->nullable();
            $table->integer('active')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_invoicing_type');
    }
}
