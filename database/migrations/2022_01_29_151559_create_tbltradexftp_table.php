<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbltradexftpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbltradexftp', function (Blueprint $table) {
            $table->integer('ClientID')->unique('CilentID');
            $table->string('FTPHost', 100)->nullable();
            $table->string('FTPUser', 100)->nullable();
            $table->string('FTPPass', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbltradexftp');
    }
}
