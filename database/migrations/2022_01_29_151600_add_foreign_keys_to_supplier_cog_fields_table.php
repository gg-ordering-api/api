<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSupplierCogFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier_cog_fields', function (Blueprint $table) {
            $table->foreign(['cog_type_id'], 'supplier_cog_fields_cog_type_id_fk')->references(['id'])->on('cog_type');
            $table->foreign(['supplier_cog_id'], 'supplier_cog_fields_supplier_cog_id_fk')->references(['id'])->on('supplier_cog');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supplier_cog_fields', function (Blueprint $table) {
            $table->dropForeign('supplier_cog_fields_cog_type_id_fk');
            $table->dropForeign('supplier_cog_fields_supplier_cog_id_fk');
        });
    }
}
