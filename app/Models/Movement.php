<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Movement extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblmovements';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected  $primaryKey = 'ID';

    /**
     * Default visible columns
     */
    protected $visible = [
        'ID',
        'EtaNotes',
        'DateRequired',
        'last_movement_date',
        'can_book_exchange',
        'can_book_removal',
        'can_book_redelivery'
    ];

    /**
     * Mapping of column names to nice names.
     *
     * @var array
     */
    public $map = [
        'ID' => 'id',
        'ClientID' => 'client_id',
        'SiteID' => 'site_id',
        'GroupID' => 'group_id',
        'PONumber' => 'po_number',
        'Supplier' => 'supplier_id',
        'EtaNotes' => 'eta_notes',
        'DateRequired' => 'date_required',
    ];

    public function site(): BelongsTo
    {
        return $this->belongsTo(Site::class, 'SiteID', 'ID');
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'ClientID', 'ID');
    }

    public function movementType(): BelongsTo
    {
        return $this->belongsTo(MovementType::class, 'MovementType', 'id');
    }

    public function containerGroup(): BelongsTo
    {
        return $this->belongsTo(ContainerGroup::class, 'GroupID', 'ID');
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class, 'Supplier', 'ID');
    }

    public function orderNumber(): BelongsTo
    {
        return $this->belongsTo(OrderNumber::class, 'ID', 'ticket_id');
    }

    public function parentTicket(): BelongsTo
    {
        return $this->belongsTo(Movement::class, 'ParentTicket', 'ID');
    }

    /**
     * @param int $id
     * @return Collection
     */
    public function getMovementsById(int $id): Collection
    {
        return self::select([
            self::getTableName() . '.ID',
            self::getTableName() . '.SiteID',
            self::getTableName() . '.ClientID',
            self::getTableName() . '.Supplier',
            self::getTableName() . '.MovementType',
            self::getTableName() . '.GroupID',
            self::getTableName() . '.ParentTicket',
            self::getTableName() . '.EtaNotes',
            self::getTableName() . '.DateRequired',
            'lastMovement.DateRequired AS last_movement_date',
            DB::raw('IF(' . self::getTableName() . '.MovementType NOT IN (4,5), 1, 0) AS can_book_exchange'),
            DB::raw('IF(' . self::getTableName() . '.MovementType NOT IN (4,5), 1, 0) AS can_book_removal'),
            DB::raw('IF(' . self::getTableName() . '.MovementType IN (4,5), 1, 0) AS can_book_redelivery')
        ])
            ->with('site', 'client', 'supplier', 'orderNumber', 'movementType', 'containerGroup.container', 'containerGroup.wasteType')
            ->leftJoin(self::getTableName() . ' AS lastMovement', function($join){
                $join->on('lastMovement.ID', '=', DB::raw('(SELECT MAX(m.ID) FROM tblmovements m WHERE m.GroupID = ' . self::getTableName() . '.GroupID)'));
            })
            ->where(self::getTableName() . '.ID', '=', $id)
            ->get();
    }

    /**
     * @return Collection
     */
    public function getMovementsBySiteTest(): Collection
    {
        return self::where(self::getTableName() . '.SiteID', '=', 1)
            ->with('site', 'client', 'supplier', 'orderNumber', 'movementType', 'containerGroup.container', 'containerGroup.wasteType')
            ->get();
    }

    /**
     * @param int $id
     * @param array $params
     * @return Collection
     */
    public function getMovementsBySite(
        int $id,
        array $params
    ): Collection
    {
        return self::select([
            self::getTableName() . '.ID',
            self::getTableName() . '.SiteID',
            self::getTableName() . '.ClientID',
            self::getTableName() . '.Supplier',
            self::getTableName() . '.MovementType',
            self::getTableName() . '.GroupID',
            self::getTableName() . '.ParentTicket',
            self::getTableName() . '.EtaNotes',
            self::getTableName() . '.DateRequired',
            'lastMovement.DateRequired AS last_movement_date',
            DB::raw('IF(' . self::getTableName() . '.MovementType IN (0, 1), 1, 0) AS can_book_exchange'),
            DB::raw('IF(' . self::getTableName() . '.MovementType IN (0, 1), 1, 0) AS can_book_removal'),
            DB::raw('IF(' . self::getTableName() . '.MovementType NOT IN (0, 1), 1, 0) AS can_book_redelivery')
        ])
            ->with('site', 'client', 'supplier', 'orderNumber', 'movementType', 'containerGroup.container', 'containerGroup.wasteType')
            ->leftJoin(self::getTableName() . ' AS exchangeRemoval', function($join){
                $join->on('exchangeRemoval.ID', '=', DB::raw('(SELECT MAX(m.ID) FROM tblmovements m WHERE m.MovementType IN (1, 2) AND m.ParentTicket = ' . self::getTableName() . '.ID)'));
            })
            ->leftJoin(self::getTableName() . ' AS lastMovement', function($join){
                $join->on('lastMovement.ID', '=', DB::raw('(SELECT MAX(m.ID) FROM tblmovements m WHERE m.GroupID = ' . self::getTableName() . '.GroupID)'));
            })
            ->where(self::getTableName() . '.SiteID', '=', $id)
            ->when(isset($params['onsite']) && $params['onsite'] == '1', function ($query) use ($params) {
                $query->where(function($query) {
                    $query->where(function($query) {
                        $query->whereNotNull('exchangeRemoval.ID')
                            ->whereDate('exchangeRemoval.DateRequired', '>=', Carbon::now()->toDateTimeString());
                    });
                    $query->orWhere(function($query) {
                        $query->whereDate(self::getTableName() . '.DateRequired', '<=', Carbon::now()->toDateTimeString())
                            ->whereNull('exchangeRemoval.ID');
                    });
                })
                    ->whereIn(self::getTableName() . '.MovementType', [0, 1]);

            })
            ->when(isset($params['past']) && $params['past'] == '1', function ($query) use ($params) {
                $query->where(function($query) {
                    $query->where(function($query) {
                        $query->whereNotNull('exchangeRemoval.ID')
                            ->whereDate('exchangeRemoval.DateRequired', '<=', Carbon::now()->toDateTimeString())
                            ->where(self::getTableName() .'.DateRequired', '<=', Carbon::now()->toDateTimeString());
                    });
                    $query->orWhere(function($query) {
                        $query->whereDate(self::getTableName() . '.DateRequired', '<=', Carbon::now()->toDateTimeString())
                            ->whereIn(self::getTableName() . '.MovementType', [2, 4, 5]);
                    });
                });
            })
            ->when(isset($params['future']) && $params['future'] == '1', function ($query) use ($params) {
                $query->whereDate(self::getTableName() . '.DateRequired', '>', Carbon::now()->toDateTimeString());
            })
            ->when(isset($params['limit']) && $params['limit'] > 0, function ($query) use ($params) {
                $query->limit($params['limit']);
            })
            ->get();
    }

    /**
     * @param int $id
     * @param array $params
     * @return Collection
     */
    public function getMovementsByClient(
        int $id,
        array $params
    ): Collection
    {
        return self::select([
            self::getTableName() . '.ID',
            self::getTableName() . '.SiteID',
            self::getTableName() . '.Supplier',
            self::getTableName() . '.MovementType',
            self::getTableName() . '.GroupID',
            self::getTableName() . '.ParentTicket',
            self::getTableName() . '.EtaNotes',
            self::getTableName() . '.DateRequired',
            'lastMovement.DateRequired AS last_movement_date',
            DB::raw('IF(' . self::getTableName() . '.MovementType NOT IN (4,5), 1, 0) AS can_book_exchange'),
            DB::raw('IF(' . self::getTableName() . '.MovementType NOT IN (4,5), 1, 0) AS can_book_removal'),
            DB::raw('IF(' . self::getTableName() . '.MovementType IN (4,5), 1, 0) AS can_book_redelivery')
        ])
            ->with('site', 'client', 'supplier', 'orderNumber', 'movementType', 'containerGroup.container', 'containerGroup.wasteType')
            ->leftJoin(self::getTableName() . ' AS exchangeRemoval', function($join){
                $join->on('exchangeRemoval.ID', '=', DB::raw('(SELECT MAX(m.ID) FROM tblmovements m WHERE m.MovementType IN (1, 2) AND m.ParentTicket = ' . self::getTableName() . '.ID)'));
            })
            ->leftJoin(self::getTableName() . ' AS lastMovement', function($join){
                $join->on('lastMovement.ID', '=', DB::raw('(SELECT MAX(m.ID) FROM tblmovements m WHERE m.GroupID = ' . self::getTableName() . '.GroupID)'));
            })
            ->where(self::getTableName() . '.ClientID', '=', $id)
            ->when(isset($params['onsite']) && $params['onsite'] == '1', function ($query) use ($params) {
                $query->where(function($query) {
                    $query->where(function($query) {
                        $query->whereNotNull('exchangeRemoval.ID')
                            ->whereDate('exchangeRemoval.DateRequired', '>=', Carbon::now()->toDateTimeString());
                    });
                    $query->orWhere(function($query) {
                        $query->whereDate(self::getTableName() . '.DateRequired', '<=', Carbon::now()->toDateTimeString())
                            ->whereNull('exchangeRemoval.ID');
                    });
                })
                    ->whereIn(self::getTableName() . '.MovementType', [0, 1]);

            })
            ->when(isset($params['past']) && $params['past'] == '1', function ($query) use ($params) {
                $query->where(function($query) {
                    $query->where(function($query) {
                        $query->whereNotNull('exchangeRemoval.ID')
                            ->whereDate('exchangeRemoval.DateRequired', '<=', Carbon::now()->toDateTimeString())
                            ->where(self::getTableName() .'.DateRequired', '<=', Carbon::now()->toDateTimeString());
                    });
                    $query->orWhere(function($query) {
                        $query->whereDate(self::getTableName() . '.DateRequired', '<=', Carbon::now()->toDateTimeString())
                            ->whereIn(self::getTableName() . '.MovementType', [2, 4, 5]);
                    });
                });
            })
            ->when(isset($params['future']) && $params['future'] == '1', function ($query) use ($params) {
                $query->whereDate(self::getTableName() . '.DateRequired', '>', Carbon::now()->toDateTimeString());
            })
            ->when(isset($params['limit']) && $params['limit'] > 0, function ($query) use ($params) {
                $query->limit($params['limit']);
            })
            ->get();
    }
}
