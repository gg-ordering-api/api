<?php

namespace App\Models;

class APIConfig extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api_config';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $visible = [
        'next_day_delivery_cut_off'
    ];

}
