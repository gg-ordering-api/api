<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ContainerGroup extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblcontainergroups';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    protected $visible = [
        'ID',
        'ContainerType',
        'WasteType',
        'IsHazardous',
        'PONumber',
        'PermitRequired',
    ];

    public $map = [
        'ID' => 'id',
        'ContainerType' => 'type',
        'WasteType' => 'waste_type',
        'IsHazardous' => 'is_hazardous',
        'PONumber' => 'po_number',
        'PermitRequired' => 'permit_required',
    ];

    public function container(): BelongsTo
    {
        return $this->belongsTo(Container::class, 'ContainerType', 'ID');
    }

    public function wasteType(): BelongsTo
    {
        return $this->belongsTo(WasteType::class, 'WasteType', 'ID');
    }

}
