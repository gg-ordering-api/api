<?php

namespace App\Models;

class WasteType extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblwastetypes';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * Default visible columns
     */
    protected $visible = [
        'id',
        'WasteName',
        'WasteCode',
    ];

    /**
     * Mapping of column names to nice names.
     *
     * @var array
     */
    public $map = [
        'id' => 'id',
        'WasteName' => 'name',
        'WasteCode' => 'code'
    ];
}
