<?php

namespace App\Models;

class MovementType extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movement_types';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $visible = [
        'id',
        'type'
    ];

    /**
     * Mapping of column names to nice names.
     *
     * @var array
     */
    public $map = [
        'id' => 'id',
        'type' => 'type'
    ];
}
