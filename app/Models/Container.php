<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Container extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblcontainers';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected  $primaryKey = 'ID';

    protected $visible = [
        'ID',
        'SkipCode',
        'SkipDescription',
        'CubicYards'
    ];

    /**
     * Mapping of column names to nice names.
     *
     * @var array
     */
    public $map = [
        'ID' => 'id',
        'SkipCode' => 'code',
        'SkipDescription' => 'description',
        'CubicYards' => 'cubic_yards'
    ];
}
