<?php

namespace App\Models;

class Site extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblsites';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    protected $visible = [
        'ID',
        'SiteName',
        'SiteAddress',
        'SitePostcode',
        'SiteContact',
        'SiteEmail',
        'SitePO'
    ];

    /**
     * Mapping of column names to nice names.
     *
     * @var array
     */
    public $map = [
        'ID' => 'id',
        'SiteName' => 'name',
        'SiteAddress' => 'address',
        'SitePostcode' => 'postcode',
        'SiteContact' => 'site_contact',
        'SiteEmail' => 'site_email',
        'SitePO' => 'po_number',
    ];
}
