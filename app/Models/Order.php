<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

class Order extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblrequestedmovement';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * Default visible columns
     */
    protected $visible = [
        'ID',
        'MovementType',
        'DateSubmited',
        'RequestedDate',
        'OpsNotes',
        'CompletionDate',
        'Complete',
        'ordered_by',
        'linked_ticket_id',
    ];

    /**
     * Mapping of column names to nice names.
     *
     * @var array
     */
    public $map = [
        'ID' => 'id',
        'DateSubmited' => 'submited_date',
        'RequestedDate' => 'required_date',
        'OpsNotes' => 'ops_notes',
        'CompletionDate' => 'completion_date',
        'Complete' => 'complete',
    ];

    public function site(): BelongsTo
    {
        return $this->belongsTo(Site::class, 'SiteID', 'ID');
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'ClientID', 'ID');
    }

    public function orderedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'ordered_by', 'id');
    }

    public function linkedTicket(): BelongsTo
    {
        return $this->belongsTo(Movement::class, 'linked_ticket_id', 'ID');
    }

    /**
     * @param int $id
     * @return Collection
     */
    public function getOrderById(int $id): Collection
    {
        return self::where('ID', '=', $id)
            ->with('site', 'client', 'orderedBy', 'linkedTicket', 'linkedTicket.supplier', 'linkedTicket.containerGroup', 'linkedTicket.containerGroup.container', 'linkedTicket.containerGroup.wasteType')
            ->get();
    }

    /**
     * @param int $id
     * @param array $params
     * @return Collection
     */
    public function getOrdersBySite(
        int $id,
        array $params
    ): Collection
    {
        return self::where('SiteID', '=', $id)
            ->with('site', 'client', 'orderedBy', 'linkedTicket', 'linkedTicket.supplier', 'linkedTicket.containerGroup', 'linkedTicket.containerGroup.container', 'linkedTicket.containerGroup.wasteType')
            ->when(isset($params['complete']), function ($query) use ($params) {
                $query->where(self::getTableName() . '.Complete', '=', $params['complete']);
            })
            ->when(isset($params['client_id']) && $params['client_id'] > 0, function ($query) use ($params) {
                $query->where(self::getTableName() . '.ClientID', '=', $params['client_id']);
            })
            ->when(isset($params['limit']) && $params['limit'] > 0, function ($query) use ($params) {
                $query->limit($params['limit']);
            })
            ->get();
    }

    /**
     * @param int $id
     * @param array $params
     * @return Collection
     */
    public function getOrdersByClient(
        int $id,
        array $params
    ): Collection
    {
        return self::where('ClientID', '=', $id)
            ->with('site', 'client', 'orderedBy', 'linkedTicket', 'linkedTicket.supplier', 'linkedTicket.containerGroup', 'linkedTicket.containerGroup.container', 'linkedTicket.containerGroup.wasteType')
            ->when(isset($params['complete']), function ($query) use ($params) {
                $query->where(self::getTableName() . '.Complete', '=', $params['complete']);
            })
            ->when(isset($params['site_id']) && $params['site_id'] > 0, function ($query) use ($params) {
                $query->where(self::getTableName() . '.SiteID', '=', $params['site_id']);
            })
            ->when(isset($params['limit']) && $params['limit'] > 0, function ($query) use ($params) {
                $query->limit($params['limit']);
            })
            ->get();
    }

}
