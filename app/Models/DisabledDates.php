<?php

namespace App\Models;

class DisabledDates extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api_disabled_dates';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected $visible = [
        'date',
        'time',
    ];

}
