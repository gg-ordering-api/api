<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;

class Client extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblclients';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected  $primaryKey = 'ID';

    protected $visible = [
        'ID',
        'ClientName',
        'ShortCode',
        'Address',
        'PostCode',
        'Telephone',
        'ClientStatus',
    ];

    /**
     * Mapping of column names to nice names.
     *
     * @var array
     */
    public $map = [
        'ID' => 'id',
        'ClientName' => 'name',
        'ShortCode' => 'short_code',
        'Address' => 'address',
        'PostCode' => 'postcode',
        'Telephone' => 'telephone',
        'ClientStatus' => 'status',
    ];
}
