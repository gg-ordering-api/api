<?php

namespace App\Models;

class OrderNumber extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gogreen_order_number';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Default visible columns
     */
    protected $visible = [
        'id',
        'ticket_id',
        'ocr_order_type',
        'order_number'
    ];

    /**
     * Mapping of column names to nice names.
     *
     * @var array
     */
    public $map = [
        'id' => 'id',
        'ticket_id' => 'ticket_id',
        'ocr_order_type' => 'order_type',
        'order_number' => 'order_number',
    ];
}
