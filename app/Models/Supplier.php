<?php

namespace App\Models;

class Supplier extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblsuppliers';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected  $primaryKey = 'ID';

    /**
     * Default visible columns
     */
    protected $visible = [
        'ID',
        'SupplierName',
    ];

    /**
     * Mapping of column names to nice names.
     *
     * @var array
     */
    public $map = [
        'ID' => 'id',
        'SupplierName' => 'name',
    ];
}
