<?php

namespace App\Models;

use App\Traits\GetTableName;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, Eloquence, Mappable, GetTableName;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api_users';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * Default visible columns
     */
    const columnArray = [
        'user_id',
        'user_email',
        'user_name',
        'user_client_id'
    ];

    /**
     * Mapping of column names to nice names.
     *
     * @var array
     */
    protected $maps = [
        'user_id' => 'id',
        'user_email' => 'email',
        'user_name' => 'name',
        'user_client_id' => 'client_id'
    ];

    /**
     * The attributes that should be visible in return array.
     *
     * @var array
     */
    protected $visible = self::columnArray;

    /**
     * Appends mapped data to the return array
     *
     * @var array
     */
    protected $appends = self::columnArray;
}
