<?php

namespace App\Models;

use App\Traits\GetTableName;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class BaseModel extends Model
{
    use HasApiTokens, HasFactory, Notifiable, GetTableName;

    /**
     *  Prevent these fields from being assigned
     *
     * @var string[]
     */
    protected $guarded = ['ID','id'];

    /**
     * Allows retrieval of table name via static call
     *
     * @return mixed
     */
    public static function getTableName()
    {
        return with(new static)->getTable();
    }
}
