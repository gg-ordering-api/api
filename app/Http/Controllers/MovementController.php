<?php

namespace App\Http\Controllers;

use App\Models\Movement;
use Illuminate\Http\Request;

class MovementController extends Controller
{

    /**
     * Get Movement Data
     *
     * Returns Movement data array
     *
     * Otherwise returns unauthenticated or error array
     *
     * @group Movements
     * @urlParam id int required Movement ID.
     *
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return self::format((new Movement())->getMovementsById($id));
    }

    /**
     * Get Movement By Site
     *
     * Returns an array of Movements on a specific site
     *
     * Otherwise returns unauthenticated or error array
     *
     * @group Movements
     * @urlParam id int required Site ID.
     * @queryParam onsite boolean OnSite (1 or 0). Leave out to return all.
     * @queryParam future boolean Future (1 or 0). Leave out to return all.
     * @queryParam past boolean Past (1 or 0). Leave out to return all.
     * @queryParam limit int Past. Leave out to return all.
     *
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function getBySiteId(
        int $id,
        Request $request
    )
    {
        $validator = $request->validate([
            'onsite' => 'sometimes|boolean|nullable',
            'future' => 'sometimes|boolean|nullable',
            'past' => 'sometimes|boolean|nullable',
            'limit' => 'sometimes|int|nullable'
        ]);

        return self::format((new Movement)->getMovementsBySite($id, $validator));
    }

    /**
     * Get Movement By Client
     *
     * Returns an array of Movements on a specific client
     *
     * Otherwise returns unauthenticated or error array
     *
     * @group Movements
     * @urlParam id int required Client ID.
     * @queryParam onsite boolean OnSite (1 or 0). Leave out to return all.
     * @queryParam future boolean Future (1 or 0). Leave out to return all.
     * @queryParam past boolean Past (1 or 0). Leave out to return all.
     * @queryParam limit int Past. Leave out to return all.
     *
     * @param int $id
     * @param Request $request
     * @return mixed
     */
    public function getByClientId(
        int $id,
        Request $request
    )
    {
        $validator = $request->validate([
            'onsite' => 'sometimes|boolean|nullable',
            'onsite' => 'sometimes|boolean|nullable',
            'future' => 'sometimes|boolean|nullable',
            'past' => 'sometimes|boolean|nullable',
            'limit' => 'sometimes|int|nullable'
        ]);

        return self::format((new Movement)->getMovementsByClient($id, $validator));
    }

    /**
     * Get Movement for specific Site categorised by State (On Site, Future or Past)
     *
     * Returns an array of Movements on a specific site
     *
     * Otherwise returns unauthenticated or error array
     *
     * @group Movements
     * @urlParam id int Site ID. No-example
     *
     * @param int $id
     * @return mixed
     */
    public function getMovementsBySiteCategoriseByState(int $id): array
    {
        $onSite = self::format((new Movement)->getMovementsBySite($id, ['onsite' => '1']));
        $future = self::format((new Movement)->getMovementsBySite($id, ['future' => '1']));
        $past = self::format((new Movement)->getMovementsBySite($id, ['past' => '1', 'limit' => '10']));

        return [
            'onsite' => $onSite,
            'future' => $future,
            'past' => $past,
        ];
    }

    public function getBySiteIdTest()
    {
        return self::format((new Movement)->getMovementsBySiteTest());
    }

}
