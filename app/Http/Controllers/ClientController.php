<?php

namespace App\Http\Controllers;

use App\Models\Client;

class ClientController extends Controller
{

    /**
     * Get Client Data
     *
     * Returns Client data array
     *
     * Otherwise returns unauthenticated or error array
     *
     * @group Client
     * @urlParam id int required Client ID.
     *
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return self::format(Client::where('ID', $id)->get());
    }

}
