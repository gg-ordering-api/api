<?php

namespace App\Http\Controllers;

use App\Models\Site;

class SiteController extends Controller
{

    /**
     * Get Site Data
     *
     * Returns Site data array
     *
     * Otherwise returns unauthenticated or error array
     *
     * @group Sites
     * @urlParam id int required Site ID.
     *
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return self::format(Site::where('ID', $id)->get());
    }

    /**
     * Get Sites by Client
     *
     * Returns an array of Site for a specific client
     *
     * Otherwise returns unauthenticated or error array
     *
     * @group Sites
     * @urlParam id int required Client ID.
     *
     * @param int $id
     * @return mixed
     */
    public function getByClientId(int $id)
    {
        return self::format(Site::where('CompanyID', $id)->get());
    }

}
