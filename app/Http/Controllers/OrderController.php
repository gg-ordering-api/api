<?php

namespace App\Http\Controllers;

use App\Models\Movement;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    /**
     * Get Order BY Id
     *
     * Returns Order data array
     * Otherwise returns unauthenticated or blank page
     *
     * @group Orders
     * @urlParam id int required Order ID
     *
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return self::format((new Order)->getOrderById($id));
    }

    /**
     * Get Order By Site
     *
     * Returns an array of Orders on a specific site
     * Otherwise returns unauthenticated or error array
     *
     * @group Orders
     * @urlParam id int required Site ID
     * @queryParam complete boolean Complete (1 or 0). Leave out to return all
     * @queryParam client_id Client ID. No-example
     * @queryParam limit int Limit. Leave out to return all
     *
     * @param int $id
     * @param Request $request
     * @return mixed
     */
    public function getBySiteId(
        int $id,
        Request $request
    )
    {
        $validator = $request->validate([
            'complete' => 'sometimes|boolean|nullable',
            'client_id' => 'sometimes|int|nullable|nullable',
            'limit' => 'sometimes|int|nullable'
        ]);

        return self::format((new Order)->getOrdersBySite($id, $validator));
    }

    /**
     * Get Order By Client
     *
     * Returns an array of Orders on a specific client
     * Otherwise returns unauthenticated or error array
     *
     * @group Orders
     * @urlParam id int required Client ID
     * @queryParam complete boolean Complete (1 or 0). Leave out to return all
     * @queryParam site_id int Site ID. No-example
     * @queryParam limit int Limit. Leave out to return all
     *
     * @param int $id
     * @param Request $request
     * @return mixed
     */
    public function getByClientId(
        int $id,
        Request $request
    )
    {

        $validator = $request->validate([
            'complete' => 'sometimes|boolean|nullable',
            'site_id' => 'sometimes|int|nullable',
            'limit' => 'sometimes|int|nullable'
        ]);

        return self::format((new Order)->getOrdersByClient($id, $validator));
    }

    /**
     * Create Order
     *
     * Returns new order details
     *
     * Otherwise returns unauthenticated or error array
     *
     * @bodyParam linked_ticket_id int required Complete (1 or 0). Example: 1
     * @bodyParam movement_type string required Movement Type. (Delivery, Exchange, Removal, Tip and Return, Wait and Load, Amendment or ETA Request). Example: Delivery
     * @bodyParam date_required datetime required Date. Date and time the skip is required for. No-example
     * @bodyParam site_contact string required Site Contact. Name of site contact. No-example
     * @bodyParam notes text Notes. User notes. No-example
     * @bodyParam ordered_by string required Ordered By. ID of person ordering. Example: 1
     *
     * @group Orders
     *
     * @param Request $request
     * @return mixed
     */
    public function createOrder(Request $request): array
    {
        $validator = $request->validate([
            'linked_ticket_id' => 'required|int',
            'movement_type' => 'required|string',
            'date_required' => 'required|date_format:Y-m-d H:i:s',
            'site_contact' => 'required|string',
            'notes' => 'sometimes|nullable',
            'ordered_by' => 'required|int',
        ]);

        $parentMovement = self::format((new Movement)->getMovementsById($validator['linked_ticket_id']))[0];

        $notes = "Type: ".$validator['movement_type']."\n"
        . "Parent Ticket ID: ".$validator['linked_ticket_id']."\n"
        . "PO Number: ".$parentMovement['site']['po_number'].$parentMovement['container_group']['po_number']."\n"
        . "Ordered By: ".$validator['ordered_by']."\n"
        . "Site Contact: ".$validator['site_contact']."\n"
        . "Extra Notes: " . (isset($validator['notes']) ? $validator['notes'] : '');

        $createArray = [
            'SiteID' => $parentMovement['site']['id'],
            'ClientID' => $parentMovement['client']['id'],
            'MovementType' => $validator['movement_type'],
            'RequestedDate' => $validator['date_required'],
            'OpsNotes' => $notes,
            'SentFrom' => '0',
            'Source' => '2',
            'linked_ticket_id' => $validator['linked_ticket_id'],
            'ordered_by' => $validator['ordered_by'],
        ];

        $insert = Order::insertGetId($createArray);

        return ['success' => $insert ?? false];
    }

}
