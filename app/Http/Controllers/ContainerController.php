<?php

namespace App\Http\Controllers;

use App\Models\Container;

class ContainerController extends Controller
{

    /**
     * Get Container Data
     *
     * Returns Container data array
     *
     * Otherwise returns unauthenticated or error array
     *
     * @group Container
     * @urlParam id int required Container ID.
     *
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return self::format(Container::where('ID', $id)->get());
    }

}
