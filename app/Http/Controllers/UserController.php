<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class UserController extends Controller
{

    /**
     * Get User data
     *
     * Returns User data array
     *
     * Otherwise returns unauthenticated or error array
     *
     * @group Users
     * @urlParam id int required User ID.
     *
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return User::where('id', $id)->get();
    }

    /**
     * Forgot Password
     *
     * Requests password reset email for specific user
     *
     * Otherwise returns unauthenticated or error array
     *
     * @group Users
     * @bodyParam email email required Email Address.
     *
     * @unauthenticated
     * @param Request $request
     * @return RedirectResponse
     */
    public function forgotPassword(Request $request): RedirectResponse
    {

        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
            ? back()->with(['status' => __($status)])
            : back()->withErrors(['email' => __($status)]);
    }

    public function resetPassword (Request $request): RedirectResponse
    {

        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
        ]);

        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));

                $user->save();

                event(new PasswordReset($user));
            }
        );

        return $status === Password::PASSWORD_RESET
            ? redirect()->route('login')->with('status', __($status))
            : back()->withErrors(['email' => [__($status)]]);
    }
}
