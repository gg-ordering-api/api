<?php

namespace App\Traits;

use Illuminate\Support\Collection;

trait DatabaseColumnMapper
{

    /**
     * @param Collection $collection
     * @return array
     */
    public static function format(Collection $collection): array
    {
        $newCollection = [];
        $collection->each(function ($item, $k) use (&$newCollection) {

            $array = $item->toArray();
            $map = $item->map;
            $newArray = [];

            if ($map) {
                array_walk($array, function ($val, $key) use ($map, &$newArray, $k) {
                    if (array_key_exists($key, $map)) {
                        $newArray[$map[$key]] = $val;
                    } else {
                        $newArray[$key] = $val;
                    }
                });
            } else {
                $newArray = $item;
            }

            if ($relation = $item->getRelations()) {

                array_walk($relation, function ($items, $part) use (&$newArray) {

                    if ($items) {
                        $anotherArray = self::format(collect([self::toSnakeCase($part) => $items]));
                        $newArray = array_merge($newArray, [self::toSnakeCase($part) => $anotherArray[0]]);
                    }
                });
            }

            $newCollection[] = $newArray;

        });

        return $newCollection;
    }

    /**
     * @param $string
     * @return string
     */
    private static function toSnakeCase($string): string
    {
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $string)), '_');
    }
}
