<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

trait NiceValidator
{
    public static function validate(
        Request $request,
        array $rules
    ): array
    {
        try {
            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()) {
//                die(json_encode(['success' => false, 'error' => $validator->errors()]));
            }

            return $validator->validated();

        } catch (ValidationException $e) {
            dd($e);
        }
    }
}
