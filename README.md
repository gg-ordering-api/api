## Local Development
First off as with all other local dev make sure you have docker installed.

## Setup
Create an entry in your `hosts` file for the following: `gg-api.local`  

Ensure you have a `.env`

[comment]: <> (Create the network manually: ```docker network create nginx-proxy```)

Run the environment in the background: ```docker-compose up -d```

You can check all is running ok with `docker-compose ps` which should return  
```
              Name                            Command               State                Ports              
------------------------------------------------------------------------------------------------------------
gg-mailhog   MailHog                          Up      0.0.0.0:1025->1025/tcp, 0.0.0.0:8025->8025/tcp
gg-mysql     docker-entrypoint.sh mariadbd    Up      0.0.0.0:3306->3306/tcp
gg-nginx     /docker-entrypoint.sh ngin ...   Up      0.0.0.0:8080->80/tcp
gg-php       docker-php-entrypoint php-fpm    Up      9000/tcp
gg-redis     docker-entrypoint.sh redis ...   Up      0.0.0.0:6379->6379/tcp

```

Run the following to install composer packages
``` docker exec gg-php composer install ```

Access the site here: [http://gg-api.local:8080/](http://gg-api.local:8080/ "http://gg-api.local:8080/")

Follow instructions on screen to generate app key

## Debugging
run docker-compose without the `-d` flag to see live logs

## Usage
To run commands interactively inside the container prefix the command with `docker exec -it gg-php` eg  
`docker exec -it gg-php php artisan tinker`
## Containers
All mail sent from local dev can be found here: [http://localhost:8025](http://localhost:8025 "Mailhog")

Credentials for **MySQL** and **Redis** can be found in the .env - user preferred client


## API Documentation
To genereate Scribe documentation for the API: `php artisan scribe:generate`
