<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'GoGreen')
<img src="https://gogreen.co.uk/wp-content/themes/fcs/images/logo.svg" style="width:180px" class="logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
