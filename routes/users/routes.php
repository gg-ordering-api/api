<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/get/{id}', [UserController::class, 'getById']);
