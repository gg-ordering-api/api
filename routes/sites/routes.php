<?php

use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Route;

Route::get('/get/{id}', [SiteController::class, 'getById']);
Route::get('/get/client/{id}', [SiteController::class, 'getByClientId']);
