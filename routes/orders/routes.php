<?php

use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

Route::get('/get/{id}', [OrderController::class, 'getById']);
Route::get('/get/user/{id}', [OrderController::class, 'getByUserId']);
Route::get('/get/site/{id}', [OrderController::class, 'getBySiteId']);
Route::get('/get/client/{id}', [OrderController::class, 'getByClientId']);
Route::post('/create/', [OrderController::class, 'createOrder']);
Route::post('/amend/', [OrderController::class, 'updateOrder']);
