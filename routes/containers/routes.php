<?php

use App\Http\Controllers\ContainerController;
use Illuminate\Support\Facades\Route;

Route::get('/get/{id}', [ContainerController::class, 'getById']);
