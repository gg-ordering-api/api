<?php

use App\Http\Controllers\MovementController;
use Illuminate\Support\Facades\Route;

Route::get('/get/{id}', [MovementController::class, 'getById']);
Route::get('/get/site/{id}', [MovementController::class, 'getBySiteId']);
Route::get('/get/site/{id}/', [MovementController::class, 'getBySiteId']);
Route::get('/get/site/{id}/categorised', [MovementController::class, 'getMovementsBySiteCategoriseByState']);
Route::get('/get/client/{id}', [MovementController::class, 'getByClientId']);
