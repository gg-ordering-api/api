<?php

use App\Http\Controllers\UserController;
use App\Models\APIConfig;
use App\Models\DisabledDates;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Login
 *
 * Simple user login and if successful returns an array containing device name and bearer token
 *
 * Otherwise returns array containing error message
 *
 * @group Login Authentication
 * @bodyParam email string required Users Email Address.
 * @bodyParam password password required Users Password.
 * @bodyParam device_name string required Unique Device Name.
 * @unauthenticated
 */
Route::post('/login', function (Request $request) {

    $request->validate([
        'email' => 'required|email',
        'password' => 'required',
        'device_name' => 'required',
    ]);

    $config = APIConfig::find(1);
    $user = User::where('email', $request->email)->first();
    $disabledDates = DisabledDates::orderby('date')->get();

    if (! $user || ! Hash::check($request->password, $user->password)) {
        return [
            'success' => false,
            'error' => 'Incorrect Details Provided'
        ];
    }

    return [
        'success' => true,
        'device_name' => $request->device_name,
        'token' => $user->createToken($request->device_name)->plainTextToken,
        'user' => $user,
        'config' => $config,
        'disabled_dates' => $disabledDates
    ];

});

Route::middleware('auth:sanctum')
    ->name('clients')
    ->prefix('clients')
    ->group(__DIR__ . '/clients/routes.php');

Route::middleware('auth:sanctum')
    ->name('users')
    ->prefix('users')
    ->group(__DIR__ . '/users/routes.php');

Route::middleware('auth:sanctum')
    ->name('sites')
    ->prefix('sites')
    ->group(__DIR__ . '/sites/routes.php');

Route::middleware('auth:sanctum')
    ->name('movements')
    ->prefix('movements')
    ->group(__DIR__ . '/movements/routes.php');

Route::middleware('auth:sanctum')
    ->name('containers')
    ->prefix('containers')
    ->group(__DIR__ . '/containers/routes.php');

Route::middleware('auth:sanctum')
    ->name('orders')
    ->prefix('orders')
    ->group(__DIR__ . '/orders/routes.php');

Route::post('/forgot-password', [UserController::class, 'forgotPassword']);

Route::post('/reset-password', [UserController::class, 'resetPassword']);

Route::get('/test/{id}', [\App\Http\Controllers\MovementController::class, 'getMovementsBySiteTest']);
