<?php

use App\Http\Controllers\ClientController;
use Illuminate\Support\Facades\Route;

Route::get('/get/{id}', [ClientController::class, 'getById'])->whereNumber('id');
